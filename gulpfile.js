const { series, parallel, src, dest, watch } = require("gulp");
const cleaCss = require("gulp-clean-css");
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
// const imagemin = require("gulp-imagemin");
const plumber = require("gulp-plumber");
// const cache = require("gulp-cache");
const babel = require("gulp-babel");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const rename = require("gulp-rename");
const sass = require("gulp-sass")(require("sass"));
const browserSync = require("browser-sync").create();
const { join } = require("path");
const { sync } = require("glob");
sass.compiler = require("node-sass");


const path = join(__dirname, "src");

const compileScss = () => {
  return src(join(path, "scss", "main.scss"))
    .pipe(concat("main.css"))
    .pipe(sass().on("error", sass.logError))
    .pipe(autoprefixer("last 4 version"))
    .pipe(dest(join(__dirname, "assets", "css")));
};
const minifyCss = () => {
  return src(sync(join(__dirname, "assets", "css", "main.css")))
    .pipe(autoprefixer("last 4 version"))
    .pipe(cleaCss({ compatibility: "ie9" }))
    .pipe(
      rename((path) => {
        return {
          dirname: path.dirname,
          basename: `${path.basename}.min`,
          extname: ".css",
        };
      })
    )
    .pipe(dest(join(__dirname, "assets", "css")));
};

const compileVendorCss = () => {
  return src(join(path, "scss", "vendorCss", "**/*.css"))
    .pipe(concat("vendor.css"))
    .pipe(dest(join(__dirname, "assets", "css")));
};
const minifyVendorCss = () => {
  return src(sync(join(__dirname, "assets", "css", "vendor.css")))
    .pipe(cleaCss({ compatibility: "ie9" }))
    .pipe(
      rename((path) => {
        return {
          dirname: path.dirname,
          basename: `${path.basename}.min`,
          extname: ".css",
        };
      })
    )
    .pipe(dest(join(__dirname, "assets", "css")));
};

const getFonts = () => {
  return src(join(path, "fonts", "**/*.*"))
    .pipe(plumber())
    .pipe(dest(join(__dirname, "assets", "fonts")));
};

const compileJs = () => {
  return src(sync(join(path, "js", "custom-js", "**/*.js")))
    .pipe(concat("script.js"))
    .pipe(
      babel({
        presets: ["@babel/env"],
      })
    )
    .pipe(dest(join(__dirname, "assets", "js")));
};

const minifyJs = () => {
  return src(sync(join(__dirname, "assets", "js", "script.js")))
    .pipe(concat("script.js"))
    .pipe(uglify())
    .pipe(
      rename((path) => {
        return {
          dirname: path.dirname,
          basename: `${path.basename}.min`,
          extname: ".js",
        };
      })
    )
    .pipe(dest(join(__dirname, "assets", "js")));
};

const compileVendorJs = () => {
  return src(sync(join(path, "js", "vendors-js", "**/*.js")))
    .pipe(concat("vendor.js"))
    .pipe(dest(join(__dirname, "assets", "js")));
};

const minifyVendorJs = () => {
  return src(sync(join(__dirname, "assets", "js", "vendor.js")))
    .pipe(concat("vendor.js"))
    .pipe(uglify())
    .pipe(
      rename((path) => {
        return {
          dirname: path.dirname,
          basename: `${path.basename}.min`,
          extname: ".js",
        };
      })
    )
    .pipe(dest(join(__dirname, "assets", "js")));
};

const minifyImages = () => {
  return src(join(path, "images", "**/*.*"))
    // .pipe(cache(imagemin()))
    .pipe(dest(join(__dirname, "assets", "images")));
};

const dev = () => {
  browserSync.init({
    proxy: "http://localhost/whatcx-ui/",
    port: 3000,
  });
};

const watchFiles = (cb) => {
  const jsFiles = sync(join(path, "js", "**/*.js"));
  watch(jsFiles, series(compileJs, minifyJs, reloadBrowser));
  const scssFiles = sync(join(path, "scss", "**/*.scss"));
  watch(scssFiles, series(compileScss, minifyCss, reloadBrowser));
  const phpFiles = sync(join(__dirname, "**/*.php"));
  watch(phpFiles, reloadBrowser);
  const fonts = sync(join(path, "fonts", "**/*.*"));
  watch(fonts, series(getFonts, reloadBrowser));
  const images = sync(join(path, "images"));
  watch(images, series(minifyImages));
  cb();
};

const reloadBrowser = (cb) => {
  browserSync.reload();
  cb();
};
const cleanAssets = () => {
  return src(sync(join(__dirname, "assets"))).pipe(clean());
};

exports.default = series(
  parallel(compileJs, compileScss),
  parallel(minifyJs, minifyCss),
  compileVendorJs,
  minifyVendorJs,
  watchFiles,
  getFonts,
  minifyImages,
  dev
);

exports.build = series(
  parallel(compileJs, compileScss),
  parallel(minifyJs, minifyCss),
  parallel(compileVendorJs),
  parallel(minifyVendorJs),
  minifyImages
);

exports.clean = cleanAssets;
