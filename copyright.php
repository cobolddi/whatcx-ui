<!DOCTYPE html>
<html lang="en">

<head>
    <title>Copyright Notice- TurboCX</title>
    <meta name="description" content="Except as permitted by the copyright law applicable to you, you may not reproduce or communicate any of the content on this website.">
       
    <meta property="og:title" content="Copyright Notice- TurboCX" />
    <meta property="og:description" content="Except as permitted by the copyright law applicable to you, you may not reproduce or communicate any of the content on this website." />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/copyright.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/whatcx-logo.php">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/whatcx-logo.php" />
    <meta property="og:image:alt" content="Copyright Notice- turbocx" />
    <link rel="canonical" href="https://turbocx.com/copyright.php/" />

<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#"> <img src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Copyright</a></li>
            </ul>
        </div>
    </section>

    <section class="Section spBanner-Section">
        <div class="container">
            <div class="spBanner">
                <div class="spBanner--heading centerSectionHeading">
                    <h1>Copyright</h1>
                </div>
            </div>
        </div>
    </section>


    <section class="Section TCP-Section">
        <div class="container-small">
            <div class="TCP-Section-content">
                <div class="TCP-Section-content_item">
                    <!-- <h4>Copyright</h3> -->
                    <p>Copyright 2022, EdgeCX Private Limited</p>
                    <p>1st March 2022</p>
                    <p>Except as permitted by the copyright law applicable to you, you may not reproduce or communicate any of the content on this website, including files downloadable from this website, without the permission of the copyright owner.</p>
                    <p>The Indian Copyright Act does allow certain uses of content from the internet without the copyright owner’s permission. This includes uses by educational institutions and by Commonwealth and State governments, provided fair compensation is paid. For more information, see <a href="https://copyright.gov.in/">https://copyright.gov.in/</a> and <a href="https://ipindia.gov.in/copyright.htm">https://ipindia.gov.in/copyright.htm</a>.</p>
                    <p>The owners of copyright in the content on this website may receive compensation for the use of their content by educational institutions and governments, including from licensing schemes managed by the Copyright Agency.</p>
                    <p>We may change these terms of use from time to time. Check before re-using any content from this website.</p>
                </div>
                
            </div>
        </div>
    </section>

    

   
</main>

<?php @include('template-parts/footer.php') ?>