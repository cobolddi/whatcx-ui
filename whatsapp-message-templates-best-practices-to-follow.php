<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX: Whatsapp message templates</title>
    <meta name="description" content="Message templates are a great way to save time and make sure your message is consistent across channels.
">
    <meta property="og:title" content="Whatsapp message templates" />
    <meta property="og:description" content="Message templates are a great way to save time and make sure your message is consistent across channels.
" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/whatsapp-message-templates-best-practices-to-follow.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.turbocx.com/assets/images/blog/single/whatsapp-msg-template-blog.webp">
    <meta property="og:image:secure_url" content="https://www.turbocx.com/assets/images/blog/single/whatsapp-msg-template-blog.webp" />
    <meta property="og:image:alt" content="Whatsapp message templates" />
    <link rel="canonical" href="https://turbocx.com/whatsapp-message-templates-best-practices-to-follow.php/" />
    <?php @include('template-parts/header.php') ?>

    <main>

        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="https://turbocx.com/blogs.php"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Resources</a></li>
                    <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog Detail</a></li>
                </ul>
            </div>
        </section>


        <!-- blog detail section -->
        <section class="Section blogSingle">
            <div class="container-small">

                <h1 class="blogSingle__heading">Whatsapp message templates: Best practices to follow</h1>

                <div class="blogSingle__area">
                    <div class="blogSingle__date">
                        <span>1/01/2022</span>
                    </div>

                    <?php @include('template-parts/share-icons.php'); ?>

                </div>

                <div class="blogSingle-image">
                    <img  src="assets/images/blog/single/whatsapp-msg-template-blog.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                </div>

                <article class="blogSingle__content">

                    <h3>What are Whatsapp message templates?</h3>

                    <p>WhatsApp template messages are pre-written, standardized, and approved by (Whatsapp) messages that are sent by a business to customers. These include support-related automatic replies, order confirmations, delivery updates, product recommendations, helpful informational alerts, relevant offers, newsletters, etc. These templates can be personalized according to different customers. For example, Hello (customer name), Your (product name) with (order ID) is ready to be delivered to your doorstep.</p>

                    <h3>Types of Whatsapp Business template messages</h3>

                    <ol type="1">
                        <li>Account updates
                            These messages begin to follow when a customer makes changes to their account - username, passwords, or any other sensitive information.
                        </li>

                        <p>
                            “Hey { {1} }, Thanks for registering with us. Your account is verified and ready to go. You can now access all the premium content.”
                        </p>

                        <li>
                            Alert updates
                            These are the notifications or updates. Alerts are mostly followed after a customer makes the purchase. It could be related to delivery or shipping details.
                        </li>

                        <p>
                            “Hey { {1} }, we have delivered your products. We hope you like them. Thanks for choosing us.”
                        </p>

                        <li>
                            Appointment updates
                            The businesses use appointment updates to remind customers about an appointment or follow-up with them.
                        </li>

                        <p>
                            “Dear {{1}}, You have an appointment scheduled with us for this weekend{{2}}. See you then!”
                        </p>

                        <li>
                            Auto-reply
                            Auto replies are sent to the customers when they contact businesses in odd or non-working hours just to ensure them that someone will be soon contacting them.
                        </li>

                        <p>
                            “Hello {{1}}, Thank you for reaching out to us. On working days we often respond to your message within 24 hours. We will get in touch with you very soon”
                        </p>

                        <li>
                            Issue resolution
                            It’s common to face bugs, break down, or malfunction in the system. Issue resolution informs customers about such issues.
                        </li>

                        <p>
                            “Hello {{1}}, We are currently experiencing a malfunction concerning {{2}}. We apologize for the inconvenience. The network will resume by midnight; we appreciate your patience.”
                        </p>

                        <li>
                            Payment updates
                            Businesses use these templates to inform customers that their order is ready to be
                            shipped and that they have received the payment successfully.
                        </li>


                        <p>
                            “Hie {{1}}, We have received your payment successfully. Your order {{2}} has been shipped and will reach you on time. Happy shopping!”
                        </p>

                        <li>
                            Reservation updates
                            Customers often book their spaces before visiting a place. These are used to tell them that their place has been reserved.
                        </li>

                        <p>
                            “Thank you for your reservation at {{1}}! We look forward to seeing you on {{2}}. To know about the check-in and check-out timings and other policies. Please visit the below link.”
                        </p>

                        <li>Shipping updates
                            These template messages are truly loved by the customers. These include order updates and when they can expect it to arrive.
                        </li>
                        <p>
                            “Hello {{1}}, Your order has been shipped. You can follow this link {{2}} to track your order. Happy shopping!”
                        </p>

                        <li>Ticket updates
                            Instead of making your customers wait to resolve an issue, make use of these templates to update them about the ticket they have raised.
                        </li>

                        <p>
                            “Dear { {1} }, this is in response to your complaint raised on your order of { {2} }. We have informed our Sales Team, they will be getting in touch with you within 24 hours. We appreciate your patience.”
                        </p>

                        <li>
                            Personal finance updates
                            These come in handy in the case of insurance and financial institutions to inform customers regarding their balance or any debt they have to repay.
                        </li>

                        <p>
                            “Dear { {1} }, your account { {2} }, has been credited with { {3} }. Your current account balance is { {4} }”
                        </p>

                        <li>
                            Welcome messages
                            When a new customer signs up for a newsletter or creates an account then the welcome messages are followed.
                        </li>
                        <p>
                            “Welcome {{1}}. We look forward to serving you on WhatsApp.”
                        </p>
                    </ol>

                    <h3>Message templates (After 24 hours have been passed)</h3>

                    <p>The most common situation arises when the customers contact the businesses in odd hours or if 24 hours have passed since the customer contacted the business. Then the template messages that should follow are:
                    </p>

                    <ol type="1">
                        <li>
                            Hey {{customer name}}. Thank you for contacting us. It seems that you have tried to connect with us out of working hours. One of our team members will contact you soon. Till then please <a href="https://turbocx.com/" target="_blank">visit our website</a> and check the latest offers. Thank you, Team Verna.
                        </li>
                        <li>
                            Oops! We apologize for not getting back to you in time. Please respond to this message if you still want an answer. We will respond to you as soon as possible.
                        </li>
                        <li>
                            Hello {{customer name}}, Unfortunately, it is not possible for us to respond to your query. If you still want to receive a response later on, please respond to this message with a thumbs up and we will get back to you soon. Thank you, Team Verna.
                        </li>
                    </ol>

                    <p>
                        To reopen the 24-hour window, begin with some mention of the previous conversation thread:
                    </p>

                    <p>
                        “We’re extremely sorry that we weren't able to respond to your queries yesterday but we’re happy to assist you now. If you’d like to continue this discussion, please reply with a “thumbs up”.
                    </p>

                    <h3>Some cases of rejected messages</h3>

                    <p>WhatsApp has formulated clear guidelines on the use of template messaging. Following something else from these are completely rejected. </p>

                    <ol type="2">
                        <li>
                            Promotional messages: Earlier WhatsApp rejected promotional content straight away but now it has allowed businesses to send promotional notifications to its customers. But remember overpromotion is not what you have to do. <br>
                            <strong>Replace</strong><br>
                            “Hello (customer name), Thank you for buying our products. Please visit our website and see the latest offers. Grab a heavy discount. Don’t miss the chance. Book your order today.”
                            <strong>with</strong><br>
                            “Hello {{customer name}}, We have a great offer for you. Get a flat 70% off on all the products. Offer valid till {{date}}”.
                        </li>
                        <li>Incorrect format: There is a proper format mentioned in Whatsapp guidelines. Messages with the wrong format are rejected. e.g. Hello ((customer name)), order ID {2270g}
                            Ensure the correct number of curly brackets (i.e., 2 on the left side of the number and 2 on the right side).
                        </li>
                        <li>Language selection: The language should match with the context. If the context is in English and the language selected by you is French, the template gets rejected. Even the mix of languages such as Hinglish is not supported.
                        </li>
                        <li>URL: The URL should belong to your business. Do not shorten the URL. </li>
                    </ol>

                    <h3>Best practices to follow in creating templates</h3>

                    <ol>
                        <li>
                            Develop an engaging tone<br>
                            Customers have limited attention spans. According to research, our attention span has markedly decreased in 15 years. Ensure that message templates should be enough engaging to appeal to the audience and make them want to read your message.
                        </li>
                        <li>
                            Don’t just stick to sales<br>
                            Sending sales texts every time lowers your brand value. Bombarding your customers with marketing messages is not an ideal plan. Some examples include:
                            <ol type="a">
                                <li>Messages with a marketing tone</li>
                                <li>Promotional offers</li>
                                <li>Overuse of caps lock</li>
                                <li>Usage of words like free, offers, deals</li>
                                <li>Messages adding no value</li>
                            </ol>
                        </li>

                        <li>
                            Avoid spammy content<br>
                            Do not ask for any personal information or share any broken links through the messages. It lowers the reputation of your business and brand. Overpromotion and sharing of malicious content leave customers with the single option of blocking your number which is not at all a good thing for your business. Avoid grammatical errors or mistakes to prevent yourself from getting marked as spam.
                        </li>
                        <li>
                            Follow 24-hour session window<br>
                            The basic rule to follow while sending a template message is that you have a 24-hour window. This means that your chat executive can talk to customers for 24 hours and after that template messages start to follow. This is the basic rule Whatsapp has implemented in its messaging system.
                        </li>
                    </ol>

                    <h3>Manage your business with TurboCX </h3>
                    <p>
                        Messaging plays a huge role in improving customer service. Unlike other platforms, sending messages to your customers on Whatsapp Cloud API is a tricky process. <a href="https://turbocx.com/" target="_blank">Whatsapp Cloud API</a> is helping businesses drive deeper engagement with help of template messaging and with its pre-defined categories and rich media capabilities.
                    </p>

                    <p>
                        <a href="https://turbocx.com/" target="_blank">TurboCX</a> makes managing your business easier. We have functionality for adding, approving, and creating broadcasts. Send unlimited messages, broadcast messages, resolve queries instantly with our best-in-business solutions.
                    </p>

                </article>


            </div>
        </section>
        <!-- blog detail section -->


        <?php @include('template-parts/blog-cards.php') ?>


    </main>

    <?php @include('template-parts/footer.php') ?>