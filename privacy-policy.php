<!DOCTYPE html>
<html lang="en">

<head>
    <title>Privacy Policy- TurboCX</title>
    <meta name="description" content="The information we collect from you through our Website is typically used by us in the following ways:">

    <meta property="og:title" content="Privacy Policy- TurboCX" />
    <meta property="og:description" content="The information we collect from you through our Website is typically used by us in the following ways:" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/privacy-policy.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/guide.webp">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/guide.webp" />
    <meta property="og:image:alt" content="Privacy Policy- TurboCX" />
    <link rel="canonical" href="https://turbocx.com/privacy-policy.php/" />

<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Privacy Policy</a></li>
            </ul>
        </div>
    </section>

    <section class="Section spBanner-Section">
        <div class="container">
            <div class="spBanner">
                <div class="spBanner--heading centerSectionHeading">
                    <h1>Privacy Policy</h1>
                </div>
            </div>
        </div>
    </section>


    <section class="Section TCP-Section">
        <div class="container-small">
            <div class="TCP-Section-content">
                <div class="TCP-Section-content_item">
                    <h4>Who we are</h3>
                    <p>Our website address is <a href="https://turbocx.com/">https://turbocx.com/</a>. This document states how we collect, process, and handle data of our customers and visitors. This policy applies to all our web applications, i.e. on every web presence of TurboCX and its subsidiaries.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Comments</h3>
                    <p>When visitors leave comments on the site we collect the data shown in the comments form, and also the visitor’s IP address and browser user agent string to help spam detection. An anonymized string created from your email address (also called a hash) may be stored. After approval of your comment, your profile picture is visible to the public in the context of your comment.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>What information do we collect?</h3>
                    <ul>
                        <li>Web Browser</li>
                        <li>IP Address</li>
                        <li>Time Zone</li>
                        <li>Cookies</li>
                        <li>Name</li>
                        <li>Address</li>
                        <li>Email</li>
                        <li>Phone Number</li>
                        <li>Age</li>
                        <li>Gender</li>
                        <li>Nationality</li>
                        <li>Payment details (In case of purchase)</li>
                    </ul>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Media</h3>
                    <p>If you upload images to the website, you should avoid uploading images with embedded location data (EXIF GPS) included. Visitors to the website can download and extract any location data from images on the website.</p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>Cookies</h3>
                    <p>If you leave a comment on our site you may opt-in to saving your name, email address, and website in cookies. These are for your convenience so that you do not have to fill in your details again when you leave another comment. These cookies will last for one year.</p>
                    <ul>
                        <li>Visit our page</li>
                        <li>We will set a temporary cookie to determine if your browser accepts</li>
                        <li>This cookie contains no personal data</li>
                        <li>Save your information and your screen display choices</li>
                    </ul>
                    <p>
                    If you visit our page, we will set a temporary cookie to determine if your browser accepts cookies. This cookie contains no personal data and is discarded when you close your browser. When you log in, we will also set up several cookies to save your login information and your screen display choices. Login cookies last for two days, and screen options cookies last for a year. If you select “Remember Me”, your login will persist for two weeks. If you log out of your account, the login cookies will be removed. If you edit or publish content, an additional cookie will be saved in your browser. This cookie includes no personal data and simply indicates the unique ID in the context of the content you just edited. It expires after 1 day.
                    </p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>Embedded content from other websites</h3>
                    <p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website. These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Who we share your data with</h3>
                    <p>If you request a password reset, your IP address will be included in the reset email.</p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>How long do we retain your data</h3>
                    <p>If you leave a comment, the comment and its metadata are retained indefinitely. This is so that we can recognize and approve any follow-up comments automatically instead of holding them in a moderation queue. For users that register on our website (if any), we also store the personal information they provide in their user profile. All users can see, edit, or delete their personal information at any time. Website administrators can also see and edit that information.<br>
                    What rights you have over your data</p>
                    <p>If you have left comments on this site, you can request to receive the personal data we hold about you, including any data you have provided to us. You can also request that we erase any personal data we hold about you. This does not include any data we are obliged to keep for administrative, legal, or security purposes.
                    </p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>Where we send your data</h3>
                    <p>Visitor comments may be checked through an automated spam detection service.
                    </p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>Disclaimer</h3>
                    <p>We may reserve the right to change this policy from time to time as per the company standards. Nothing in this document can prevent us from making changes to any clauses mentioned in our policy. By complying with this, you agree that any security breaches beyond our control are at your sole risk and discretion. 
                    </p>
                </div>

                <div class="TCP-Section-content_item">
                    <h4>Contact us </h3>
                    <p>For more information regarding our privacy practices, if you have queries, or if you would like to make a complaint, please contact us at our email address. We would be happy to solve them. 
                    </p>
                </div>
            </div>
        </div>
    </section>

    

   
</main>

<?php @include('template-parts/footer.php') ?>