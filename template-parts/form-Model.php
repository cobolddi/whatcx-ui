<div class="Section modalWindow">
    <div class="modalWindow-content">
        <button class="modalWindow-close">
            <svg id="noun_Close_1548868" xmlns="http://www.w3.org/2000/svg" width="21.826" height="21.826"
                viewBox="0 0 21.826 21.826">
                <path id="Path_6495" data-name="Path 6495"
                    d="M41.643,61.926l9.37-9.37,9.37,9.37,1.543-1.543-9.37-9.37,9.37-9.37L60.383,40.1l-9.37,9.37-9.37-9.37L40.1,41.643l9.37,9.37-9.37,9.37Z"
                    transform="translate(-40.1 -40.1)" fill="#06303f" />
            </svg>
        </button>
        <div class="formWrapper">
            <div class="centerSectionHeading">
                <h2>Sign up for a free trial</h2>
            </div>
            <form method="post" class="lead_form" id="lead_form">
                <div class="lead_formG twoColumnForm">
                    <div class="formControl">
                        <label>First Name<span>*</span></label>
                        <input type="text" name="fname" value="" placeholder="First Name" required>
                    </div>
                    <div class="formControl">
                        <label>Last Name<span>*</span></label>
                        <input type="text" name="lname" value="" placeholder="Last Name" required>
                    </div>
                    <div class="formControl">
                        <label>Company Email<span>*</span></label>
                        <input type="email" name="email" value="" placeholder="Company Email" required>
                    </div>
                    <div class="formControl">
                        <label>Phone Number<span>*</span></label>
                        <input type="tel" pattern="[0-9]{10}" name="mobile" placeholder="Phone Number" required>
                    </div>
                    <div class="formControl">
                        <label for="">Company Name<span>*</span></label>
                        <input type="text" name="cname" placeholder="Company Name" required>
                    </div>
                    <div class="formControl">
                        <label for="">Website URL<span>*</span> </label>
                        <input type="url" placeholder="Website URL" name="websiteURL" required>
                    </div>
                    <!-- <div class="formControl">
                        <label>How can we help you?<span>*</span></label>
                        <textarea name="help" id="" cols="30" rows="10" required></textarea>
                    </div> -->
                    <div class="formControl">
                        <div class="btnWrap">
                            <button class="secondaryBtn submitbtn">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>