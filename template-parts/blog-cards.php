<!-- blog card section starts -->
<section class="Section blogCard-section blogSingle-related">
    <div class="container">
        <div class="blogCard__head centerSectionHeading">
            <h2>Related Blogs</h2>
        </div>


        <div class="blogCard__wrapper">

            <button class="blogCard-btnPrev">
                <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M6.93825 12.2001L16.6605 2.54513L14.0976 0L0 14L14.0976 28L16.6605 25.4549L6.93825 15.7999H29V12.2001H6.93825Z"
                        fill="white" />
                </svg>
            </button>

            <button class="blogCard-btnNext">
                <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path
                        d="M22.0618 12.2001L12.3395 2.54513L14.9024 0L29 14L14.9024 28L12.3395 25.4549L22.0618 15.7999H0V12.2001H22.0618Z"
                        fill="white" />
                </svg>
            </button>

            <div class="recent-slider">

                <div class="blogCard blogCard--shadow ">
                    <a href="understanding-the-whatsapp-communication-channel-making-the-most-of-it.php">
                        <img loading="lazy" src="assets/images/blog/thumb/whatsapp-connection.png" alt="card">
                    </a>
                    <div class="blogCard__content">

                        <div class="blogCard__meta">
                            <span>5/01/2022</span>
                            <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->
                        </div>

                        <h3 class="blogCard__heading">Understanding the Whatsapp communication channel - making the most
                            of it.</h3>

                        <a href="understanding-the-whatsapp-communication-channel-making-the-most-of-it.php"
                            class="btn btn--text btn--orange">
                            Read more <?php @include('assets/images/icons/arrow-orange.svg');?>
                        </a>
                    </div>

                </div>


                <div class="blogCard blogCard--shadow ">
                    <a
                        href="do-s-and-dont-s-while-communicating-with-clients-to-avoid-being-reported-as-spam-being-blocked.php">
                        <img loading="lazy" src="assets/images/blog/thumb/teamwork-communication.png" alt="card">
                    </a>

                    <div class="blogCard__content">

                        <div class="blogCard__meta">
                            <span>10/01/2022</span>
                            <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->
                        </div>

                        <h3 class="blogCard__heading">Do's and Dont's - while communicating with clients (to avoid being
                            reported as spam/being blocked)</h3>

                        <a href="do-s-and-dont-s-while-communicating-with-clients-to-avoid-being-reported-as-spam-being-blocked.php"
                            class="btn btn--text btn--orange">
                            Read more <?php @include('assets/images/icons/arrow-orange.svg');?>
                        </a>
                    </div>

                </div>


                <div class="blogCard blogCard--shadow ">
                    <a href="canned-responses-how-to-set-up-quick-responses.php">
                        <img loading="lazy" src="assets/images/blog/thumb/communication.png" alt="card">
                    </a>

                    <div class="blogCard__content">

                        <div class="blogCard__meta">
                            <span>10/01/2022</span>
                            <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->
                        </div>

                        <h3 class="blogCard__heading">Canned responses- how to set up quick responses</h3>

                        <a href="canned-responses-how-to-set-up-quick-responses" class="btn btn--text btn--orange.php">
                            Read more <?php @include('assets/images/icons/arrow-orange.svg');?>
                        </a>
                    </div>

                </div>

                <div class="blogCard blogCard--shadow ">
                    <a href="whatsapp-message-templates-best-practices-to-follow.php">
                        <img loading="lazy" src="assets/images/blog/thumb/whatsapp-msg-template-blog.png" alt="card">
                    </a>

                    <div class="blogCard__content">

                        <div class="blogCard__meta">
                            <span>10/01/2022</span>
                            <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->
                        </div>

                        <h3 class="blogCard__heading">Whatsapp message templates: Best practices to follow</h3>

                        <a href="whatsapp-message-templates-best-practices-to-follow.php"
                            class="btn btn--text btn--orange">
                            Read more <?php @include('assets/images/icons/arrow-orange.svg');?>
                        </a>
                    </div>

                </div>

                <div class="blogCard blogCard--shadow ">
                    <a href="contact-management-and-views-in-whatcx.php">
                        <img loading="lazy" src="assets/images/blog/thumb/contact-management-blog-5.png" alt="card">
                    </a>

                    <div class="blogCard__content">

                        <div class="blogCard__meta">
                            <span>04/03/2022</span>
                            <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->
                        </div>

                        <h3 class="blogCard__heading">Contact management and views in TurboCX</h3>

                        <a href="contact-management-and-views-in-whatcx.php"
                            class="btn btn--text btn--orange">
                            Read more <?php @include('assets/images/icons/arrow-orange.svg');?>
                        </a>
                    </div>

                </div>

              
            </div>
        </div>
    </div>
</section>
<!-- blog card section ends -->