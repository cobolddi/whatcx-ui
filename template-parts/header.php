    <?php
    $version = "?v=1.0.0";
    ?>

    <meta http-equiv="Cache-Control" content="no-cache, no-store, max-age=0, must-revalidate">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="icon" href="assets/images/favicon.png" sizes="32x32" />
    <link rel="preconnect" href="https://fonts.googleapis.com" />
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400&display=swap" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/main.min.css<?php echo $version; ?>" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
    <link href="https://widget.turbocx.com/widget.css" rel="stylesheet">

    <!-- Meta Pixel Code -->
    <script>
        ! function(f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function() {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '322298396625943');
        fbq('track', 'PageView');
    </script>
    <noscript>
        <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=322298396625943&ev=PageView&noscript=1" />
    </noscript>
    <!-- End Meta Pixel Code -->

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-89SH53VP2K"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-89SH53VP2K');
    </script>

    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

    <!-- Google tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-249511955-1">
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-249511955-1');
    </script>

    </head>

    <body class="light">


        <!-- notification slider -->
        <section class="notification">
            <div class="container">

                <div class="notification__wrapper">
                    <div class="notificationSlider">
                        <div class="notificationSlider__item">
                            <p><a href="sign-up.php">Click here to register</a> for a free demo.</p>
                        </div>
                    </div>

                    <img src="assets/images/icons/close.svg" alt="close" alt="TurboCX - Close">
                </div>

            </div>
        </section>


        <nav class="closed">
            <div class="container">
                <div class="nav nav-js">
                    <div class="nav-logo">
                        <a href="/">
                            <?php @include('template-parts/svg/whatcx-logo.php') ?>
                        </a>
                        <div class="navCtaBtn">
                            <a href="sign-up.php" class="secondaryBtn">Sign up for free Demo</a>
                            <button class="navHamBtn" id="toggle">
                                <div class="one"></div>
                                <div class="two"></div>
                                <div class="three"></div>
                            </button>
                        </div>
                    </div>
                    <div class="nav-links">
                        <ul>
                            <li><a href="/"> Home</a></li>
                            <li>
                                <span>
                                    <a href="javascript:;">Product</a>
                                    <?php @include('assets/images/icons/arrow-down.svg'); ?>
                                </span>
                                <ul>
                                    <li>
                                        <a href="benefits.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/benefit.php') ?>
                                            </span>
                                            <span>
                                                <h5>Benefits</h5>
                                                <p>Explore how TurboCX can benefit your business.</p>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="features.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/features.php') ?>
                                            </span>
                                            <span>
                                                <h5>Features</h5>
                                                <p>Get best-in-class solutions to grow your business.</p>
                                            </span>
                                        </a>

                                    </li>
                                    <li>
                                        <a href="how-it-works.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/hiw.php') ?>
                                            </span>
                                            <span>
                                                <h5>How it works</h5>
                                                <p>Learn how TurboCX works to get started in a few easy steps.</p>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="book-a-demo.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/book-a-demo.php') ?>
                                            </span>
                                            <span>
                                                <h5>Book a free demo</h5>
                                                <p>Schedule a free demo with us today.</p>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <span>
                                    <a href="javascript:;">Solutions</a>
                                    <?php @include('assets/images/icons/arrow-down.svg'); ?>
                                </span>
                                <ul>
                                    <li>
                                        <a href="solution-jewellery.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/calendar.php') ?>
                                            </span>
                                            <span>
                                                <h5>Jewellery</h5>
                                                <p>Find more deals and close more leads for your business. </p>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="solution-real-estate.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/git.php') ?>
                                            </span>
                                            <span>
                                                <h5>Real Estate</h5>
                                                <p>Excel in customer and client servicing with TurboCX.</p>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="solution-educational.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/stack.php') ?>
                                            </span>
                                            <span>
                                                <h5>Education</h5>
                                                <p>Enhance learning experiences and automate communication.</p>
                                            </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="solutions.php">
                                            <span>
                                                <?php @include('template-parts/svg/icons/all-industry.php') ?>
                                            </span>
                                            <span>
                                                <h5>Explore all industries</h5>
                                                <p>See how TurboCX helps businesses of all types succeed.</p>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li><a href="pricing.php">Pricing</a></li>
                            <li><a href="faqs.php">FAQs</a></li>
                            <li><a href="contact.php">Contact</a></li>
                            <li><a href="blogs.php">Resources</a></li>
                        </ul>
                        <div class="navMenuCta">
                            <a href="sign-up.php" class="primaryBtn">Sign up for free Demo</a>
                        </div>
                    </div>
                    <div class="nav-actions">
                        <div class="nav-actions--darkSwitch">
                            <label>
                                <input type="checkbox">
                                <div class="planet">
                                </div>
                                <div class="elements">
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                    <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                        <circle cx="250" cy="250" r="200" />
                                    </svg>
                                </div>
                            </label>
                        </div>
                        <div class="nav-actions--login">
                            <a href="https://app.turbocx.com/login" target="_blank">Login</a>
                        </div>
                        <div class="nav-actions--signUp">
                            <a href="sign-up.php" class="secondaryBtn">Sign up for free Demo</a>
                        </div>
                    </div>

                </div>
            </div>
        </nav>