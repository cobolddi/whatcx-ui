<!--  b2cBrochure -->
<section class="Section ppBrochureSection"> 
    <div class="container">
      <div class="ppBrochure">
        <h5>Download Brochure</h5>
        <p>Download our brochure and stay up to date while learning more about conversational messaging. </p>
        <div>
          <form class="brochure-form" action="email/thanks.php" method="post">
            <input type="email" placeholder="Enter your email ID." required>
            <input type="hidden" value="TurboCX Download Brochure ">
            <button type="submit">Download</button>
            <!-- <a download class="filePdf" href="assets/images/pdf/dummy.pdf" hidden></a> -->
          </form>
          <div class="info"></div>
          <div class="success">Thanks for sharing the email address. We will mail the brochure to you shortly.</div>
        </div>
      </div>
    </div>
  </section>
<!--  -->
