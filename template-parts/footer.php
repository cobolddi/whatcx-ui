<?php @include('template-parts/form-Model.php') ?>

<!-- <a href="https://wa.me/+918595665505?text=" target="_blank" class="sticky-whatsapp" target="_blank">
  <img src="assets/images/whatsapp.svg" alt="TurboCX - whatsapp icon">
</a> -->

<footer class="Section footer-Section">
  <div class="container">
    <div class="footer fourColWithCenteredOrphans">
      <div class="colItems colItems-2  footer-logo">
        <?php @include('template-parts/svg/whatcx-logo.php') ?>
        <div class="footer-logo--social">
          <ul>
            <li><a href="https://wa.me/+918595665505?text=" target="_blank"><img src="assets/images/icons/Group 54.svg" alt="TurboCX - whatsapp icon"></a></li>
            <li><a href="https://www.linkedin.com/companywhatcx/" target="_blank"><img src="assets/images/icons/linkedin.svg" alt="TurboCX - linkedin icon"></a></li>
            <li><a href="https://www.facebook.comwhatcx/" target="_blank"><img src="assets/images/icons/Group 56.svg" alt="TurboCX - facebook icon"></a></li>
            <li><a href="https://www.instagram.comwhatcxofficial/" target="_blank"><img src="assets/images/icons/Group 58.svg" alt="TurboCX - instagram icon"></a></li>
            <li><a href="https://www.youtube.com/channel/UC0IZEqVmjENlVo4U50hQDjw/" target="_blank"><img src="assets/images/icons/youtube-icon.svg" alt="TurboCX - youtube icon"></a></li>
          </ul>
        </div>
        <div class="download-links">
          <ul>
            <li><a href="https://play.google.com/store/apps/details?id=com.whatcx.app" target="_blank"><?php @include('template-parts/svg/play-store.php') ?></a></li>
            <li><a href="https://apps.apple.com/my/app/whatcx-247/id1629037587" target="_blank"><?php @include('template-parts/svg/app-store.php') ?></a></li>
          </ul>
        </div>
      </div>

      <div class="colItems footerCompany">
        <h5 role="button">Solutions</h5>
        <ul>

          <li><a href="solution-jewellery.php">Jewellery </a></li>
          <li><a href="solution-real-estate.php">Real Estate</a></li>
          <li><a href="solution-educational.php">Education </a></li>
          <li><a href="solutions.php">Explore all industries</a></li>

        </ul>
      </div>

      <div class="colItems footerProduct">
        <h5 role="button">Product</h5>
        <ul>
          <li><a href="book-a-demo.php">Book a free demo</a></li>
          <li><a href="benefits.php"> Benefits</a></li>
          <li><a href="features.php">Features</a></li>
          <li><a href="how-it-works.php">How it works</a></li>
          <li><a href="pricing.php">Pricing </a></li>
          <li><a href="faqs.php">FAQs </a></li>

        </ul>
      </div>

      <div class="colItems footerPolicies">
        <h5 role="button">Company</h5>
        <ul>
          <li><a href="contact.php">Contact Us</a></li>
          <li><a href="blogs.php">Resources</a></li>
          <li><a href="cobold.php">Cobold Digital</a></li>
        </ul>
      </div>

      <div class="colItems footerPolicies">
        <h5 role="button">Privacy</h5>
        <ul>
          <li><a href="disclaimer.php">Disclaimer</a></li>
          <li><a href="terms-of-use.php">Terms of Use</a></li>
          <li><a href="privacy-policy.php">Privacy Policy</a></li>
          <li><a href="copyright.php">Copyright Notice</a></li>
          <li><a href="refund-policy.php">Refund & Cancellation Policy</a></li>
        </ul>
      </div>

    </div>
  </div>
</footer>
<div class="footerBottom">
  <p>Copyright 2022, EdgeCX Private Limited</p>
  <img class="desktop" src="assets/images/footerbottom.png" alt="TurboCX - footer image desktop">
  <img class="mobile" src="assets/images/footer.svg" alt="TurboCX - footer image mobile">
</div>

<script src="assets/js/vendor.js<?php echo $version; ?>" defer></script>
<!-- <script src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js" defer></script> -->
<!-- <script src="https://cdn.jsdelivr.net/npm/fuse.js@6.5.3" defer></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js" defer></script> -->
<script src="assets/js/script.min.js<?php echo $version; ?>" defer></script>
<script defer src="https://widget.turbocx.com/widget.js" id="whatcx-widget-script" data-id="62e91067c7689fff8a5765da" data-nonce="1651121319443_1a4ffe36e2ce4f27d5ff81749ac901232d5511680c09ac9c75e521325d38b816"></script>

<!-- <script src="assets/js/custom.js"></script> -->
<!-- <script>
  function delay(n) {
  n = n || 2000;
  return new Promise((done) => {
    setTimeout(() => {
      done();
    }, n);
  });
}

function pageTransition() {
  const tl = gsap.timeline();
  tl.to(".loading-screen", {
    duration: 0.7,
    width: "100%",
    left: "0%",
    ease: "Expo.easeInOut",
   
  });
  tl.to(".loading-screen--plane", {
    duration: 0.7,
   background:"yellow",
   top:0,
   scale:1.5,
    left: "120%",
    ease: "Expo.easeInOut",
   
   
  });

  tl.to(".loading-screen", {
    duration: 0.7,
    width: "100%",
    left: "100%",
    ease: "Expo.easeInOut",
    delay: 0.3,
    
  });
  tl.set(".loading-screen", { left: "-100%" });
  tl.set(".loading-screen--plane", { left: "-100%" });
}

$(function () {
  barba.init({
    sync: true,
    views: [
      {
        namespace: "home-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
        
      },
      {
        namespace: "portfolio-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
      },
      {
        namespace: "incubation-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
      },
      {
        namespace: "contact-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
      },
      {
        namespace: "singleProject-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
      },
      {
        namespace: "about-section",
        beforeEnter({ next }) {
          Array.from(document.querySelectorAll("script")).forEach((script) => {
            if (script.src.includes("assets/js/script.js")) {
              script.parentNode.removeChild(script);
            }
          });
          let script = document.createElement("script");
          script.src = "assets/js/script.js";
          next.container.parentNode.appendChild(script);
        },
      },
    ],

    transitions: [
      {
        async leave(data) {
          const done = this.async();
          pageTransition();
          await delay(700);
          done(); 
        },
        async enter(data) {
         console.log("Enter")
        },

        async beforeOnce(data) {
        console.log("Before Once")
        },
        async beforeEnter(data) {
         console.log("Before Enter")
        },
        async once(data) {},
      },
    ],
  });
});

</script> -->

</body>

</html>