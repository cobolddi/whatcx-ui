<!-- testimonial -->
  <!-- <section class="Section ppTestSection b2cTestimonial">
    <div class="container-small">
      <div class="ppTest">
        <div class="ppTestSlide">
          <button class="ppTestSlide-btnPrev">
            <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M6.93825 12.2001L16.6605 2.54513L14.0976 0L0 14L14.0976 28L16.6605 25.4549L6.93825 15.7999H29V12.2001H6.93825Z"
                fill="white" />
            </svg>

          </button>
          <div class="ppTestSlide-slider">
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
          </div>
          <button class="ppTestSlide-btnNext">
            <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M22.0618 12.2001L12.3395 2.54513L14.9024 0L29 14L14.9024 28L12.3395 25.4549L22.0618 15.7999H0V12.2001H22.0618Z"
                fill="white" />
            </svg>
          </button>

        </div>
      </div>
    </div>
  </section> -->
  <!--  -->
