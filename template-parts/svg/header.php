<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>TurboCX</title>
  <link rel="preconnect" href="https://fonts.googleapis.com" />
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
  <link href="https://fonts.googleapis.com/css2?family=DM+Sans:ital,wght@0,400;0,500;0,700;1,400&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="assets/css/main.min.css" />
</head>

<body class="light">

  <nav>
    <div class="nav">
      <div class="nav-logo">
        <?php @include('template-parts/svg/whatcx-logo.php') ?>
      </div>
      <div class="nav-links">
        <ul>
          <li><a href=""> Home</a></li>
          <li>
            <span>
              Products
            </span>
            <ul>
              <li>
                <span>
                  he
                </span>
                <span>
                  <h5>Capabilities</h5>
                  <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
              </li>
              <li>
                <span>
                  he
                </span>
                <span>
                  <h5>Capabilities</h5>
                  <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
              </li>
              <li>
                <span>
                  he
                </span>
                <span>
                <h5>Capabilities</h5>
                <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
               
              </li>
            </ul>
          </li>
          <li>
            <span>
              Solutions
            </span>
            <ul>
              <li>
                <span>
                  he
                </span>
                <span>
                  <h5>Capabilities</h5>
                  <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
              </li>
              <li>
                <span>
                  he
                </span>
                <span>
                  <h5>Capabilities</h5>
                  <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
              </li>
              <li>
                <span>
                  he
                </span>
                <span>
                  <h5>Capabilities</h5>
                  <p>Explore what TurboCX can do for you out of the box.</p>
                </span>
              </li>
            </ul>
          </li>
          <li><a href="">Pricing</a></li>
          <li><a href="">Contact</a></li>
          <li><a href="">Resources</a></li>
        </ul>
      </div>
      <div class="nav-actions">
        <div class="nav-actions--darkSwitch">
          <button>
            <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M15.8835 12.0194C15.7492 11.8666 15.5316 11.8179 15.3449 11.8987C14.5227 12.2552 13.6481 12.4359 12.7454 12.4359C9.13761 12.4359 6.20245 9.50076 6.20245 5.89299C6.20245 4.16218 6.87211 2.53034 8.08811 1.29797C8.23083 1.15329 8.26411 0.933099 8.17045 0.752725C8.07676 0.57235 7.87767 0.472726 7.67708 0.506444C5.8203 0.817193 4.12064 1.78063 2.89117 3.21928C1.6477 4.67421 0.962891 6.53095 0.962891 8.44745C0.962891 9.40347 1.12877 10.3339 1.44617 11.2064C2.08405 10.6641 2.88608 10.3683 3.73127 10.3683C4.66227 10.3683 5.54105 10.7273 6.20561 11.3792C6.59855 11.7647 6.89152 12.2271 7.06886 12.7369C7.8743 13.1603 8.39992 14.0011 8.39992 14.9437C8.39992 15.4979 8.21801 16.0103 7.91105 16.4247C8.27405 16.4742 8.6427 16.4999 9.01536 16.4999C10.4346 16.4999 11.8301 16.1254 13.051 15.4169C14.235 14.7297 15.2319 13.745 15.9339 12.5691C16.0382 12.3944 16.0178 12.1723 15.8835 12.0194Z" fill="#181717"/>
              <path d="M5.90605 16.4999H1.5562C0.698085 16.4999 0 15.8018 0 14.9437C0 14.2164 0.501462 13.6041 1.1768 13.4343C1.27189 12.9101 1.52592 12.4285 1.91332 12.0484C2.40153 11.5695 3.04712 11.3057 3.73114 11.3057C4.41516 11.3057 5.06075 11.5695 5.54896 12.0484C5.93633 12.4284 6.19036 12.9101 6.28548 13.4343C6.96082 13.6041 7.46228 14.2164 7.46228 14.9437C7.46225 15.8018 6.76413 16.4999 5.90605 16.4999V16.4999Z" fill="#181717"/>
            </svg>
          </button>
        </div>
        <div class="nav-actions--login">
          <a href="">Login</a>
        </div>
        <div class="nav-actions--signUp">
          <a href="" class="secondaryBtn">Sign up for Free</a>
        </div>
      </div>

    </div>
  </nav>