<ul class="blogSingle__social">
    <li>
    <a href="https://wa.me/?text=" data-action="share/whatsapp/share" target="_blank" rel="nofollow" title="share on WhatsApp">
    <img loading="lazy" src="assets/images/blog/whts.svg" ></a>
    </li>
    
    <li><a href="https://www.facebook.com/sharer/sharer.php?u=" rel="nofollow" target="_blank" title="share on Facebook"><img loading="lazy" src="assets/images/blog/fb.svg"></a></li>

    <li><a href="https://www.linkedin.com/sharing/share-offsite/?url=" rel="nofollow" target="_blank" title="share on Linkedin"><img loading="lazy" src="assets/images/blog/linked.svg"></a></li>

    <li><a href="https://www.instagram.com/TurboCXofficial/?url=" rel="nofollow" target="_blank" title="share on instagram"><img loading="lazy" src="assets/images/blog/insta.svg"></a></li>
    <li><a id="shareCopy" href="" rel="nofollow" target="_blank" title="Copy Link"><img loading="lazy" src="assets/images/blog/slide.svg">
    <div class="copyText">Copied</div>
    </a></li>
</ul>