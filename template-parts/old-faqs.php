    <!-- faqs -->
    <!-- <section class="Section faqs-section">
        <div class="container">
            <div class="faqs__head centerSectionHeading">
                <h3>If you have any questions, please read through these common queries first or
                    <span class="text--orange"><a href="contact">contact us.</a></span>
                </h3>
            </div>

            <div class="faqs__wrapper faqs_single">

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What is TurboCX?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>TurboCX is an all-in-one business WhatsApp tool. It helps you manage your customers on WhatsApp by bringing your whole team together, on one WhatsApp number for your customer’s ease.</p>
                    </div>
                </div>
                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How can TurboCX help my business grow?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>TurboCX uses one of the most popular personal communication apps, WhatsApp. Most if not all your customers are probably on WhatsApp already. So of course it’s the natural channel businesses want to use to engage customers. But managing your conversations on WhatsApp is not easy. That’s where TurboCX comes in. TurboCX packs in features like rich text support, live chat, unified dashboard, customer profile, private notes, collaboration on chat etc.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Is TurboCX part of WhatsApp?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>TurboCX is an application that uses WhatsApp. We use the official Whatsapp Cloud API, but WhatsApp does not certify our services. We are an independent providers of this service for a fee. </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Can more than one agent respond to WhatsApp messages using TurboCX?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes! You can bring your whole team together on TurboCX, sales, marketing and customer service - on one WhatsApp number! Multiple agents can easily collaborate on a single WhatsApp conversation as well using TurboCX.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What do you mean by a WhatsApp conversation?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>
                            WhatsApp Conversations are an important term because your business is charged based on conversations, not on the number of messages sent. Any message sent in a 24 hour time-window is regarded as a conversation. When the first message is delivered - whether sent by you or your customer - that is when WhatsApp considers a conversation (or session) to have started.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What is WhatsApp Conversation Based pricing?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>
                            Once the conversation is initiated the session is considered to be active. You are charged upon message delivery. The pricing is split into two heads, depending on who has initiated the conversation.
                        </p>
                        <ol>
                            <li>Business initiated conversation</li>
                            <li>Customer initiated conversation</li>
                        </ol>
                        <p><a href="contact">Connect with us</a> to know more about Conversation Based Pricing.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>When do session windows start in a conversation? What does it mean for my business?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Conversations or sessions start when the message is delivered. When your business responds to a message sent by your customer, that 24 hour conversation window or session goes live. This is important because your business can only communicate in free form conversation with customers during such conversations or sessions.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>When does my business get charged? </h4>
                    </div>

                    <div class="accordion-body">
                        <p>Your business will be charged only once the message is delivered. During this active session you can send as many messages as you want - you will only be charged once during this conversation window.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How are businesses charged for messages? Is it charged for each message?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Businesses are charged for a conversation or session, instead of individual messages. When you initiate a conversation or reply to a customer, a 24 hour conversation window begins. Unlimited messages can be exchanged during this 24 hour time period. You are only charged once for the initial message depending on whether it was your business or the customer who initiated the conversation.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What is WhatsApp Template Message?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Outside of the 24 hour conversation window, businesses can only send messages to customers in a pre-approved template. These templates must be submitted from the TurboCX dashboard and approved by WhatsApp. Template are made up of set text with placeholders variables.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What is the pricing you follow?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>TurboCX has plans for all business sizes. For more information, see our <a href="pricing.php">pricing page</a>.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Can I change from one plan to the other?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes. You can always migrate between plans. <a href="contact">Contact us</a> for more information.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>I want to downgrade plans. Will I get a refund?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes, you will receive a refund on a pro-rata basis according to the usage months and the cost incurred for the initial setup. For more details kindly reach out to us.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Can I cancel my subscription?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes. That’s right. However, the number registered with WhatsApp Cloud API is non-transferable. You won’t be able to downgrade back to a standard WhatsApp Business Account.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How does TurboCX’s free trial work?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Our free trial brings you all of the powerful features of TurboCX for you to test and explore. However, you are limited to sending messages to 10 contacts. This list of contacts is refreshed every hour.
                        </p>
                        <p>You’ll be able to use our WhatsApp number to receive messages from your customers, to interact with them and set up auto-responders to see TurboCX in action.</p>
                    </div>
                </div>


                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What methods of payment are accepted?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>You can recharge your wallet on TurboCX by using credit cards, debit cards, net banking, and check.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>I have paid my annual subscription, why do I need a monthly invoice? Why do I need balance in my wallet?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Your subscription to TurboCX is a lumpsum payment. But to send messages and communicate with your customers you need to maintain a wallet balance because WhatsApp messages are billed on actuals.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>I want to increase the number of agents that can login to TurboCX? What are the charges?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>You can upgrade your plan, or contact us for a customized solution.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Does TurboCX offer training in using and help for setting up the WhatsApp channel for business?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes. With the TurboCX managed service you get all the help you need. With this optional service, we can help you setup your Whatsapp Cloud API, content and template strategy, and even help with the Green Tick Verification.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How can I get the WhatsApp Business Verified Green Tick?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>The green tick means that you have an Official Business Account on WhatsApp. This green tick gives your business credibility and lets customers see your business profile, even if they have not saved your phone number as a contact.
                        </p>
                        <p>Applying for the green tick is free, however, approval is solely at WhatsApp’s discretion. We can help you with the process and our expertise can come in handy. But we can’t influence WhatsApp’s decision. Green tick verification is based on many factors, so to know more, please get in touch with us.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Is TurboCX available on mobile?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes. We have a companion mobile app where you can see & assign chats, check active chats, start free form conversations, view contacts, edit customer profiles, and do much more.
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>If I have an existing WhatsApp Business Account, can I use it with TurboCX?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes. But you’ll have to delete the old Business Account. This is an irreversible step, so please backup your chats and contacts. Kindly contact us to learn more.
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </section> -->
