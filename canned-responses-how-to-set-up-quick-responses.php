<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX: Whatsapp message templates</title>
    <meta name="description" content="How quickly can you respond to a customer? How can you set up quick and easy responses that provide good service while they are on the phone with you?
">
    <meta property="og:title" content="Whatsapp message templates" />
    <meta property="og:description" content="How quickly can you respond to a customer? How can you set up quick and easy responses that provide good service while they are on the phone with you?
" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://www.turbocx.com/canned-responses-how-to-set-up-quick-responses.php" />   
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.turbocx.com/assets/images/blog/single/communication.webp">
    <meta property="og:image:secure_url" content="https://www.turbocx.com/assets/images/blog/single/communication.webp" />
    <meta property="og:image:alt" content="Whatsapp message templates


" />
  
    <link rel="canonical" href="https://www.turbocx.com/canned-responses-how-to-set-up-quick-responses.php" />
<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="https://turbocx.com/blogs.php"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Resources</a></li>
                <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog Detail</a></li>
            </ul>
        </div>
    </section>


    <!-- blog detail section -->
    <section class="Section blogSingle">
        <div class="container-small">

            <h1 class="blogSingle__heading">Whatsapp message templates: Best practices to follow</h1>

            <div class="blogSingle__area">
                <div class="blogSingle__date">
                    <span>1/01/2022</span>
                </div>

                <?php @include('template-parts/share-icons.php');?>

            </div>

            <div class="blogSingle-image">
                <img  src="assets/images/blog/single/communication.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
            </div>

            <article class="blogSingle__content">

                <p>A brand is defined by its customer experience. Delivering maximum value to your customers is not easy
                    as it seems. Having multiple contacts, handling multiple repetitive queries is really difficult
                    tasks for any service team. Most of the customers don’t have the patience to wait for the replies
                    while they are having a conversation. More than 70% of consumers (age 16-24) believe that a <a
                        href="https://www.comm100.com/resources/infographic/millennials-prefer-live-chat-speed-convenience/"
                        target="_blank">quick reply from a service team </a>can drastically improve their customer
                    experience.
                </p>

                <p>
                    Moreover, every customer is unique so are their requirements and issues. Sending the same messages
                    having similar tones to every customer is not the best thing to do. Customer service demands
                    personalized conversations.
                </p>

                <p>
                    Keeping your responses short and precise while adding personalization to them delivers maximum
                    satisfaction to your customers. Making use of structured and standardized replies in your
                    conversations saves an ample amount of time as well.
                </p>

                <h3>What are canned responses?</h3>

                <p>
                    Canned responses are predetermined answers or messages for the frequently asked or common questions
                    related to sales, customer support, feedback, or marketing. These prompt responses engage customers
                    better during an ongoing conversation to deliver a faster & consistent response. Rather than typing
                    out the exact same information hundreds of times every time, create an automatic live chat answer
                    for each basic question you’re commonly asked.
                </p>

                <h3>How to create quick replies?</h3>

                <p>You can create canned responses/quick replies on <a  href="https://turbocx.com/" target="_blank">TurboCX</a> by following these:</p>

                <ol>
                    <li>Login to your TurboCX account.</li>
                    <img loading="lazy" src="assets/images/blog/single/signup.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>Click <strong>Users</strong> from the left side of the screen & Choose <strong>Teams.</strong>
                    </li>
                    <img loading="lazy" src="assets/images/blog/single/Roles&permission.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>You can either create teams or edit existing teams. If you choose to create teams, you will
                        visit Add New Team</li>
                    <img loading="lazy" src="assets/images/blog/single/Teams.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>Fill the required fields</li>
                    <img loading="lazy" src="assets/images/blog/single/Add_Teams.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>Click on the Quick replies</li>
                    <img loading="lazy" src="assets/images/blog/single/quickreplies.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>Here you can start adding quick replies. You can add multiple quick replies as per your
                        requirement. You can also remove the replies if required with Remove quick replies button.</li>
                    <img loading="lazy" src="assets/images/blog/single/add quick replies.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                    <li>Here is the final result. You have finally generated replies for your account.</li>
                    <img loading="lazy" src="assets/images/blog/single/Conversation.webp" alt="TurboCX - Whatsapp message templates: Best practices to follow">
                </ol>

                <h3>Use cases of quick replies</h3>

                <p>Canned responses are not made to replace the human element in a conversation. As a way of saving time
                    and speeding up the responses, they’re brilliant. It doesn’t have to be anything time-consuming:
                    just a small personalized touch will go a long way. Think of a canned response as a foundation to
                    build on. There are various examples of canned responses that can be used in industries:</p>

                <h3>Jewellery Industry</h3>

                <ol>
                    <li>Hie, Welcome to<a href="https://turbocx.com/solution-jewellery.php" target="_blank"> Vasundhra Jewellers</a>. How can we help you today?</li>
                    <li>Please select from the options below to continue:
                        <ol type="a">
                            <li>Latest offers</li>
                            <li>Latest collection</li>
                            <li>Talk to the expert</li>
                            <li>Main menu</li>
                        </ol>
                    </li>
                    <li>Please check out from latest offers</li>
                    <li>Thank you for showing interest to learn more about our offers. You can find all of our offers
                        here.</li>
                    <li>Dear [name], Thank you for reaching out and sharing your opinion on the experience with us.</li>
                </ol>

                <h3>Apparel Industry</h3>

                <ol>
                    <li>Hello (customer name), Thank you for reaching out to Westside, How may we help you today?</li>
                    <li>Thanks for getting in touch, glad we could help you, (customer_name). Let us know if you need
                        anything else. Have a great day!</li>
                    <li>Our business hours are from 10:00 am to 8:00 pm. Please leave a message and we will get back to
                        you at the earliest. Till then please take a look at our catalogue.</li>
                    <li>Thank you so much for your valuable feedback. We hope you continue choosing us and give us a
                        chance to deliver you a great experience.</li>
                    <li>Dear (name), Thank you for being with us. We are currently offering a 20% discount on all items.
                        Would you like to know more? Click the link below to view more details.</li>
                </ol>

                <h3>Wrapping up</h3>

                <p>The best part of having quick replies for your customers is that you can personalize them according
                    to every customer. With <a href="https://turbocx.com/" target="_blank">TurboCX</a>, generating quick responses has
                    been much easier. Start sending quick responses and serve your customers better.</p>

            </article>


        </div>
    </section>
    <!-- blog detail section -->


    <?php @include('template-parts/blog-cards.php') ?>


</main>

<?php @include('template-parts/footer.php') ?>