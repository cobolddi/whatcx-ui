<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX: Onboarding your team on TurboCX
</title>
    <meta name="description" content="TurboCX helps to promote your business with more effective communication with your customers living in the UK and worldwide.
">
    <meta property="og:title" content="Onboarding your team on TurboCX

" />
    <meta property="og:description" content="TurboCX helps to promote your business with more effective communication with your customers living in the UK and worldwide.
" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/onboarding-your-team-on-whatcx.php/" />   
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.turbocx.com/assets/images/blog/single/onboarding-your-team-on-whatcx-blog-6.webp">
    <meta property="og:image:secure_url" content="https://www.turbocx.com/assets/images/blog/single/onboarding-your-team-on-whatcx-blog-6.webp" />
    <meta property="og:image:alt" content="Onboarding your team on TurboCX

" />
  
    <link rel="canonical" href="https://turbocx.com/onboarding-your-team-on-whatcx.php/" />
<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="https://turbocx.com/blogs.php"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog</a></li>
                <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog Detail</a></li>
            </ul>
        </div>
    </section>


    <!-- blog detail section -->
    <section class="Section blogSingle">
        <div class="container-small">

            <h1 class="blogSingle__heading">Onboarding your team on TurboCX</h1>

            <div class="blogSingle__area">
                <div class="blogSingle__date">
                    <span>14/03/2022</span>
                </div>

                <?php @include('template-parts/share-icons.php'); ?>

            </div>

            <div class="blogSingle-image">
                <img  src="assets/images/blog/single/onboarding-your-team-on-whatcx-blog-6.webp" alt="TurboCX - Onboarding your team on TurboCX">
            </div>

            <article class="blogSingle__content">

                <p>The majority of the businesses are using WhatsApp to create effective communication with their customers. It is one of the most accessible platforms to create sales funnel as well as resolve customer queries. Moreover, 80% of <a href="https://www.freshworks.com/live-chat-software/customer-engagement/latest-whatsapp-statistics-blog/">small and medium businesses</a> believe WhatsApp helps in effectively communicating with customers.
                </p>

                <p>
                    Once you get WhatsApp Cloud API for your business, the next step becomes onboarding your team on it. Setting up your team for Whatsapp Cloud API is the most essential aspect of taking your business on WhatsApp. It involves a significant amount of time, an elaborate procedure, and a platform.
                </p>

                <p>
                    Although this whole process demands very little effort on your part, <a href="https://turbocx.com/">TurboCX</a> will require your simple assistance with the onboarding process. Before beginning the setup process, you need to have an account with us.
                </p>
                <img loading="lazy" src="assets/images/blog/blog-6/1.signup.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <h3>How to set up your team with us?</h3>
                <p>
                    You can add a new team, members and assign a team manager from Add Team option.
                </p>
                <img loading="lazy" src="assets/images/blog/blog-6/2.add-new-team.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>You can easily add a new user or assign a role to them with Add a new user option on user view. </p>
                <img loading="lazy" src="assets/images/blog/blog-6/3.add new user.webp" alt="TurboCX - Onboarding your team on TurboCX">
                
                <p>
                Super admin can edit the details of the users or users themselves can change their passwords or edit their profile. The process of deactivating a user can only be performed by the super admin or other members than the member himself.
                </p>
                <img loading="lazy" src="assets/images/blog/blog-6/4.users.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>Once contacts and users are added, you can view the team with their names, manager, member, and status clearly.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/5.teams.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>The template messages play a key role in situations where a team member is not available or customer contacts you on non-working days. You can add multiple quick replies, add templates as per your feasibility.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/6.quick replies.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>You can activate mail requests for your team manager when there is no one available to respond to customer queries.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/7. template.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>Roles & Permissions allows you to assign roles to your team members and give them permissions to access features accordingly.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/8.roles and permissions.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>You can add a role anytime or choose permissions for that particular role from Add Role option.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/9.add roles.webp" alt="TurboCX - Onboarding your team on TurboCX">

                <p>A user can edit his profile and update his role at any time with the Edit User option.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/10.edit users.webp" alt="TurboCX - Onboarding your team on TurboCX">
                    
                <p>A team member can choose templates and add quick replies himself from the conversation module.</p>
                <img loading="lazy" src="assets/images/blog/blog-6/11. conversation.webp" alt="TurboCX - Onboarding your team on TurboCX">


                <h3>Wrapping Up</h3>
                    
                <p>Who doesn’t prefer simpler and more effective solutions? </p>

                <p>Yes, it is that easy to set up your team on <a href="https://turbocx.com/">TurboCX</a> - a WhatsApp Cloud API solution. You can do it in very little time.</p>
                
                <p>
                TurboCX’s conversational solutions automate almost everything including contacts management, team onboarding, conversations, broadcast, chats, and much more. If you’ve ever wanted a solution that is consistent, context-driven, easily compatible, and successful, you’re in the right place. 
                </p>
                <p>Level up your conversations with the all-in-one platform and deliver comprehensive solutions for your business. </p>

            </article>


        </div>
    </section>
    <!-- blog detail section -->


    <?php @include('template-parts/blog-cards.php') ?>


</main>

<?php @include('template-parts/footer.php') ?>