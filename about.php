<!DOCTYPE html>
<html lang="en">

<head>
  <title>About Us-whatcx</title>
  <meta name="description"
    content="Start communicating officially on WhatsApp and improve customer satisfaction. Get a multi-agent mobile app, broadcast on WhatsApp, official WhatsApp number, unlimited broadcasts on WhatsApp.">
  <meta name="keywords" content="about TurboCX,about whatsapp crm,what is whatsapp cloud api">
  <meta property="og:title" content="About Us-whatcx" />
  <meta property="og:description"
    content="Start communicating officially on WhatsApp and improve customer satisfaction. Get a multi-agent mobile app, broadcast on WhatsApp, official WhatsApp number, unlimited broadcasts on WhatsApp." />

  <meta property="og:url" content="https://turbocx.com/about/" />
  <link rel="canonical" href="https://turbocx.com/about/" />

  <?php @include('template-parts/header.php') ?>

  <main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
      <div class="container">
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="#"> <img src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> About</a>
          </li>
        </ul>
      </div>
    </section>

    <section class="Section about-banner">
      <div class="container">
        <div class="ab-animation">
          <div class="circle-1">
            <img src="assets/images/about/circle.svg">
            <p>WhatsApp</p>
          </div>
          <div class="btw-img">
            <img src="assets/images/about/ani-img.svg">
          </div>
          <div class="circle-2">
            <img src="assets/images/about/circle.svg">
            <p>Customer<br> Experience</p>
          </div>
        </div>
        <div class="about-htxt centerSectionHeading">
          <h1>About us</h1>
          <p>WhatsApp Marketing is a <b>buzz</b> these days and why not? Meta has officially announced the WhatsApp Cloud API allowing businesses to send and receive messages using the cloud-hosted version of its WhatsApp Business API. What if we told you that TurboCX started building on the same premises before Meta announced it? Yes, TurboCX is a powerful Customer relationship management tool that helps marketing, sales, and customer support teams build a deeper relationship with their contacts on WhatsApp and nurture leads to generate more marketing qualified leads. It is software that helps marketing teams manage, automate and scale thousands of conversations daily on the world's best chat platforms.</p>
        </div>
      </div>
    </section>
    <section class="Section">
      <div class="container">
        <div class="ap-box leftRightGrid">
          <div class="leftRightGrid-content ap-box-content">
            <h2>Manage your customers. Officially now on WhatsApp. </h2>
            <p>Our mission is to make you succeed in your conversational marketing strategy on WhatsApp powered by WhatsApp Cloud API. Don’t believe us?  </p>
            <a href="#" class="primaryBtn">Start a trial</a>
          </div>
          <div class="leftRightGrid-image">
            <img src="assets/images/solutions/spManageCustomer-image.png">
          </div>
        </div>
      </div>
    </section>

    <section class="b2cClient-section hpClients-Section">
      <div class="container">
        <div class="hpClients">
          <h5>Join hundreds of businesses who use TurboCX for customer success.</h5>
          <div class="hpClients--logos scroll-logos">
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/1.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/2.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/3.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/4.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/5.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/6.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/7.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/8.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/9.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/10.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/11.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/12.png" alt="" class="w--100">
            </div>
            <div class="logoWrap">
              <img loading="lazy" src="assets/images/about/logos/13.png" alt="" class="w--100">
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="Section timeline-section">
      <div class="container">
        <div class="tl-txt">
          <h2>How did we get here?</h2>
          <p>When did we realize conversational marketing was crucial for businesses?</p>
        </div>
        <div class="timeline-box">
          <ul>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box">
                  <!-- <h5>hhh</h5> -->
                  <p>Do we seem a very lateral entry into the industry? No, we’re not…We are the same young and ambitious team of Cobold Digital, a full-service digital agency that helped companies to maintain online identity and transform businesses with cloud-first technology solutions. Though we’ve got energetic minds joining us at TurboCX.</p>
                </div>
                <div class="tl-num">
                  <p>01</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box">
                  <p>As eCommerce has grown in popularity over the past five years, conversational commerce has become an increasingly common buzzword. At its core, conversational commerce refers to brands asking customers questions, leaving reviews, checking order status, or making purchases through channels like SMS, WhatsApp, social DMs, or voice. </p>
                </div>
                <div class="tl-num">
                  <p>02</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box">
                  <p>With such channels being where consumer audiences already hang out, we realized there is a huge potential for building long-term relationships with customers and eventually selling to them.</p>
                </div>
                <div class="tl-num">
                  <p>03</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box">
                  <p>During the pandemic, CRM adoption boomed in emerging markets. But the current CRMs were not suited for chat. We believed that there is only one way to successfully position yourself in the market: “Best customer experience”. </p>
                </div>
                <div class="tl-num">
                  <p>04</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box ">
                  <p>There were already major players in this space: Interakt, Wati, and Twilio but we entered the market on 21st may’ 2022 with the only mission of bringing back “relationships” into Customer Relationship Management with chat commerce & leaving customers “Happy” “Loyal” & “Repeat”.</p>
                </div>
                <div class="tl-num">
                  <p>05</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box ">
                  <p>Since then, we have only scaled upwards. As a company, we live by our mission, and we strive to improve business conversations & help marketing teams manage, automate and scale thousands of conversations daily on the world's best chat platforms. </p>
                </div>
                <div class="tl-num">
                  <p>06</p>
                </div>
              </div>
            </li>
            <li>
              <div class="timeline-li">
                <div class="tl-txt-box ">
                  <p>As well as building a platform and products we believe in, we are building a team of people who are curious, creative, and want to do the best work of their lives. We live by getting the job done by connecting your conversations to the tools and services you use. Currently, we're the only CRM that fully supports WhatsApp Cloud API.</p>
                </div>
                <div class="tl-num">
                  <p>07</p>
                </div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>

    <section class="Section ab-slider-sec">
      <div class="container">
        <div class="ab-slide-txt">
          <h2>What else should you know</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua.</p>
        </div>



        <div class="slideshow-container">
          <div class="ab-mySlides fade">
            <div class="grid-three-ab">
              <div class="card-ab-box">
                <img src="assets/images/about/5-days.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>5 days work week</h5>
                  <p>We believe that reduced hours, generally result in healthier, happier, and less burned-out employees.</p>
                </div>
                <!-- <div class="overlay-ab-card">
                  <div class="text-overlay">
                    <h3>Ownership Culture</h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                      iure tenetur architecto molestiae ea modi iste.</p>
                  </div>
                </div> -->
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/Remote-first.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Remote-first company</h5>
                  <p>We’re a team of remote workers who work from around the world. We believe in employee productivity whether it comes from working on mountains or beaches.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/Unbossing.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Unbossing the bossy culture</h5>
                  <p>We strive to unleash the power of our people through an inspired, curious and unbossed culture.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/Ownership-culture.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Managers take the responsibility for their teams, eliminating unnecessary checks at TurboCX.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/No-strict.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>No strict leave policy</h5>
                  <p>We understand the responsibilities people have in remote setups so we don’t have strict leave policies in place.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/Week-of-Gratitude.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Learn & Grow</h5>
                  <p>We have created a space to grow, nurture and boost our employee capabilities by encouraging everyone who wants to learn & ideate.</p>
                </div>
              </div>
            </div>
          </div>

          <a class="prev"><img src="assets/images/about/left-arrow.svg"></a>
          <a class="next"><img src="assets/images/about/right-arrow.svg"></a>

        </div>
      </div>
    </section>

    <section class="Section">
      <div class="container-small">
        <div class="ab-founder">
          <div class="L-founder">
            <img src="assets/images/about/founder.png" class="w--100">
          </div>
          <div class="R-founder">
            <p class="fs-2">Himanshu Sukhwani</p>
            <p class="fs-5">Founder of TurboCX</p>
            <p>Our founder, Himanshu realized the potential of conversational commerce in the SaaS industry. After leading successful ventures like Cobold Digital and building two agencies from scratch, he diverted his energies towards creating software where customer experience can be truly defined. His aim is to ensure that your customer is delighted when they reach you on WhatsApp. He founded TurboCX (The best customer experience on WhatsApp) in January 2022 as an official WhatsApp marketing platform and has since grown it into a platform that has helped multiple companies accelerate their growth & ace their conversational marketing strategy. </p>
            <div class="founder-social">
              <ul>
                <li><a href="">
                    <svg width="45" height="45" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path class="color-new-stroke"
                        d="M24.4992 48.0336C18.2576 48.0336 12.2717 45.5541 7.85821 41.1407C3.44475 36.7272 0.965332 30.7413 0.965332 24.4997V24.4997C0.965332 18.2581 3.44475 12.2722 7.85821 7.85869C12.2717 3.44524 18.2576 0.96582 24.4992 0.96582V0.96582C30.7408 0.96582 36.7267 3.44524 41.1401 7.85869C45.5536 12.2722 48.0331 18.2581 48.0331 24.4997V24.4997C48.0331 30.7413 45.5536 36.7272 41.1401 41.1407C36.7267 45.5541 30.7408 48.0336 24.4992 48.0336V48.0336Z"
                        stroke="#222221" stroke-width="1.5" stroke-miterlimit="10" />
                      <path class="color-new-bg" fill-rule="evenodd" clip-rule="evenodd"
                        d="M37.4179 23.8431C37.305 20.573 35.9339 17.4728 33.5907 15.1891C31.2475 12.9053 28.113 11.6145 24.8411 11.5857C21.5692 11.5569 18.4125 12.7926 16.0295 15.0348C13.6465 17.277 12.2211 20.3526 12.0508 23.6202C12.0508 23.7936 12.0508 23.9918 12.0508 24.1652C12.0517 26.4624 12.6814 28.7155 13.8716 30.6804L11.543 37.4309L18.5783 35.2014C20.5055 36.2561 22.673 36.7937 24.8696 36.7619C27.0663 36.7301 29.2172 36.13 31.113 35.0199C33.0088 33.9099 34.5848 32.3278 35.6875 30.4277C36.7903 28.5276 37.3821 26.3743 37.4055 24.1776C37.4158 24.0664 37.4199 23.9547 37.4179 23.8431ZM24.7344 34.7555C22.6483 34.7622 20.6064 34.1547 18.8632 33.009L14.7634 34.2476L16.0887 30.3212C14.7766 28.5221 14.0658 26.3548 14.0574 24.1281C14.0574 23.7936 14.0574 23.4468 14.0574 23.1C14.3223 20.4469 15.5702 17.9888 17.5559 16.2094C19.5415 14.4299 22.121 13.4578 24.7872 13.4842C27.4534 13.5106 30.0132 14.5336 31.9632 16.352C33.9132 18.1705 35.1123 20.6527 35.3246 23.3105C35.3246 23.583 35.3246 23.8556 35.3246 24.1281C35.3115 26.9478 34.1796 29.6469 32.1776 31.6326C30.1756 33.6183 27.4674 34.7282 24.6476 34.7183L24.7344 34.7555Z"
                        fill="#222221" />
                      <path fill-rule="evenodd" clip-rule="evenodd" class="color-new-bg"
                        d="M30.5566 26.7168C30.2346 26.5558 28.7111 25.8126 28.4263 25.7135C28.1414 25.6144 27.9308 25.5525 27.7202 25.8621C27.5097 26.1718 26.9151 26.8654 26.7293 27.1008C26.5435 27.3361 26.3702 27.3361 26.0481 27.1875C25.1393 26.8236 24.3009 26.3038 23.5709 25.6516C22.8903 25.0314 22.3059 24.3135 21.8368 23.5212C21.7714 23.4193 21.7491 23.2956 21.7746 23.1772C21.8001 23.0589 21.8714 22.9554 21.973 22.8895C22.134 22.7285 22.2826 22.5179 22.4437 22.3445C22.4885 22.3012 22.5262 22.2511 22.5552 22.1959C22.6308 22.0773 22.697 21.9531 22.7533 21.8243C22.8024 21.7419 22.8283 21.6477 22.8283 21.5518C22.8283 21.4558 22.8024 21.3617 22.7533 21.2793C22.679 21.1307 22.0597 19.6071 21.7996 18.9878C21.5395 18.3685 21.2794 18.48 21.0936 18.48C20.9078 18.48 20.7096 18.48 20.499 18.48C20.3406 18.484 20.1849 18.5214 20.0419 18.5897C19.8989 18.658 19.7719 18.7557 19.6692 18.8764C19.3191 19.207 19.0416 19.6067 18.854 20.0502C18.6664 20.4936 18.5728 20.9712 18.5792 21.4526C18.5894 21.8089 18.6434 22.1625 18.7402 22.5055C18.9862 23.2784 19.3633 24.0032 19.855 24.6483C21.1509 26.6714 22.9932 28.2861 25.1687 29.3056C28.3395 30.5442 28.3395 30.123 28.8845 30.0735C29.3176 29.9934 29.7285 29.8216 30.0895 29.5695C30.4506 29.3174 30.7535 28.9909 30.9778 28.6119C31.1891 28.1512 31.254 27.6367 31.1636 27.138C31.0645 26.9769 30.8663 26.8778 30.5566 26.7168Z"
                        fill="#222221" />
                    </svg>
                  </a></li>
                <li><a href="">
                    <svg width="45" height="45" viewBox="0 0 49 49" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <path class="color-new-stroke"
                        d="M24.4992 48.0336C18.2576 48.0336 12.2717 45.5541 7.85821 41.1407C3.44475 36.7272 0.965332 30.7413 0.965332 24.4997V24.4997C0.965332 18.2581 3.44475 12.2722 7.85821 7.85869C12.2717 3.44524 18.2576 0.96582 24.4992 0.96582V0.96582C30.7408 0.96582 36.7267 3.44524 41.1401 7.85869C45.5536 12.2722 48.0331 18.2581 48.0331 24.4997V24.4997C48.0331 30.7413 45.5536 36.7272 41.1401 41.1407C36.7267 45.5541 30.7408 48.0336 24.4992 48.0336V48.0336Z"
                        stroke="#222221" stroke-width="1.5" stroke-miterlimit="10" />
                      <g clip-path="url(#clip0_1791_10540)">
                        <path class="color-new-bg"
                          d="M26.5219 36.88H21.177V19.6246H26.2972V21.9374C26.67 21.5012 26.982 21.0656 27.3643 20.7041C28.3073 19.8113 29.4604 19.3466 30.7421 19.2289C31.861 19.1257 32.9718 19.208 34.0509 19.5492C35.8192 20.1089 36.859 21.3581 37.3806 23.0935C37.7293 24.2534 37.854 25.4475 37.8622 26.6486C37.885 29.9763 37.8761 33.3041 37.8793 36.6324C37.8793 36.6932 37.8685 36.754 37.8609 36.83H32.5116C32.5116 36.7015 32.5116 36.5793 32.5116 36.4571C32.5104 33.6029 32.518 30.7487 32.5003 27.8952C32.4965 27.2981 32.4427 26.6941 32.3357 26.1066C32.0104 24.3212 30.8541 23.7146 29.199 23.9159C27.7022 24.0977 26.8813 24.9777 26.6522 26.6011C26.5687 27.193 26.5301 27.7958 26.5276 28.3941C26.5149 31.09 26.5219 33.7859 26.5219 36.4824C26.5219 36.6065 26.5219 36.7306 26.5219 36.8806V36.88Z"
                          fill="black" />
                        <path d="M12.4558 19.624H17.7798V36.8674H12.4558V19.624Z" class="color-new-bg" fill="black" />
                        <path class="color-new-bg"
                          d="M18.2081 14.1024C18.2138 15.8195 16.8221 17.2377 15.124 17.2459C13.4183 17.2535 11.9975 15.8252 12 14.105C12.0025 12.4107 13.3949 11.0121 15.0905 11.0001C16.8012 10.988 18.2024 12.3822 18.2081 14.1024Z"
                          fill="black" />
                      </g>
                      <defs>
                        <clipPath id="clip0_1791_10540">
                          <rect width="25.88" height="25.88" fill="white" transform="translate(12 11)" />
                        </clipPath>
                      </defs>
                    </svg>
                  </a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class=" timeline-section">
      <div class="container-small">
        <div class="tl-txt">
          <h2>Our Team</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
            dolore magna aliqua. </p>
        </div>
        <div class="team-sec">
          <div class="team-mem-card">
            <img src="assets/images/about/mem-1.png" class="w--100">
            <div class="mem-overlay">
              <h5>Rakesh Kr Chaudhary</h5>
              <div class="BB-p"><p>Customer Success Manager</p></div>
              <p>"Rakesh brings to the table more than four years of experience in backend development and team management. In his role, he onboards the customers to solve their everyday problems to deliver the experience they deserve & acts as a bridge between the product & customer success."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-2.png" class="w--100">
            <div class="mem-overlay">
              <h5>Hammad Akhtar</h5>
              <div class="BB-p"><p>Lead Frontend developer</p></div>
              <p>"Hammad, with his four years of experience primarily in creating UI/UX for apps, is working as a lead frontend developer with TurboCX where he coordinates with the backend team, conducts testing drills, and makes the system scalable & reliable for delivering the best customer experience."</p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-3.png" class="w--100">
            <div class="mem-overlay">
              <h5>Khalid Raza Khan</h5>
              <div class="BB-p"><p>Lead backend developer</p></div>
              <p>"Khalid is a lead backend developer who brings to the table more than fours of experience. In his role, he is responsible for managing the entire backend development of TurboCX. His job allows him to blend his passion to build values alongside the software to create remarkable experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-4.png" class="w--100">
            <div class="mem-overlay">
              <h5>Shifali Aggarwal</h5>
              <div class="BB-p"><p>Project Manager</p></div>
              <p>"Shifali manages the projects of TurboCX with her four years of client servicing & project management experience to help clients reach their ultimate goals. She loves to use her background in client servicing to truly deliver what TurboCX stands for - The best customer experience."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-1.png" class="w--100">
            <div class="mem-overlay">
              <h5>Rishab Verma</h5>
              <div class="BB-p"><p>Senior Visual Designer</p></div>
              <p>"Rishab brings to the table three years of experience in utilizing his skills to create user-friendly projects with modern aesthetics. He is a senior visual designer who understands the importance of hitting the target market and always keeps the customer experience in mind with his intuitive designs."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-2.png" class="w--100">
            <div class="mem-overlay">
              <h5>Akash Pachauri</h5>
              <div class="BB-p"><p>Sr. Executive Motion Graphic</p></div>
              <p>"Akash creates highly strategic video content for clients that align with their marketing and sales goals. He has more than three years of experience in creating compelling videos to help tell their stories and showcase their products. He uses his experience to create remarkable visual experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-3.png" class="w--100">
            <div class="mem-overlay">
              <h5>Apurva Sharma</h5>
              <div class="BB-p"><p>Senior Marketing Executive</p></div>
              <p>"Apurva makes sure all deliverables for customers are accurate and compelling from a marketing strategist's perspective. In addition to undertaking all marketing strategies with the marketing team, she has been delivering remarkable customer experiences aligned with overall strategic company goals."</p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-4.png" class="w--100">
            <div class="mem-overlay">
              <h5>Abhisekh Behera</h5>
              <div class="BB-p"><p>UI/UX designer</p></div>
              <p>"Abhisekh uses his skills to take user experience to the next level with the latest design trends, and with a user-friendly aesthetic. With experience in working with startups on 3-d printing technology and boot camps, he works as a UI/UX designer in TurboCX & has been delivering his best to curate extraordinary experiences."</p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-1.png" class="w--100">
            <div class="mem-overlay">
              <h5>Aman Srivastava</h5>
              <div class="BB-p"><p>Junior Business Analyst</p></div>
              <p>"Aman is a junior business analyst at TurboCX. In his role, he identifies the customer needs and the larger business objectives that a product will fulfill and articulates what success looks like for a product. Also a founding member of a healthcare startup, he brings two years of product and growth marketing experience."</p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-2.png" class="w--100">
            <div class="mem-overlay">
              <h5>Devyani Keserwani</h5>
              <div class="BB-p"><p>Customer Success Manager</p></div>
              <p>"Devyani uses her customer success background to deliver an objectives-based onboarding process aimed at achieving the client's goals. With five years of experience in customer success and brand positioning, she combines her marketing knowledge with strategy, and execution skills to deliver best-in-class service."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-3.png" class="w--100">
            <div class="mem-overlay">
              <h5>Mrinal Thakur</h5>
              <div class="BB-p"><p>Website Developer</p></div>
              <p>"Mrinal has been crafting the most efficient codes with no digital concerns. With experience in backend and frontend development and expertise in WordPress, maintenance, UI & website development, he has been delivering his tasks with the sole objective of imparting the best customer experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-4.png" class="w--100">
            <div class="mem-overlay">
              <h5>Rohit Kumar</h5>
              <div class="BB-p"><p>Senior Accounts Executive</p></div>
              <p>"Rohit manages the accounting department of TurboCX. He brings to the table five years of experience in managing accounts. In his role, he also looks after the cohort analysis and SaaS metrics & strives toward creating remarkable customer experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-1.png" class="w--100">
            <div class="mem-overlay">
              <h5>Sumit Chauhan</h5>
              <div class="BB-p"><p>Sr Executive- Human Resources</p></div>
              <p>"Sumit helps develop valuable critical reports for our team, as well as guides the recruiting and interviewing process. He has 3 years of experience in talent acquisition which helps him to recruit the best talents to add value to the organization & live by the mission & vision of “Best customer experience”."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-2.png" class="w--100">
            <div class="mem-overlay">
              <h5>Rajat Tiwari</h5>
              <div class="BB-p"><p>Backend developer</p></div>
              <p>"With three years of experience in the developer field, Rajat is working as a Backend developer in TurboCX. In his role, he is responsible for testing, development, and bug fixing for the product. He is eager to learn & share his knowledge in the industry to craft extraordinary customer experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-3.png" class="w--100">
            <div class="mem-overlay">
              <h5>Dhaneshwar Umre</h5>
              <div class="BB-p"><p>Junior Backend developer</p></div>
              <p>"Dhaneshwar is working as a junior backend developer & has been solving dynamic programming problems with his expertise. With more than two years of experience in backend development and a driven attitude, he ensures to deliver of the best of experiences for the customers."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-4.png" class="w--100">
            <div class="mem-overlay">
              <h5>Suresh Kumar</h5>
              <div class="BB-p"><p>Graphic Designer</p></div>
              <p>"Suresh is working as a graphic designer at TurboCX. He creates social media assets to deliver effective, aesthetic, on-brand, and on-strategy interactive creative for social media. He brings more than three years of experience to the table and strives to live by the vision of TurboCX - The best customer experience."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-1.png" class="w--100">
            <div class="mem-overlay">
              <h5>Siddharth Soni</h5>
              <div class="BB-p"><p>Website Developer</p></div>
              <p>"Siddharth brings to the table one year of experience and expertise in frontend development, javascript, WordPress, HTML, and CSS. In his role, he is working on the interactive development and SEO of the website. The goal of his work is to provide customers with remarkable experiences."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-2.png" class="w--100">
            <div class="mem-overlay">
              <h5>Shweta Mehta</h5>
              <div class="BB-p"><p>Website Developer</p></div>
              <p>"Shweta brings to the table more than one year of experience in design & development. In her role at TurboCX, she has been working on the UI development and SEO of the website. Her work aims to deliver remarkable customer experiences by delivering results that surpass expectations."
              </p>
            </div>
          </div>
          <div class="team-mem-card">
            <img src="assets/images/about/mem-3.png" class="w--100">
            <div class="mem-overlay">
              <h5>Amir Adal</h5>
              <div class="BB-p"><p>Frontend Developer</p></div>
              <p>"Amir has joined TurboCX all the way from Turkey. With intensive expertise in programming and frontend development, he is working on the role of a Frontend developer with TurboCX. He combines his expertise and experience in ways that make customers relive remarkable experiences on WhatsApp."
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>


  </main>

  <?php @include('template-parts/footer.php') ?>