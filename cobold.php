<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX | We are the product of Cobold </title>
    <meta name="description" content="TurboCX is a product by Cobold Digital LLP">
    <meta property="og:title" content="TurboCX | We are the product of Cobold">
    <meta property="og:description" content="TurboCX is a product by Cobold Digital LLP">
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/cobold.php">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://cobold.in/wp-content/themes/cobold-digital/assets/img/hiring-logo.svg">
    <meta property="og:image:secure_url" content="https://cobold.in/wp-content/themes/cobold-digital/assets/img/hiring-logo.svg" />
    <meta property="og:image:alt" content="TurboCX | We are the product of Cobold" />
    <link rel="canonical" href="https://turbocx.com/cobold.php" />
<?php @include('template-parts/header.php') ?>

<main style="min-height: auto;">

    <section class="Section thankyou coboldp">
        <div class="container-small">
            <div class="thankyouG leftRightGrid">
                <div class="thankyouG-content leftRightGrid-content"> 
                    <!-- <div class="LogoContainer">
                        <a href="index.php">
                            <?php //@include('template-parts/svg/TurboCX-logo.php') ?>
                        </a>
                    </div> -->
                    <h3>TurboCX is a product by</h3>
                    <h3 class="thankyouG-dsc"> <a href="https://www.cobold.in/">Cobold Digital LLP</a></h3>
                    <p>
                        <a class="btn--text" href="index.php" tabindex="0">
                        Back to Home <img  src="assets/images/icons/arrow-orange.svg">
                            </a>
                        </p>
                </div>
                <div class="thankyouG-graphics leftRightGrid-image">
                    <img loading="lazy" src="assets/images/whatcx-coboldp.webp" alt="cobold">
                </div>
            </div>
        </div>
    </section>
</main>

<?php @include('template-parts/footer.php') ?>