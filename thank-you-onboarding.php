<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Onboarding-whatcx</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta property="og:title" content="Onboarding-whatcx" />
  <meta property="og:description" content="" />
  <link rel="stylesheet" href="assets/css/main.min.css" />
  <meta property="og:url" content="https://turbocx.com/onboarding/" />
  <link rel="canonical" href="https://turbocx.com/onboarding/" />
  <link rel="icon" href="assets/images/favicon.png" sizes="32x32" />
</head>

<body class="on-board-bg">
  <main>
    <section>
      <div class="container">
        <div class="onboard-box thank-you-onboarding">
          <div class="logo-onboard">
            <img src="assets/images/onboard-logo.svg">
          </div>
          <div class="thnx">
            <div class="thnx-img-1">
              <img src="assets/images/tq-1.png" alt="">
            </div>
            <h1 class="text-center fs-32 fw-600 txt-1 ">Thank you !!</h1>
            <p class="text-center txt-g fs-16 txt-75">for signing up with us. Our representative will soon get back to you for further steps.</p>
            <div class="thnx-img-2">
              <img src="assets/images/tq-2.png" alt="">
            </div>
          </div>


          <div class="onboard-foot">
            <p>It is our policy to protect your privacy and to keep all the information shared by you confidential. To visit our website, please click here. In case of any support, feel free to reach out to us at <a target="_blank" href="https://turbocx.com/">TurboCX</a>.</p>
          </div>
          <div class="foot-url">
            <a target="_blank" href="https://app.turbocx.com/login/">Login</a>
            <a target="_blank" href="https://www.turbocx.com/contact.php">Support</a>
          </div>
        </div>
      </div>
    </section>
  </main>
</body>