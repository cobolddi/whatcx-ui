<!DOCTYPE html>
<html lang="en">

<head>
    <title>WhatsApp business solutions at simple pricing model- TurboCX</title>
    <meta name="description" content="Manage unlimited contacts, access 25-500 GB storage, military-grade security, broadcasts, analytics, and green tick verification for WhatsApp crm.">
    <meta name="keywords" content="whatsapp business account price,whatsapp business pricing,whatsapp for business pricing,how to improve customer experience,whatsapp api pricing india,pricing of TurboCX">
    <meta property="og:title" content="WhatsApp business solutions at simple pricing model- TurboCX" />
    <meta property="og:description" content="Manage unlimited contacts, access 25-500 GB storage, military-grade security, broadcasts, analytics, and green tick verification for WhatsApp crm." />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/pricing.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://whatcx.com/assets/images/og/pricing-og.png">
    <meta property="og:image:secure_url" content="https://whatcx.com/assets/images/og/pricing-og.png" />
    <meta property="og:image:alt" content="WhatsApp business solutions at simple pricing model- WhatCX" />
    <link rel="canonical" href="https://whatcx.com/pricing.php/" />

    <?php @include('template-parts/header.php') ?>


    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#"> <img src="assets/images/icons/arrow-right.svg" alt="WhatCX - breadcrumbs icon"> Pricing</a></li>
            </ul>
        </div>
    </section>


    <!-- banner -->
    <section class="Section hpBanner-Section ppBanner-Section">
        <div class="container">
            <div class="hpBanner">
                <div class="centerSectionHeading">
                    <h1>Simple pay as you go pricing plans to suit every business</h1>
                    <p>
                        Let customers communicate with your sales, marketing & customer service teams easily and manage
                        business conversations effectively using WhatCX.
                    </p>
                </div>
            </div>
        </div>
    </section>

    <!-- desktop plans -->
    <section class="Section ppPlans-Section" id="plans">
        <div class="container-small">
            <div class="ppPlans">

                <div class="ppPlans-plansWrap">

                    <div class="ppPlans-outerGrid">

                        <div class="ppPlans-rows plansHeading">
                            <div class="ppPlans-rows--colms">
                                <img src="assets/images/icons/price-1.svg" alt="WhatCX - Basic" />
                                <h4>Basic</h4>
                                <p>If your business is just starting out using WhatsApp to build customer relationships then
                                    here’s some commendable firepower. </p>
                            </div>
                        </div>


                        <div class="ppPlans-rows plansPricing">

                            <div class="ppPlans-rows--colms">
                                <p>Starts from just</p>
                                <!-- <h3>Rs.2599<span>/month</span></h3> -->
                                <span>For small teams just starting out</span>
                                <div class="plansNPrice">
                                    <div>
                                        +
                                        <p>WhatsApp Conversation Costs
                                            <span>(billed on actuals from wallet)</span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="ppPlans-rows plansFeatures">

                            <div class="ppPlans-rows--colms">
                                <h5>
                                    <?php @include('template-parts/svg/icons/user.php') ?> 5 user
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/storage.php') ?>25 GB storage
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                                </h5>
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </div>
                        </div>
                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </div>
                        </div>
                        <!-- <div class="planName">
                            <span>
                            + BASIC
                            </span>
                        </div> -->

                        <div class="planCta">
                            <a href="tel:+919871069987" class="secondaryBtn">Start From 2199 / Monthly</a>
                        </div>


                    </div>

                    <div class="ppPlans-outerGrid ppPlans-outerGrid--Tag">

                        <div class="ppPlans-rows plansTag">
                            <p>Most Popular</p>
                        </div>

                        <div class="ppPlans-rows plansHeading">
                            <div class="ppPlans-rows--colms">
                                <img loading="lazy" src="assets/images/icons/price-2.svg" alt="WhatCX - Business" />
                                <h4>Business</h4>
                                <p>If your business is already some way along the WhatsApp-communication digital trail, here
                                    are resources to navigate this complex landscape.</p>

                            </div>
                        </div>


                        <div class="ppPlans-rows plansPricing">

                            <div class="ppPlans-rows--colms">
                                <p>Starts from just</p>
                                <!-- <h3>Rs.4899<span>/month</span></h3> -->
                                <span>For teams looking to grow fast</span>
                                <div class="plansNPrice">
                                    <div>
                                        +
                                        <p>WhatsApp Conversation Costs
                                            <span>(billed on actuals from wallet)</span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="ppPlans-rows plansFeatures">

                            <div class="ppPlans-rows--colms">
                                <h5>
                                    <?php @include('template-parts/svg/icons/user.php') ?> 25 user
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/storage.php') ?>100 GB storage
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                                </h5>
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </div>
                        </div>
                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </div>
                        </div>

                        <!-- <div class="planName">
                            <span>
                            + BUSINESS
                            </span>
                        </div> -->

                        <div class="planCta">
                            <a href="tel:+919871069987" class="secondaryBtn">Start From 5500 / Monthly</a>
                        </div>


                    </div>

                    <div class="ppPlans-outerGrid">

                        <div class="ppPlans-rows plansHeading">
                            <div class="ppPlans-rows--colms">
                                <img loading="lazy" src="assets/images/icons/price-3.svg" alt="WhatCX - Enterprise" />
                                <h4>Enterprise</h4>
                                <p>This is your weapon of choice if you want to go all out on the most popular messaging
                                    platform in the country!</p>
                            </div>
                        </div>


                        <div class="ppPlans-rows plansPricing">

                            <div class="ppPlans-rows--colms">
                                <p>Starts from just</p>
                                <!-- <h3>Rs.6999<span>/month</span></h3> -->
                                <span>For businesses ready to dominate</span>

                                <div class="plansNPrice">
                                    <div>
                                        +
                                        <p>WhatsApp Conversation Costs
                                            <span>(billed on actuals from wallet)</span>
                                        </p>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="ppPlans-rows plansFeatures">

                            <div class="ppPlans-rows--colms">
                                <h5>
                                    <?php @include('template-parts/svg/icons/user.php') ?> 100 user
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/storage.php') ?>500 GB storage
                                </h5>
                                <h5>
                                    <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                                </h5>
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </div>
                        </div>
                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </div>
                        </div>

                        <div class="ppPlans-rows featureRow">
                            <div class="ppPlans-rows--colms">
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </div>
                        </div>
                        <!-- <div class="planName">
                            <span>
                            + ENTERPRISE
                            </span>
                        </div> -->

                        <div class="planCta">
                            <a href="tel:+919871069987" class="secondaryBtn">Start From 25000 / Monthly</a>
                        </div>


                    </div>


                </div>

            </div>

        </div>
        </div>
    </section>

    <!-- mobile plans -->
    <section class="Section ppPlansMobileSection">
        <div class="container">
            <h3>Plans</h3>
            <div class="ppPlansMobile">
                <div class="ppPlansMobile-basic ppPlansMobile-plan">
                    <button>
                        <h3><img loading="lazy" src="assets/images/icons/price-1.svg" alt="WhatCX - Basic" />Basic</h3>
                        <p>
                            <!-- 2599/month -->
                            <?php @include('template-parts/svg/icons/arrow-down.php') ?>
                        </p>
                    </button>
                    <div class="ppPlansMobile-wrap">
                        <h4>If your business is just starting out using WhatsApp to build customer relationships then here’s
                            some commendable firepower.</h4>

                        <h5><em>For small teams just starting out</em></h5>
                        <p>
                            <?php @include('template-parts/svg/icons/user.php') ?> 5 user
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/storage.php') ?>25 GB storage
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                        </p>

                        <ul>
                            <span>Features</span>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </li>
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </li>
                        </ul>
                    </div>

                </div>
                <div class="ppPlansMobile-business ppPlansMobile-plan">
                    <button>

                        <h3> <img loading="lazy" src="assets/images/icons/price-2.svg" alt="WhatCX - BUSINESS" />BUSINESS</h3>
                        <p>
                            <!-- 4899/month -->
                            <?php @include('template-parts/svg/icons/arrow-down.php') ?>
                        </p>
                    </button>
                    <div class="ppPlansMobile-wrap">
                        <h4>If your business is already some way along the WhatsApp-communication digital trail, here are
                            resources to navigate this complex landscape.</h4>
                        <h5><em>For teams looking to grow fast
                            </em></h5>
                        <p>
                            <?php @include('template-parts/svg/icons/user.php') ?> 25 user
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/storage.php') ?>100 GB storage
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                        </p>
                        <ul>
                            <span>Features</span>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </li>
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="ppPlansMobile-enterprise ppPlansMobile-plan">
                    <button>
                        <h3> <img loading="lazy" src="assets/images/icons/price-3.svg" alt="WhatCX - Enterprise" />Enterprise</h3>
                        <p>
                            <!-- 6999/month -->
                            <?php @include('template-parts/svg/icons/arrow-down.php') ?>
                        </p>
                    </button>
                    <div class="ppPlansMobile-wrap">
                        <h4>This is your weapon of choice if you want to go all out on the most popular messaging platform
                            in the country!</h4>

                        <h5><em>For businesses ready to dominate</em></h5>
                        <p>
                            <?php @include('template-parts/svg/icons/user.php') ?> 100 user
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/storage.php') ?>500 GB storage
                        </p>
                        <p>
                            <?php @include('template-parts/svg/icons/contact.php') ?>Contact Management
                        </p>

                        <ul>
                            <span>Features</span>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Dashboard
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Analytics
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Security
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Contact Management
                            </li>
                            </li>
                            <li>
                                <?php @include('template-parts/svg/icons/tick.php') ?> Unlimited Broadcast
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <?php @include('template-parts/enterpriseSection.php') ?>

    <!-- faqs -->
    <section class="Section faqs-section ppFaqs noPaddingTop">
        <div class="container">
            <div class="faqs__head centerSectionHeading">
                <h2>FAQ'S
                </h2>
            </div>

            <div class="faqs__wrapper faqs_single">

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How can WhatCX help my business?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>WhatsApp is a widely-used communication platform. Probably, all your customers are already using it. But managing customer relationships on WhatsApp is not as easy as it sounds, that’s where WhatCX comes in. It helps you put your entire team on one WhatsApp number (using the official WhatsApp Cloud API), helps you apply for a verified green tick for your business WhatsApp account, and integrates smoothly with your existing systems. In fact, you get a lot more than this! For more details, you can call us at +919871069987</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Is integrating WhatsApp Cloud API a complex process?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>The team at WhatCX does it for you. You don’t have to worry about the WhatsApp Cloud API process. We need just a few details from you. You will get all the features in place without any hassles. </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Can I use my existing WhatsApp number?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes, you can use your number. The only requirement is that you have to delete your account before onboarding with WhatCX. Your existing number will be officially marked as a Whatsapp Cloud API number after we connect it with WhatCX.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What additional services does WhatCX provide?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>
                            With WhatCX, you get a fully managed service. Meaning you’ll not have to worry about setting up your WhatsApp Business, figure out length submissions to WhatsApp, etc. Here are the optional services you can get with WhatsApp.
                        <ol>
                            <li>Onboarding & Verification for Whatsapp Cloud API</li>
                            <li>Green Tick Verification Service (this is an optional service, please connect with us to know more)</li>
                            <li>Template & Content Strategy Service</li>
                            <li>Team Training Sessions - WhatsApp best practises and using TurboCX effectively For more details, please can call us on +919871069987.</li>
                        </ol>
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How can I add funds to my Wallet?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>You can easily add credit to your wallet from our Billing Dashboard using credit/debit card, check, DD or online fund transfer. Once you add funds you can start broadcasting and using WhatCX.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Why do I need to maintain Wallet balance?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Wallet funds are required for the messages to be delivered on WhatsApp. These charges are pre-determined, are based on WhatsApp fees and are billed on actuals at the time of sending messages. To know more about Whatsapp conversation and messaging charges, please click here (hyperlink: https://developers.facebook.com/docs/whatsapp/pricing/).</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How many languages are available? </h4>
                    </div>

                    <div class="accordion-body">
                        <p>WhatCX offers vernacular support and messages to your customers can be sent in many popular Indian languages. But right now the user interface is available only in English.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>Can multiple employees collaborate on one WhatsApp chat?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Yes, definitely. The chat can be assigned to single or multiple agents/employees, or once assigned the agent can add other agents to the chat as well.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What are template messages?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>These are the specific message formats that have preset text and variables, used by businesses to send out messages to existing customers or new customers. These include feedback messages or customer care messages. These must be approved by WhatsApp before use.</p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>What is WhatsApp’s policy on doing business on this channel?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>You need to comply with WhatsApp policies, guidelines and terms of use prescribed by WhatsApp.
                        <ol>
                            <li><a href="https://www.whatsapp.com/legal/business-solution-terms">WhatsApp Business Terms</a></li>
                            <li><a href="https://www.whatsapp.com/legal/business-policy">WhatsApp Policy</a></li>
                            <li><a href="https://www.whatsapp.com/legal/commerce-policy/">WhatsApp commerce policy</a></li>
                        </ol>
                        </p>
                    </div>
                </div>

                <div class="accordion">
                    <div class="accordion-heading">
                        <h4>How do I start using WhatCX?</h4>
                    </div>

                    <div class="accordion-body">
                        <p>Simply contact us at <a href="mailto:sales@whatcx.com">sales@whatcx.com</a> or call at +91-9871069987</a></p>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- Faq's section ends -->

    </div>
    </div>
    </section>
    <!-- Faq's section ends -->

    <?php @include('template-parts/testimonialSection.php') ?>

    <?php @include('template-parts/brochureSection.php') ?>

    <?php @include('template-parts/footer.php') ?>