<!DOCTYPE html>
<html lang="en">

<head>
    <title>Turbocx</title>

<?php @include('template-parts/header.php') ?>

<main>

  <!-- breadcrumbs -->
  <section class="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="https://turbocx.com/blogs.php"> <img  src="assets/images/icons/arrow-right.svg" alt="Turbocx - breadcrumbs icon"> Blog</a></li>
        <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="Turbocx - breadcrumbs icon"> Blog Detail</a></li>
      </ul>
    </div>
  </section>  

        
    <!-- blog detail section -->
    <section class="Section blogSingle">
        <div class="container-small">
            
            <h1 class="blogSingle__heading">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
            </h1>

            <div class="blogSingle__area">    
                <div class="blogSingle__date">
                    <span>1/01/2022</span>
                </div>

                <?php @include('template-parts/share-icons.php');?>
                
            </div>

            <div class="blogSingle-image">
                <img src="assets/images/blog/banner.png" alt="blog-banner">
            </div>

            <article class="blogSingle__content">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. 
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor 
                </p>
                <p>
                    incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
            </article>

            
        </div>
    </section>
    <!-- blog detail section -->


    <?php @include('template-parts/blog-cards.php') ?>


    <!-- <div class="leftRightGrid-content recent">
        <h3 class="recent__head">Recent Blogs</h3>

        <div class="recent__list">
            <div class="recent__list--date">
                Lorem ipsum | 1/01/2022
            </div>
            <div class="recent__list--heading">
                How Are Insurance Chatbots Transforming Customer Experience in the Insurance Industry?
            </div>
            <a href="#" class="btn btn--text btn--orange">
                Read more <img loading="lazy" src="assets/images/blog/btn-arrow.svg" alt="">
            </a>
        </div>
        
        <div class="recent__list">

            <div class="recent__list--date">
                Lorem ipsum | 1/01/2022
            </div>
            <div class="recent__list--heading">
                How Are Insurance Chatbots Transforming Customer Experience in the Insurance Industry?
            </div>
            <a href="#" class="btn btn--text btn--orange">
                Read more <img loading="lazy" src="assets/images/blog/btn-arrow.svg" alt="">
            </a>

        </div>

        <div class="recent__list">

            <div class="recent__list--date">
                Lorem ipsum | 1/01/2022
            </div>
            <div class="recent__list--heading">
                How Are Insurance Chatbots Transforming Customer Experience in the Insurance Industry?
            </div>
            <a href="#" class="btn btn--text btn--orange">
                Read more <img loading="lazy" src="assets/images/blog/btn-arrow.svg" alt="">
            </a>

        </div>

    </div> -->

</main>

<?php @include('template-parts/footer.php') ?>
