<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TurboCX - Official WhatsApp Sales & Marketing.</title>
    <link rel="icon" href="../assets/images/favicon.png" sizes="32x32" />
    <link rel="stylesheet" href="../assets/css/main.min.css" />
</head>

<body class='light'>

    <!-- notification slider -->
    <section class="notification">
        <div class="container">

            <div class="notification__wrapper">
                <div class="notificationSlider">
                    <div class="notificationSlider__item">
                        <p>Header announcement area. This will have a vertically scrolling slider for more than 1
                            announcements.</p>
                    </div>
                    <div class="notificationSlider__item">
                        <p>Header announcement area. This will have a vertically scrolling slider for more than 1
                            announcements.</p>
                    </div>
                    <div class="notificationSlider__item">
                        <p>Header announcement area. This will have a vertically scrolling slider for more than 1
                            announcements.</p>
                    </div>
                </div>

                <img loading="lazy" src="../assets/images/icons/close.svg" alt="close">
            </div>

        </div>
    </section>


    <nav class="closed">
        <div class="container">
            <div class="nav nav-js">
                <div class="nav-logo">
                    <a href="../index.php">
                        <?php @include('../template-parts/svg/whatcx-logo.php') ?>
                    </a>
                    <button class="navHamBtn" id="toggle">
                        <div class="one"></div>
                        <div class="two"></div>
                        <div class="three"></div>
                    </button>
                </div>
                <div class="nav-links">
                    <ul>
                        <li class="active"><a href="../index.php"> Home</a></li>
                        <li>
                            <span>
                                Products
                            </span>
                            <ul>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/calendar.php') ?>
                                    </span>
                                    <span>
                                        <a href="../benefits.php">
                                            <h5>Benefits</h5>
                                            <p>Explore how TurboCX can benefit your business.</p>
                                        </a>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/git.php') ?>
                                    </span>
                                    <span>
                                        <a href="../features.php">
                                            <h5>Features</h5>
                                            <p>Get best-in-class solutions to grow your business.</p>
                                        </a>
                                    </span>

                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/stack.php') ?>
                                    </span>
                                    <span>
                                        <a href="../how-it-works.php">
                                            <h5>How it works</h5>
                                            <p>Learn how TurboCX works to get started in a few easy steps.</p>
                                        </a>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/vector.php') ?>
                                    </span>
                                    <span>
                                        <a href="../book-a-demo.php">
                                            <h5>Book a free demo</h5>
                                            <p>Schedule a free demo with us today.</p>
                                        </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <span>
                                <a href="../solutions.php">Solutions</a>
                            </span>
                            <ul>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/calendar.php') ?>
                                    </span>
                                    <span>
                                        <a href="../solution-jewellery.php">
                                            <h5>Jewellery</h5>
                                            <p>Find more deals and close more leads for your business. </p>
                                        </a>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/git.php') ?>
                                    </span>
                                    <span>
                                    <a href="../solution-real-estate.php">
                                        <h5>Real Estate</h5>
                                        <p>Excel in customer and client servicing with TurboCX.</p>
                                    </a>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/stack.php') ?>
                                    </span>
                                    <span>
                                        <a href="../solution-educational.php">
                                            <h5>Education</h5>
                                            <p>Enhance learning experiences and automate communication.</p>
                                        </a>
                                    </span>
                                </li>
                                <li>
                                    <span>
                                        <?php @include('../template-parts/svg/icons/vector.php') ?>
                                    </span>
                                    <span>
                                    <a href="../solution-apparel.php">
                                        <h5>Apparel</h5>
                                        <p>Provide convenience to your customers and boost conversions.</p>
                                    </a>
                                    </span>
                                </li>
                            </ul>
                        </li>
                        <li><a href="../pricing.php">Pricing</a></li>
                        <li><a href="../contact.php">Contact</a></li>
                        <li><a href="../blogs.php">Resources</a></li>
                    </ul>
                </div>
                <div class="nav-actions">
                    <!-- <div class="nav-actions--darkSwitch">
                        <label>
                            <input type="checkbox" checked>
                            <div class="planet">
                            </div>
                            <div class="elements">
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                                <svg version="1.1" viewBox="0 0 500 500" xmlns="http://www.w3.org/2000/svg">
                                    <circle cx="250" cy="250" r="200" />
                                </svg>
                            </div>
                        </label>
                    </div> -->
                    <div class="nav-actions--login">
                        <a href="">Login</a>
                    </div>
                    <!-- <div class="nav-actions--signUp">
                        <a href="sign-up" class="primaryBtn">Sign up for free Demo</a>
                    </div> -->
                </div>

            </div>
        </div>
    </nav>


<main style="min-height: auto;">

    <section class="Section thankyou">
        <div class="container-small">
            <div class="thankyouG leftRightGrid">
                <div class="thankyouG-content leftRightGrid-content"> 
                    <!-- <div class="LogoContainer">
                        <a href="../index.php">
                            <?php //@include('../template-parts/svg/whatcx-logo.php') ?>
                        </a>
                    </div> -->
                    <h1>Thank you!</h1>
                    <p>Congratulations! Your account has been successfully created. Please check your mail for login credentials.</p>
                    <p><a class="btn--text" href="../index.php" tabindex="0">
                        Back to Home <img loading="lazy" src="../assets/images/icons/arrow-orange.svg">
                    </a></p>
                </div>
                <div class="thankyouG-graphics leftRightGrid-image">
                    <img loading="lazy" src="../assets/images/thanks.webp" alt="">
                </div>
            </div>
        </div>
    </section>

</main>

<footer class="Section footer-Section">
  <div class="container">
    <div class="footer fourColWithCenteredOrphans">
          <div class="colItems footer-logo">
              <?php @include('../template-parts/svg/whatcx-logo.php') ?>
              <div class="footer-logo--social">
                <ul>
                  <li><a href="https://wa.me/+919871069987?text=" target="_blank"><img loading="lazy" src="../assets/images/icons/Group 54.svg" alt=""></a></li>
                  <li><a href="https://www.linkedin.com/company/TurboCX/" target="_blank"><img loading="lazy" src="../assets/images/icons/linkedin.svg" alt=""></a></li>
                  <li><a href="" target="_blank"><img loading="lazy" src="../assets/images/icons/Group 56.svg" alt=""></a></li>
                  <li><a href="" target="_blank"><img loading="lazy" src="../assets/images/icons/Group 58.svg" alt=""></a></li>
                </ul>
              </div>
            </div>

            <div class="colItems footerCompany">
                <h5>Solutions</h5>
                <ul>

                    <li><a href="../solution-jewellery.php">Jewellery </a></li>
                    <li><a href="../solution-real-estate.php">Real Estate</a></li>
                    <li><a href="../solution-educational.php">Education </a></li>
                    <li><a href="../solution-apparel.php">Apparel</a></li>

                </ul>
            </div>

            <div class="colItems footerProduct">
                <h5>Product</h5>
                <ul>
                    <li><a href="../benefits.php"> Benefits</a></li>
                    <li><a href="../features.php">Features</a></li>
                    <li><a href="../how-it-works.php">How it works</a></li>
                    <li><a href="../pricing.php">Pricing </a></li>

                </ul>
            </div>

            <div class="colItems footerPolicies">
                <h5>Company</h5>
                <ul>
                    <li><a href="../book-a-demo.php">Book a free demo</a></li>
                    <li><a href="../contact.php">Contact Us</a></li>
                    <li><a href="../blogs.php">Resources</a></li>
                </ul>
            </div>

            <div class="colItems footerPolicies">
                <h5>Privacy</h5>
                <ul>
                    <li><a href="../">Disclaimer</a></li>
                    <li><a href="../">Terms of Use</a></li>
                    <li><a href="../">Privacy Policy</a></li>
                    <li><a href="../">Copyright Notice</a></li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<div class="footerBottom">
    <img loading="lazy" src="../assets/images/footerBottom.png" alt="">
</div>


<script src="../assets/js/vendor.js" defer></script>
<script src="https://unpkg.com/isotope-layout@3.0.6/dist/isotope.pkgd.min.js" defer></script>
<script src="../assets/js/script.min.js" defer></script>



</body>

</html>