<?php
 
if($_POST) {
  $to = "sales@whatcx.com.com";
  $email = filter_var($_POST["email"], FILTER_SANITIZE_EMAIL);
  $subject= filter_var($_POST["subject"], FILTER_SANITIZE_STRING);
  $body = "Email: $email\n$subject";
  $from = 'noreply@whatcx.com';

  $headers = 'From: ' .$from . "\r\n". 
  'Reply-To: ' . $email. "\r\n" . 
  'X-Mailer: PHP/' . phpversion();
 
  if(@mail($to, $subject, $body, $headers)) {
    $output = json_encode(array('success' => true));
    die($output);
  } else {
    $output = json_encode(array('success' => false));
    die($output);
  }
}