<?php

function get_client_ip()
{
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else if (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else if (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else if (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else if (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
        $tmpip = explode(",", $ipaddress);
        return trim($tmpip[0]);
    } else {
        $ipaddress = 'UNKNOWN';
    }

    return $ipaddress;
}

$servername = "localhost";
$database   = "whatcx_onboarding";
$username   = "newcobold";
$password   = "Gurgaon2022@#$%";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);

$cname = filter_var($_POST['cname'], FILTER_SANITIZE_STRING);
$gstno = filter_var($_POST['gstno'], FILTER_SANITIZE_STRING);
$raddress = filter_var($_POST['raddress'], FILTER_SANITIZE_STRING);
$caddress = filter_var($_POST['caddress'], FILTER_SANITIZE_STRING);
$name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
$mobile = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
$femail = filter_var($_POST['femail'], FILTER_SANITIZE_STRING);
$pincode = filter_var($_POST['pincode'], FILTER_SANITIZE_STRING);
$city = filter_var($_POST['city'], FILTER_SANITIZE_STRING);
$state = filter_var($_POST['state'], FILTER_SANITIZE_STRING);
$country = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
$agreement1 = filter_var($_POST['agreement1'], FILTER_SANITIZE_STRING);
$agreement2 = filter_var($_POST['agreement2'], FILTER_SANITIZE_STRING);
$ip = get_client_ip();
date_default_timezone_set('Asia/Kolkata');
$date = date('Y-m-d h:i:s');

$sqlquery = "INSERT into onboarding_form (company_name,gst_no,registerd_address,correspondence_address,name,mobile_no,email,founder_email,pincode,city,state,country,agreement1,agreement2,date_time,ip_address) VALUES ('$cname','$gstno','$raddress','$caddress','$name','$mobile','$email','$femail','$pincode','$city','$state','$country','$agreement1','$agreement2','$date','$ip')";

if (mysqli_query($conn, $sqlquery)) {
    echo "success";
} else {
    echo "fail";
    print_r(mysqli_error($conn));
}

$to = "mrinal@cobold.in";
$subject = "Onboarding Form";
$txt = "Company Name: " . $cname . " <br/> GST/TGS ID: " . $gstno . " <br/> Registerd Address: " . $raddress . " <br/> Correspondence Address: " . $caddress . " <br/> Name: " . $name . " <br/> Mobile no: " . $mobile . " <br/> Email: " . $email . " <br/> Founder's Email: " . $femail . " <br/> Pincode: " . $pincode . " <br/> City: " . $city . " <br/> State: " . $state . " <br/> Country: " . $country .  " <br/> Checkbox 1: " . $agreement1 . " <br/> Checkbox 2: " . $agreement2;

// echo $txt;
$headers = "From: noreply@whatcx.com" . "\r\n"; //."CC: sales@whatcx.com";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

mail($to, $subject, $txt, $headers);

header("Location: ../thank-you-onboarding.php");
