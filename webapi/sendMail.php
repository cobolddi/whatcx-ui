<?php
$data = json_decode(file_get_contents('php://input'), true);

$name = filter_var($data['name'], FILTER_SANITIZE_STRING);
$email = filter_var($data['email'], FILTER_SANITIZE_STRING);
$mobile = filter_var($data['mobile'], FILTER_SANITIZE_STRING);
$country = filter_var($data['country'], FILTER_SANITIZE_STRING);
$companyName = filter_var($data['companyName'], FILTER_SANITIZE_STRING);
$websiteURL = filter_var($data['websiteURL'], FILTER_SANITIZE_STRING);
$utm_source = 'TurboCX';
$utm_medium = 'Header';
$utm_campaign = 'Signup';
$date = date("d/m/Y");

//save in sheet/excel
$workflowurl = "https://workflow.whatcx.com/webhook/3d8658e0-e569-44a6-b8d9-33ca49a845a91";
$workflowcurl = curl_init($workflowurl);
curl_setopt($workflowcurl, CURLOPT_URL, $workflowurl);
curl_setopt($workflowcurl, CURLOPT_POST, true);
curl_setopt($workflowcurl, CURLOPT_RETURNTRANSFER, true);
$workflowpostData = array(
    "name" => $name,
    "email" => $email,
    "mobile" => $country . $mobile,
    "companyName" => $companyName,
    "website" => $websiteURL,
    "utm_source" => $utm_source,
    "utm_medium" => $utm_medium,
    "utm_campaign" => $utm_campaign,
    "createdOn" => $date,
    "wa_opted_in" => true,
);
$workflowdata = json_encode($workflowpostData);
$workflowheaders = array(
    "Authorization: Basic bXJpbmFsOnJha2VzaA==",
    "Content-Type: application/json",
);
curl_setopt($workflowcurl, CURLOPT_HTTPHEADER, $workflowheaders);
curl_setopt($workflowcurl, CURLOPT_POSTFIELDS, $workflowdata);

$workflowresp = curl_exec($workflowcurl);
curl_close($workflowcurl);

var_dump($workflowresp);

?>
