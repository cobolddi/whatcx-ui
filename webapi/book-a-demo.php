<?php
$fname = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
$lname = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
$mobile = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
$country = filter_var($_POST['country'], FILTER_SANITIZE_STRING);
$cname = filter_var($_POST['cname'], FILTER_SANITIZE_STRING);
$websiteURL = filter_var($_POST['websiteURL'], FILTER_SANITIZE_STRING);
$numberOfEmployee = filter_var($_POST['numberOfEmployee'], FILTER_SANITIZE_STRING);
$message = filter_var($_POST['message'], FILTER_SANITIZE_STRING);
$to = "sales@whatcx.com";
$subject = "Booking for a TurboCX demo";
$txt = "Name: ".$fname." ".$lname." | Email:".$email." | Mobile: ".$country.$mobile." | Company Name:".$cname." | Website URL:".$websiteURL." | How many employee work there?:".$numberOfEmployee." | Message:".$message;
$headers = "From: noreply@whatcx.com"; //. "\r\n" ."CC: sales@whatcx.com";

mail($to,$subject,$txt,$headers);
header("Location: ../thankyou.php")
?>