<?php
$fname = filter_var($_POST['fname'], FILTER_SANITIZE_STRING);
$lname = filter_var($_POST['lname'], FILTER_SANITIZE_STRING);
$email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
$mobile = filter_var($_POST['mobile'], FILTER_SANITIZE_STRING);
$cname = filter_var($_POST['cname'], FILTER_SANITIZE_STRING);
$websiteURL = filter_var($_POST['websiteURL'], FILTER_SANITIZE_STRING);
// $help = filter_var($_POST['help'], FILTER_SANITIZE_STRING);
$to = "sales@whatcx.com";
$subject = "Sign up for a free trial";
$txt = "Name: ".$fname." ".$lname." | Email:".$email." | Mobile:".$mobile." | Company Name:".$cname." | Website URL:".$websiteURL;
$headers = "From: noreply@whatcx.com"; //. "\r\n" ."CC: sales@whatcx.com";

mail($to,$subject,$txt,$headers);
header("Location: ../thankyou.php")
?>