<div class="slideshow-container">
          <div class="ab-mySlides fade">
            <div class="grid-three-ab">
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-1.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-1.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
                <!-- <div class="overlay-ab-card">
                  <div class="text-overlay">
                    <h3>Ownership Culture</h3>
                    <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                      iure tenetur architecto molestiae ea modi iste.</p>
                  </div>
                </div> -->
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-2.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-3.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
            </div>
          </div>

          <!-- <div class="ab-mySlides fade">
            <div class="grid-three-ab">
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-4.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-5.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
              <div class="card-ab-box">
                <img src="assets/images/about/slide-ab-6.png" class="w--100">
                <div class="ab-bt-txt">
                  <h5>Ownership Culture</h5>
                  <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Distinctio ipsum ipsa facere magnam
                    iure tenetur architecto molestiae ea modi iste.</p>
                </div>
              </div>
            </div>
          </div> -->

          <a class="prev" onclick="plusSlides(-1)"><img src="assets/images/about/left-arrow.svg"></a>
          <a class="next" onclick="plusSlides(1)"><img src="assets/images/about/right-arrow.svg"></a>

        </div>