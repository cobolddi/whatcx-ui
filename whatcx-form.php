<!DOCTYPE html>
<html lang="en">
<head>
    <title>Whatcx: 404 | Not found</title>
    <meta name="description"
    content="Page not found | Whatcx."> 
    <meta property="og:title" content="Whatcx: 404 | Not found">
    <meta property="og:description" content="Page not found | Whatcx.">
    <meta property="og:site_name" content="WhatCX">
    <meta property="og:url" content="https://whatcx.com/404.php">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://whatcx.com/assets/images/og/logo-og.png">
    <meta property="og:image:secure_url" content="https://whatcx.com/assets/images/og/logo-og.png" />
    <meta property="og:image:alt" content="Whatcx: 404 | Not found" />
    <link rel="canonical" href="https://whatcx.com/404.php" />
    <style>
        input{
            color:black;
        }
        input[type="submit"]{
            background: red;
        }
    </style>
<?php @include('template-parts/header.php') ?>

<main class="not-found-main">
    <section >
        <div class="container">
            <div class="not-found">
            <form action="" > 
                <div>
                <label for="company-name">Company Name</label>
                <input type="text" name="company-name" id="company-name">
                </div>
                <div>
                <label for="gst">GST / Tax ID No.</label>
                <input type="text" name="Gst"id="gst">
                </div>
                <div>
                <label for="registered-address">Registered Address</label>
                <input type="text" name="Registered-Address"id="registered-address">
                </div>
                <div>
                <label for="Corrporate-address">Corrporate Address</label>
                <input type="text" name="Corrporate-Address"id="Corrporate-address">
                </div>
                <div>
                <label for="name">Name</label>
                <input type="text" name="name"id="name">
                </div>
                <div>
                <label for="mobile">mobile no.</label>
                <input type="text" name="mobile"id="mobile">
                </div>
                <div>
                <label for="email">Email</label>
                <input type="email" name="email"id="email">
                </div>
                <div>
                <label for="c-email">company-email</label>
                <input type="email" name="c-email"id="c-email">
                </div>
                <div>
                <input type="checkbox" name="xyz" id="xyz">
                <label for="xyz"> I authorised Lorem ipsum dolor sit amet.</label>
                <input type="checkbox" name="abc" id="abc">
                <label for="abc"> I agree Lorem ipsum dolor sit amet.</label>
                </div>
                <input type="submit" value="submit">
            </form>
            </div>
        </div>
    </section>
</main>
<?php @include('template-parts/footer.php') ?>

