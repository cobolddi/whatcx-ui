<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX</title>

<?php @include('template-parts/header.php') ?>

<main style="min-height: auto;">

    <section class="Section thankyou">
        <div class="container-small">
            <div class="thankyouG leftRightGrid">
                <div class="thankyouG-content leftRightGrid-content"> 
                    <!-- <div class="LogoContainer">
                        <a href="index.php">
                            <?php //@include('template-parts/svg/whatcx-logo.php') ?>
                        </a>
                    </div> -->
                    <h1>Thank you!</h1>
                    <p>We will get back to you shortly!</p>
                    <p><a class="btn--text" href="https://turbocx.com/" tabindex="0">
                        Back to Home <?php @include('assets/images/icons/arrow-orange.svg');?>
                    </a></p>
                </div>
                <div class="thankyouG-graphics leftRightGrid-image">
                    <img  src="assets/images/thanks.webp" alt="TurboCX - Thank You">
                </div>
            </div>
        </div>
    </section>

</main>

<?php @include('template-parts/footer.php') ?>