<!DOCTYPE html>
<html lang="en">
<head>
    <title>TurboCX: 404 | Not found</title>
    <meta name="description"
    content="Page not found | TurboCX."> 
    <meta property="og:title" content="TurboCX: 404 | Not found">
    <meta property="og:description" content="Page not found | TurboCX.">
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/404.php">
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/og/logo-og.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/logo-og.png" />
    <meta property="og:image:alt" content="TurboCX: 404 | Not found" />
    <link rel="canonical" href="https://turbocx.com/404.php" />
<?php @include('template-parts/header.php') ?>

<main class="not-found-main">
    <section >
        <div class="container">
            <div class="not-found">
            <img src="./assets/images/icons/404.svg" alt="">
            <h4>PAGE NOT FOUND</h4>
            <p>The page you are looking for might be removed, had its name changed or is temporarily unavailable</p>
            <a href="./index.php" class="secondaryBtn">Redirect to Home</a>
            </div>
        </div>
    </section>
</main>
<?php @include('template-parts/footer.php') ?>

