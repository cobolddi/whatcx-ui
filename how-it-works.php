<!DOCTYPE html>
<html lang="en">

<head>
    <title>A beginner's guide to crm integration with WhatsApp</title>
    <meta name="description" content="Organize conversations, increase sales, assign tasks, and set auto-respond bots to simplify communications with WhatsApp crm">
    <meta name="keywords" content="how whatsapp cloud api work,how whatsapp crm work,how TurboCX work,how to do whatsapp marketing">
    <meta property="og:title" content="A beginner's guide to crm integration with WhatsApp" />
    <meta property="og:description" content="Organize conversations, increase sales, assign tasks, and set auto-respond bots to simplify communications with WhatsApp crm" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/how-it-works.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/og/how-it-work-og.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/how-it-work-og.png" />
    <meta property="og:image:alt" content="A beginner's guide to crm integration with WhatsApp" />
    <link rel="canonical" href="https://turbocx.com/how-it-works.php/" />
    <?php @include('template-parts/header.php') ?>

    <main>
        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#"><img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon">Product</a></li>
                    <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> How it works</a></li>
                </ul>
            </div>
        </section>

        <section class="Section hiwBanner-section noPaddingBottom">
            <div class="container">
                <div class="hiwBannerG leftRightGrid">
                    <div class="hiwBannerG-content leftRightGrid-content">
                        <h1>How it works</h1>
                        <p>Peek under the hood to see what goes into powering up TurboCX for you business. See how you can scale your customer relationships using the power of technology and social connections with these simple steps. </p>
                    </div>
                    <div class="hiwBannerG-graphics leftRightGrid-image">
                        <picture>
                            <source media="(min-width:769px)" srcset="assets/images/hiw/hiw-banner.png">
                            <img  src="assets/images/hiw/hiw-banner-m.webp" alt="TurboCX - How it works">
                        </picture>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section hiwSteps-section noPaddingTop">
            <div class="container">
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-1.png" alt="TurboCX - Sign up for TurboCX">
                        </div>
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 1</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Sign up for TurboCX</h3>
                            <p>This is the easiest part to scaling your business! Simply click on the <a href="https://turbocx.com/sign-up.php" class="btn--text">sign-up</a>  button, fill in the details and you’re all set to go. </p>
                            <p>
                                Welcome email received! Done and dusted in under 2 mins. Now go ahead and connect your Business WhatsApp Phone Number.
                            </p>
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-1.svg" alt="TurboCX - Team" />
                            <h5>Team</h5>
                            <p>
                               <a href="https://turbocx.com/sign-up.php" class="btn--text">Bring your whole team onboard</a>, bucket by roles and give them required access.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-2.svg" alt="TurboCX - Contacts" />
                            <h5>Contacts</h5>
                            <p>
                                Long wait times and repeated follow up to get issues resolved
                                negatively affects customer retention.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-3.svg" alt="TurboCX - Group Views" />
                            <h5>Group Views</h5>
                            <p>
                                Split & segment your contact list by any combination of custom columns.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 2</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Connect Your Phone Number</h3>
                            <p>Ready to get one, centralized WhatsApp number for your whole team? Good, let’s get to it! </p>
                            <p>Input a valid number that can receive an OTP into TurboCX. That’s all! Now you can start sending text messages to a limited group of contacts. </p>
                        </div>
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-2.png" alt="TurboCX - Connect Your Phone Number">
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-4.svg" alt="TurboCX - VIP Numbers" />
                            <h5>VIP Numbers</h5>
                            <p>
                                Want to stand out from the crowd? Go in for our unique VIP phone numbers to be instantly
                                recognizable.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-5.svg" alt="TurboCX - Existing Accounts" />
                            <h5>Existing Accounts</h5>
                            <p>
                                Phone numbers active on WhatsApp will need to delete their existing WhatsApp account.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-6.svg" alt="TurboCX - Final Check" />
                            <h5>Final Check</h5>
                            <p>
                                Backup all your contacts & chats. Also note, you can’t downgrade back to standard WhatsApp.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-3.png" alt="TurboCX - Register for Whatsapp Cloud API">
                        </div>
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 3</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Register for Whatsapp Cloud API </h3>
                            <p>Register for Whatsapp's Cloud API to unlock unlimited access to the most used social platform in India.
                            </p>
                            <p>Plus, our White Glove Managed Services can make this step as easy as it can be - leaving you to focus on scaling your business with TurboCX.</p>
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-7.svg" alt="TurboCX - Verification Process" />
                            <h5>Verification Process</h5>
                            <p>
                                Use Facebook Business Manager Account & a website to verify that your business is in fact … yours (can take upto 4 weeks)
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-5.svg" alt="TurboCX - What You Get" />
                            <h5>What You Get</h5>
                            <p>
                                Get access to your WhatsApp Business Account (WABA) Profile, a unique WABA ID & WABA Key to access the <a href="https://turbocx.com/features.php" class="fw-bold">full suite of TurboCX’s features</a>.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-6.svg" alt="TurboCX - Documentation" />
                            <h5>Documentation</h5>
                            <p>
                                You can upload your Business bank statement, Business license, Certificate of incorporation, GST registration certificate, PAN card, Shop establishment certificate, Udyog Aadhar (UID), Utility bill, etc.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 4</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Start Using TurboCX to Grow Business</h3>
                            <p>Now that you’re here, it’s time to say ‘Hello World!’ Old customers, new customers or prospects, now reach them all with a few button taps.
                            </p>
                            <p>But new sales channels come with new responsibility. Abusing this tool will earn you penalties or bans - and stop you from applying for the all-powerful Green Tick Verification.</p>
                            <div>
                             <a href="https://turbocx.com/onboarding-your-team-on-whatcx.php" class="btn--text">Onboarding your team</a>,<br>
                                <a class="btn--text" href="https://turbocx.com/understanding-the-whatsapp-communication-channel-making-the-most-of-it.php" tabindex="0">
                                     Understanding the Whatsapp communication channel - making the most of it.
                                </a>
                            </div>
                        </div>
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-4.png" alt="TurboCX - Start Using TurboCX to Grow Business">
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-8.svg" alt="TurboCX - Optional Training" />
                            <h5>Optional Training</h5>
                            <p>
                                Opt-in for training on best practices for WhatsApp marketing, integration with existing
                                channels and using TurboCX effectively.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-5.svg" alt="TurboCX - Better Broadcasts" />
                            <h5>Better Broadcasts</h5>
                            <p>
                                WhatsApp enforces a cooling off period - staggering access to unlimited broadcasts
                                incrementally to curb abuse. We can help you through this.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-6.svg" alt="TurboCX - Live Sales" />
                            <h5>Live Sales</h5>
                            <p>
                                Don’t miss out on a single lead! Engage instantly when contacts respond to your broadcasts,
                                boost sales and increase your ROI.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-5.png" alt="TurboCX - Green Tick Verification">
                        </div>
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 5</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Green Tick Verification</h3>
                            <p>Everyone trusts WhatsApp! When WhatsApp marks your business as verified, it gives you that credibility currency. </p>
                            <p>That’s what Green Tick Verification does. It can transform your ordinary WhatsApp Business profile into a living, breathing brand. </p>
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-9.svg" alt="TurboCX - Boost Credibility" />
                            <h5>Boost Credibility</h5>
                            <p>
                                When you send and receive messages, your business isn’t another face in the crowd - and your business profile is visible even if you’re not a saved contact.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-10.svg" alt="TurboCX - Establish Brand Identity" />
                            <h5>Establish Brand Identity</h5>
                            <p>
                                Green Tick Verification means that your business is a recognized brand that people talk about, and WhatsApp verifies this.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-11.svg" alt="TurboCX - TurboCX White Glove" />
                            <h5>TurboCX White Glove</h5>
                            <p>
                                Applying for Green Tick Verification is free but approval is not guaranteed. Our expertise can <a href="https://turbocx.com/benefits.php" class="btn--text">make this process easier</a>.

                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 6</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Start Conversations with WhatsApp Templates</h3>
                            <p>Settle in and talk your talk! Businesses get a 24 hour window (it’s WhatsApp, not us!) to respond with free form messages to customers.
                            </p>
                            <p>
                                But for starting any chat outside of this time-window, you’ll need to utilize pre-approved WhatsApp messages templates that your business is free to send.
                            </p>
                            <!-- <div>
                            <a class="btn--text" href="#" tabindex="0">
                                Understanding the whatsapp communication channel <img loading="lazy" src="assets/images/icons/arrow-orange.svg">
                            </a>
                        </div> -->
                        </div>
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-6.png" alt="TurboCX - Start Conversations with WhatsApp Templates">
                        </div>
                    </div>
                    <div class="hiwSteps fourColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-12.svg" alt="TurboCX - Message Templates" />
                            <h5>Message Templates</h5>
                            <p>
                                Templates have wildcard variables to be auto-filled & pre-set text - plus buttons & quick replies.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-13.svg" alt="TurboCX - Creating Templates" />
                            <h5>Creating Templates</h5>
                            <p>
                                Templates are easy to create & submit. Create & submit them to WhatsApp with a few clicks right from the TurboCX dashboard.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-14.svg" alt="TurboCX - Manage Templates" />
                            <h5>Manage Templates</h5>
                            <p>
                                Templates should be transactional, not promotional, so approval can take minutes or even 6-12 hours (for manual approval).
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-15.svg" alt="TurboCX - Template Services" />
                            <h5>Template Services</h5>
                            <p>
                                Improve customer experience. With our template strategy & approval service, you’ll never feel out of your depth.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-7.png" alt="TurboCX - Setup Wallet and Send Messages">
                        </div>
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 7</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Setup Wallet and Send Messages</h3>
                            <p>All set up! You’re ready to step into the big league and looking forward to growing your business using TurboCX … well, almost. </p>
                            <p>You’ll need to sync an active credit/debit card and <a href="https://turbocx.com/pricing.php" class="btn--text">select a pricing plan</a> to activate your wallet and access the full power of TurboCX. Plus you’ll get transparency on costs for messages and campaigns.</p>
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-16.svg" alt="TurboCX - Initialize Wallet" />
                            <h5>Initialize Wallet</h5>
                            <p>
                                Add credit to your wallet using popular payment methods for all your budgeted operations and get access to monthly receipts of spends.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-17.svg" alt="TurboCX - Pay Per Conversation" />
                            <h5>Pay Per Conversation</h5>
                            <p>
                                See the bigger picture! With conversation based pricing, you get a 24 hour window to exchange unlimited messages.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-18.svg" alt="TurboCX - Broadcast Costs" />
                            <h5>Broadcast Costs</h5>
                            <p>
                                A few simple steps sets up your campaign and reserves its cost from your wallet balance. Once messages are delivered, this amount is debited.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="hiwStepsInner">
                    <div class="hiwStepsG leftRightGrid">
                        <div class="hiwStepsG-content leftRightGrid-content">
                            <h3 class="hiwStepsG-content__firstTitle">Step 8</h3>
                            <h3 class="hiwStepsG-content__secondTitle">Analyze Conversations for Meaningful Relationships
                            </h3>
                            <p>Make customer connections enjoyable! Businesses lose customers over that one bad experience or that one pesky marketing campaign.
                            </p>
                            <p>Use TurboCX’s powerful analytics to ensure that every communication from your business delights your customers and makes interactions with customers meaningful. </p>
                        </div>
                        <div class="hiwStepsG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/hiw/hiw-steps-8.png" alt="TurboCX - Analyze Conversations for Meaningful Relationships">
                        </div>
                    </div>
                    <div class="hiwSteps threeColWithCenteredOrphans">
                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-19.svg" alt="TurboCX - Message Analytics" />
                            <h5>Message Analytics</h5>
                            <p>
                                TurboCX also helps you measure and <a href="https://turbocx.com/features.php" class="btn--text">improve your communications</a>. Read rates, engagements, ROI - get a birds eye view of things to know what’s going wrong, and what you’re doing right.
                            </p>
                        </div>

                        <div class="hiwSteps__item colItems">
                            <img loading="lazy" src="assets/images/hiw/hiwIcon-20.svg" alt="TurboCX - Agent Analytics" />
                            <h5>Agent Analytics</h5>
                            <p>
                                Improve your customer experience by digging into agent responses - see response times, resolution rates and wait time split by date, agent or team.
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section>


        <?php @include('template-parts/testimonialSection.php') ?>

        <?php @include('template-parts/brochureSection.php') ?>
    </main>

    <?php @include('template-parts/footer.php') ?>