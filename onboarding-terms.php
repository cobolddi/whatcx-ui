<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Onboarding-whatcx</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta property="og:title" content="Onboarding-whatcx" />
  <meta property="og:description" content="" />
  <link rel="stylesheet" href="assets/css/main.min.css" />
  <meta property="og:url" content="https://turbocx.com/onboarding/" />
  <link rel="canonical" href="https://turbocx.com/onboarding/" />
  <link rel="icon" href="assets/images/favicon.png" sizes="32x32" />
</head>

<?php

// Initialize the session
session_start();

// Temporarily in $_POST structure.
$_SESSION['cname'] = $_POST['cname'];
$_SESSION['gstno'] = $_POST['gstno'];
$_SESSION['raddress'] = $_POST['raddress'];
$_SESSION['caddress'] = $_POST['caddress'];
$_SESSION['name'] = $_POST['name'];
$_SESSION['countrycode'] = $_POST['countrycode'];
$_SESSION['mobile'] = $_POST['mobile'];
$_SESSION['email'] = $_POST['email'];
$_SESSION['femail'] = $_POST['femail'];
$_SESSION['pincode'] = $_POST['pincode'];
$_SESSION['city'] = $_POST['city'];
$_SESSION['state'] = $_POST['state'];
$_SESSION['country'] = $_POST['country'];
$_SESSION['agreement1'] = $_POST['agreement1'];
$_SESSION['agreement2'] = $_POST['agreement2'];

?>

<body class="on-board-bg">
  <main>
    <section>
      <div class="container">
        <div class="onboard-box">
          <div class="logo-onboard">
            <img src="assets/images/onboard-logo.svg">
          </div>
          <p class="text-center fs-32 fw-600 txt-1">Terms of services</p>
          <p class="text-center txt-g fs-16 txt-75">We’re super excited to work with you! But before that, please fill out the form below to begin the onboarding process.</p>
          <div>
            <form action="webapi/onboarding-form.php" class="onboard_form" method="post">
              <input type="text" name="cname" hidden value="<?php echo $_POST["cname"]; ?>">
              <input type="text" name="gstno" hidden value="<?php echo $_POST["gstno"]; ?>">
              <input type="text" name="raddress" hidden value="<?php echo $_POST["raddress"]; ?>">
              <input type="text" name="caddress" hidden value="<?php echo $_POST["caddress"]; ?>">
              <input type="text" name="name" hidden value="<?php echo $_POST["name"]; ?>">
              <input type="text" name="mobile" hidden value="<?php echo $_POST["countrycode"]; ?> <?php echo $_POST["mobile"]; ?>">
              <input type="text" name="email" hidden value="<?php echo $_POST["email"]; ?>">
              <input type="text" name="femail" hidden value="<?php echo $_POST["femail"]; ?>">
              <input type="text" name="pincode" hidden value="<?php echo $_POST["pincode"]; ?>">
              <input type="text" name="city" hidden value="<?php echo $_POST["city"]; ?>">
              <input type="text" name="state" hidden value="<?php echo $_POST["state"]; ?>">
              <input type="text" name="country" hidden value="<?php echo $_POST["country"]; ?>">
              <input type="text" name="agreement1" hidden value="<?php echo $_POST["agreement1"]; ?>">
              <input type="text" name="agreement2" hidden value="<?php echo $_POST["agreement2"]; ?>">

              <div class="terms-onboard">

                <h3>SOFTWARE AS A SERVICE AGREEMENT </h3>
                <p>This Software as a Service Agreement (“Agreement”) is entered into between <span class="fw-600"><?php echo $_POST["cname"]; ?></span> (“Customer”) situated at <span class="fw-600"><?php echo $_POST["raddress"]; ?></span> and EdgeCX Private Limited (“EdgeCX”) with its principal place of business at 55,
                  Lane 2, 2nd Floor, Westend Marg, Saidullajab, Saket, New Delhi - 110030. EdgeCX and Customer agree
                  that the following terms and conditions will apply to the services provided under this Agreement and
                  Orders placed thereunder. </p>


                <h3>1. DEFINITIONS </h3>
                <p><span class="fw-600">“Administrator User”</span> means each Customer employee designated by Customer
                  to serve as technical administrator of the SaaS Services on Customer’s behalf. Each Administrator User
                  must complete training and qualification requirements reasonably required by EdgeCX. </p>
                <p><span class="fw-600">“Customer Content”</span> means all data and materials provided by Customer to
                  EdgeCX for use in connection with the SaaS Services, including, without limitation, customer
                  applications, data files, and graphics. </p>
                <p><span class="fw-600">“Documentation”</span> means the user guides, online help, release notes,
                  training materials and other documentation provided or made available by EdgeCX to Customer regarding
                  the use or operation of the SaaS Services. </p>
                <p><span class="fw-600">“Host”</span> means the computer equipment on which the Software is installed,
                  which is owned and operated by EdgeCX or its subcontractors. </p>
                <p><span class="fw-600">“Maintenance Services”</span> means the support and maintenance services
                  provided by EdgeCX to Customer pursuant to this SaaS Agreement and Exhibit B. </p>
                <p><span class="fw-600">“Other Services”</span> means all technical and non-technical services performed
                  or delivered by EdgeCX under this SaaS Agreement, including, without limitation, implementation
                  services and other professional services, training and education services but excluding the SaaS
                  Services and the Maintenance Services. Other Services will be provided on a time and material basis at
                  such times or during such periods, as may be specified in a Schedule and mutually agreed to by the
                  parties. All Other Services will be provided on a non-work for hire basis. </p>
                <p><span class="fw-600">“Schedule”</span> is a written document attached to this SaaS Agreement under
                  Exhibit A or executed separately by EdgeCX and Customer for the purpose of purchasing SaaS Services
                  under the terms and conditions of this SaaS Agreement. </p>
                <p><span class="fw-600">“Software”</span> means the object code version of any software to which
                  Customer is provided access as part of the Service, including any updates or new versions. </p>
                <p><span class="fw-600">“SaaS Services”</span> refer to the specific EdgeCX’s internet-accessible
                  service identified in a Schedule that provides use of EdgeCX’s business communication Software that is
                  hosted by EdgeCX or its services provider and made available to Customer over a network on a term-use
                  basis. </p>
                <p><span class="fw-600">“Subscription Term”</span> shall mean that period specified in a Schedule during
                  which Customer will have on-line access and use of the Software through EdgeCX’s SaaS Services. The
                  Subscription Term shall renew for successive 12-month periods unless either party delivers written
                  notice of non-renewal to the other party at least 30 days prior to the expiration of the then-current
                  Subscription Term. </p>
                <p><span class="fw-600">“User”</span> means all persons identified by Customer as users of EdgeCX’s SaaS
                  Services and configured on the software. </p>


                <h3>2. SAAS SERVICES </h3>
                <p><Span class="fw-600">2.1</Span> During the Subscription Term, Customer will receive a nonexclusive,
                  non-assignable, royalty free, worldwide right to access and use the SaaS Services solely for business
                  operations subject to the terms of this Agreement and up to the number of Users and other limitations
                  documented in the Schedule. </p>
                <p><span class="fw-600">2.2</span> Customer acknowledges that this Agreement is a services agreement and
                  EdgeCX will not be delivering copies of the Software to Customer as part of the SaaS Services. </p>


                <h3>3. RESTRICTIONS </h3>
                <p>Customer shall not, and shall not permit anyone to: </p>
                <ol>
                  <li>copy or republish the SaaS Services or Software, </li>
                  <li>make the SaaS Services available to any person other than authorized Users, </li>
                  <li>use or access the SaaS Services to provide service bureau, time-sharing or other computer hosting
                    services to third parties, </li>
                  <li>modify or create derivative works based upon the SaaS Services or Documentation, </li>
                  <li>remove, modify or obscure any copyright, trademark or other proprietary notices contained in the
                    software used to provide the SaaS Services or in the Documentation, </li>
                  <li>reverse engineer, decompile, disassemble, or otherwise attempt to derive the source code of the
                    Software used to provide the SaaS Services, except and only to the extent such activity is expressly
                    permitted by applicable law, or </li>
                  <li>access the SaaS Services or use the Documentation in order to build a similar product or
                    competitive product. Subject to the limited licenses granted herein, EdgeCX shall own all right,
                    title and interest in and to the Software, services, Documentation, and other deliverables provided
                    under this SaaS Agreement, including all modifications, improvements, upgrades, derivative works and
                    feedback related thereto and intellectual property rights therein. Customer agrees to assign all
                    right, title and interest it may have in the foregoing to EdgeCX. </li>
                </ol>


                <h3>4. CUSTOMER RESPONSIBILITIES </h3>
                <p><span class="fw-600">4.1 Assistance.</span> Customer shall provide commercially reasonable
                  information and assistance to EdgeCX to enable EdgeCX to deliver the SaaS Services. Upon request from
                  EdgeCX, Customer shall deliver Customer Content to EdgeCX in an electronic file format specified and
                  accessible by EdgeCX. Customer acknowledges that EdgeCX’s ability to deliver the SaaS Services in the
                  manner provided in this SaaS Agreement may depend upon the accuracy and timeliness of such information
                  and assistance. </p>
                <p><span class="fw-600">4.2 Compliance with Laws.</span> Customer shall comply with all applicable
                  local, state, national and foreign laws in connection with its use of the SaaS Services, including
                  those laws related to data privacy, international communications, and the transmission of technical or
                  personal data. Customer acknowledges that EdgeCX exercises no control over the content of the
                  information transmitted by Customer or the Users through the SaaS Services. Customer shall not upload,
                  post, reproduce or distribute any information, software or other material protected by copyright,
                  privacy rights, or any other intellectual property right without first obtaining the permission of the
                  owner of such rights. </p>
                <p><span class="fw-600">4.3 Unauthorized Use; False Information.</span> Customer shall: (a) notify
                  EdgeCX immediately of any unauthorized use of any password or user id or any other known or suspected
                  breach of security, (b) report to EdgeCX immediately and use reasonable efforts to stop any
                  unauthorized use of the SaaS Services that is known or suspected by Customer or any User, and (c) not
                  provide false identity information to gain access to or use the SaaS Services. </p>
                <p><span class="fw-600">4.4 Administrator Access.</span> Customer shall be solely responsible for the
                  acts and omissions of its Administrator Users. EdgeCX shall not be liable for any loss of data or
                  functionality caused directly or indirectly by the Administrator Users. </p>
                <p><span class="fw-600">4.5 Customer Input.</span> Customer is solely responsible for collecting,
                  inputting and updating all Customer Content stored on the Host, and for ensuring that the Customer
                  Content does not (i) include anything that actually or potentially infringes or misappropriates the
                  copyright, trade secret, trademark or other intellectual property right of any third party, or (ii)
                  contain anything that is obscene, defamatory, harassing, offensive or malicious. Customer shall: (i)
                  notify EdgeCX immediately of any unauthorized use of any password or user id or any other known or
                  suspected breach of security, (ii) report to EdgeCX immediately and use reasonable efforts to stop any
                  unauthorized use of the Service that is known or suspected by Customer or any User, and (iii) not
                  provide false identity information to gain access to or use the Service. </p>
                <p><span class="fw-600">4.6 License from Customer.</span> Subject to the terms and conditions of this
                  SaaS Agreement, Customer shall grant to EdgeCX a limited, non-exclusive and non-transferable license,
                  to copy, store, configure, perform, display and transmit Customer Content solely as necessary to
                  provide the SaaS Services to Customer. </p>
                <p><span class="fw-600">4.7 Ownership and Restrictions.</span> Customer retains ownership and
                  intellectual property rights in and to its Customer Content. EdgeCX or its licensors retain all
                  ownership and intellectual property rights to the services, Software programs, and anything developed
                  and delivered under the Agreement. Third party technology that may be appropriate or necessary for use
                  with some EdgeCX programs is specified in the program Documentation or ordering document as
                  applicable. </p>
                <p><span class="fw-600">4.8 Suggestions.</span> EdgeCX shall have a royalty-free, worldwide,
                  irrevocable, perpetual license to use and incorporate into the SaaS Services any suggestions,
                  enhancement requests, recommendation or other feedback provided by Customer(s), including Users,
                  relating to the operation of the SaaS Services. </p>


                <h3>5. ORDERS & PAYMENT </h3>
                <p><span class="fw-600">5.1 Orders.</span> Customer shall order SaaS Services pursuant to a Schedule.
                  All services acquired by Customer shall be governed exclusively by this SaaS Agreement and the
                  applicable Schedule. In the event of a conflict between the terms of a Schedule and this SaaS
                  Agreement, the terms of the Schedule shall take precedence. </p>
                <p><span class="fw-600">5.2 Invoicing and Payment.</span> Unless otherwise provided in the Schedule,
                  EdgeCX shall invoice Customer for all fees on the Schedule effective date. Customer shall pay all
                  undisputed invoices within 7 days after Customer receives the invoice. Except as expressly provided
                  otherwise, fees are non-refundable. All fees are stated in Indian Rupee and must be paid in Indian
                  Rupee. </p>
                <p><span class="fw-600">5.3 Taxes.</span> EdgeCX shall bill Customer for applicable taxes as a separate
                  line item on each invoice. Customer shall be responsible for payment of all taxes as per governing law
                  at the time of invoicing or similar charges relating to Customer’s purchase and use of the services.
                </p>


                <h3>6. TERM AND TERMINATION </h3>
                <p><span class="fw-600">6.1 Term of SaaS Agreement.</span> The term of this SaaS Agreement shall begin
                  on the Effective Date and shall continue until terminated by either party as outlined in this Section.
                </p>
                <p><span class="fw-600">6.2 Termination.</span> Either party may terminate this SaaS Agreement
                  immediately upon a material breach by the other party that has not been cured within thirty (30) days
                  after receipt of notice of such breach. </p>
                <p><span class="fw-600">6.3 Suspension for Non-Payment.</span> EdgeCX reserves the right to suspend
                  delivery of the SaaS Services if Customer fails to timely pay any undisputed amounts due to EdgeCX
                  under this SaaS Agreement, but only after EdgeCX notifies Customer of such failure and such failure
                  continues for fifteen (15) days. Suspension of the SaaS Services shall not release Customer of its
                  payment obligations under this SaaS Agreement. Customer agrees that EdgeCX shall not be liable to
                  Customer or to any third party for any liabilities, claims or expenses arising from or relating to
                  suspension of the SaaS Services resulting from Customer’s nonpayment. </p>
                <p><span class="fw-600">6.4 Suspension for Ongoing Harm.</span> EdgeCX reserves the right to suspend
                  delivery of the SaaS Services if EdgeCX reasonably concludes that Customer or a Customer’s user’s use
                  of the SaaS Services is causing immediate and ongoing harm to EdgeCX or others. In the extraordinary
                  case that EdgeCX must suspend delivery of the SaaS Services, EdgeCX shall immediately notify Customer
                  of the suspension and the parties shall diligently attempt to resolve the issue. EdgeCX shall not be
                  liable to Customer or to any third party for any liabilities, claims or expenses arising from or
                  relating to any suspension of the SaaS Services in accordance with this Section 6.4. Nothing in this
                  Section 6.4 will limit EdgeCX’s rights under Section 6.5 below. </p>
                <p><span class="fw-600">6.5 Effect of Termination.</span> </p>
                <p>(a) Upon termination of this SaaS Agreement or expiration of the Subscription Term, EdgeCX shall
                  immediately cease providing the SaaS Services and all usage rights granted under this SaaS Agreement
                  shall terminate. </p>
                <p>(b) If EdgeCX terminates this SaaS Agreement due to a breach by Customer, then Customer shall
                  immediately pay to EdgeCX all amounts then due under this SaaS Agreement and to become due during the
                  remaining term of this SaaS Agreement, but for such termination. If Customer terminates this SaaS
                  Agreement due to a breach by EdgeCX, then EdgeCX shall repay within 45 days to Customer all pre-paid
                  amounts for any unperformed SaaS Services scheduled to be delivered after the termination date. </p>
                <p>(c) Upon termination of this SaaS Agreement and upon subsequent written request by the disclosing
                  party, the receiving party of tangible Confidential Information shall immediately return such
                  information or destroy such information and provide written certification of such destruction,
                  provided that the receiving party may permit its legal counsel to retain one archival copy of such
                  information in the event of a subsequent dispute between the parties. </p>


                <h3>7. SERVICE LEVEL AGREEMENT </h3>
                <p>The SaaS Services will achieve System Availability (as defined below) of at least 96% during each
                  calendar year of the Subscription Term. All other SaaS Services will achieve System Availability (as
                  defined below) of at least 99% during each calendar year of the Subscription Term. </p>
                <p>“System Availability” means the number of minutes in a year that the key components of the SaaS
                  Services are operational as a percentage of the total number of minutes in such year, excluding
                  downtime resulting from (a) scheduled maintenance, (b) events of Force Majeure in the SaaS Agreement),
                  (c) malicious attacks on the system, (d) issues associated with the Customer’s computing devices,
                  local area networks or internet service provider connections, or (e) inability to deliver services
                  because of acts or omissions of Customer or user. </p>
                <p>EdgeCX reserves the right to take the Service offline for scheduled maintenance for which Customer
                  has been provided reasonable notice and EdgeCX reserves the right to change its maintenance window
                  upon prior notice to Customer. If EdgeCX fails to meet System Availability in the year, upon written
                  request by Customer within 30 days after the end of the year, EdgeCX will issue a credit in Customer’s
                  next invoice in an amount equal to 1% of the yearly fee for the affected SaaS Services for each 1%
                  loss of System Availability below stated SLA per SaaS Service, up to a maximum of the Customer’s fee
                  for the affected SaaS Services. If the yearly fee has been paid in advance, then at Customer’s
                  election EdgeCX shall provide a credit to Customer to be used for additional users or term extension.
                  The remedy stated in this paragraph is Customer’s sole and exclusive remedy for interruption of SaaS
                  Services and EdgeCX’s failure to meet System Availability. </p>


                <h3>8. WARRANTIES </h3>
                <p><span class="fw-600">8.1 Warranty.</span> EdgeCX represents and warrants that it will provide the
                  SaaS Services in a professional manner consistent with general industry standards and that the SaaS
                  Services will perform substantially in accordance with the Documentation. For any beach of a warranty,
                  Customer’s exclusive remedy shall be as provided in Section 6, Term and Termination. </p>
                <p>EDGECX WARRANTS THAT THE SAAS SERVICES WILL PERFORM IN ALL MATERIAL RESPECTS IN ACCORDANCE WITH THE
                  HIGHEST LEVELS OF PROFESSIONAL. EDGECX DOES NOT GUARANTEE THAT THE SAAS SERVICES WILL BE PERFORMED
                  ERROR-FREE OR UNINTERRUPTED, OR THAT EDGECX WILL CORRECT ALL SAAS SERVICES ERRORS. CUSTOMER
                  ACKNOWLEDGES THAT EDGECX DOES NOT CONTROL THE TRANSFER OF DATA OVER COMMUNICATIONS FACILITIES,
                  INCLUDING THE INTERNET, AND THAT THE SAAS SERVICE MAY BE SUBJECT TO LIMITATIONS, DELAYS, AND OTHER
                  PROBLEMS INHERENT IN THE USE OF SUCH COMMUNICATIONS FACILITIES. THIS SECTION SETS FORTH THE SOLE AND
                  EXCLUSIVE WARRANTY GIVEN BY EDGECX (EXPRESS OR IMPLIED) WITH RESPECT TO THE SUBJECT MATTER OF THIS
                  AGREEMENT. NEITHER EDGECX NOR ANY OF ITS LICENSORS OR OTHER SUPPLIERS WARRANT OR GUARANTEE THAT THE
                  OPERATION OF THE SUBSCRIPTION SERVICE WILL BE UNINTERRUPTED, VIRUS-FREE OR ERROR-FREE, NOR SHALL
                  EDGECX OR ANY OF ITS SERVICE PROVIDERS BE LIABLE FOR UNAUTHORIZED ALTERATION, THEFT OR DESTRUCTION OF
                  CUSTOMER’S OR ANY USER’S DATA, FILES, OR PROGRAMS.</p>


                <h3>9. LIMITATION OF LIABILITY </h3>
                <p>NEITHER PARTY (NOR ANY LICENSOR OR OTHER SUPPLIER OF EDGECX) SHALL BE LIABLE FOR INDIRECT,
                  INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOST
                  BUSINESS, PROFITS, DATA OR USE OF ANY SERVICE, INCURRED BY EITHER PARTY OR ANY THIRD PARTY IN
                  CONNECTION WITH THIS SAAS AGREEMENT, REGARDLESS OF THE NATURE OF THE CLAIM (INCLUDING NEGLIGENCE),
                  EVEN IF FORESEEABLE OR THE OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. NEITHER
                  PARTY’S AGGREGATE LIABILITY FOR DAMAGES UNDER THIS SAAS AGREEMENT, REGARDLESS OF THE NATURE OF THE
                  CLAIM (INCLUDING NEGLIGENCE), SHALL EXCEED THE FEES PAID OR PAYABLE BY CUSTOMER UNDER THIS SAAS
                  AGREEMENT DURING THE 12 MONTHS PRECEDING THE DATE THE CLAIM AROSE. The foregoing limitations shall not
                  apply to the parties’ obligations (or any breach thereof) under Sections entitled “Restriction”,
                  “Indemnification”, or “Confidentiality”. </p>


                <h3>10. INDEMNIFICATION </h3>
                <p><span class="fw-600">10.1 Indemnification by EdgeCX.</span> If a third party makes a claim against
                  Customer that the SaaS Services infringes any patent, copyright or trademark, or misappropriates any
                  trade secret, or that EdgeCX’s negligence or willful misconduct has caused bodily injury or death,
                  EdgeCX shall defend Customer and its directors, officers and employees against the claim at EdgeCX’s
                  expense and EdgeCX shall pay all losses, damages and expenses (including reasonable attorneys’ fees)
                  finally awarded against such parties or agreed to in a written settlement agreement signed by EdgeCX,
                  to the extent arising from the claim. EdgeCX shall have no liability for any claim based on (a) the
                  Customer Content, (b) modification of the SaaS Services not authorized by EdgeCX, or (c) use of the
                  SaaS Services other than in accordance with the Documentation and this SaaS Agreement. EdgeCX may, at
                  its sole option and expense, procure for Customer the right to continue use of the SaaS Services,
                  modify the SaaS Services in a manner that does not materially impair the functionality, or terminate
                  the Subscription Term and repay to Customer any amount paid by Customer with respect to the
                  Subscription Term following the termination date. </p>
                <p><span class="fw-600">10.2 Indemnification by Customer.</span> If a third party makes a claim against
                  EdgeCX that the Customer Content infringes any patent, copyright or trademark, or misappropriates any
                  trade secret, Customer shall defend EdgeCX and its directors, officers and employees against the claim
                  at Customer’s expense and Customer shall pay all losses, damages and expenses (including reasonable
                  attorneys’ fees) finally awarded against such parties or agreed to in a written settlement agreement
                  signed by Customer, to the extent arising from the claim. </p>
                <p><span class="fw-600">10.3 Conditions for Indemnification.</span> A party seeking indemnification
                  under this section shall (a) promptly notify the other party of the claim, (b) give the other party
                  sole control of the defense and settlement of the claim, and (c) provide, at the other party’s expense
                  for out-of-pocket expenses, the assistance, information and authority reasonably requested by the
                  other party in the defense and settlement of the claim. </p>


                <h3>11. CONFIDENTIALITY </h3>
                <p><span class="fw-600">11.1 Definition.</span> “Confidential Information” means any information
                  disclosed by a party to the other party, directly or indirectly, which, (a) if in written, graphic,
                  machine-readable or other tangible form, is marked as “confidential” or “proprietary,” (b) if
                  disclosed orally or by demonstration, is identified at the time of initial disclosure as confidential
                  and is confirmed in writing to the receiving party to be “confidential” or “proprietary” within 30
                  days of such disclosure, (c) is specifically deemed to be confidential by the terms of this SaaS
                  Agreement, or (d) reasonably appears to be confidential or proprietary because of the circumstances of
                  disclosure and the nature of the information itself. Confidential Information will also include
                  information disclosed by third parties to a disclosing party under an obligation of confidentiality.
                  Subject to the display of Customer Content as contemplated by this SaaS Agreement, Customer Content is
                  deemed Confidential Information of Customer. EdgeCX software and Documentation are deemed Confidential
                  Information of EdgeCX. </p>
                <p><span class="fw-600">11.2 Confidentiality.</span> During the term of this SaaS Agreement and for 5
                  years thereafter (perpetually in the case of software), each party shall treat as confidential all
                  Confidential Information of the other party, shall not use such Confidential Information except to
                  exercise its rights and perform its obligations under this SaaS Agreement, and shall not disclose such
                  Confidential Information to any third party. Without limiting the foregoing, each party shall use at
                  least the same degree of care, but not less than a reasonable degree of care, it uses to prevent the
                  disclosure of its own confidential information to prevent the disclosure of Confidential Information
                  of the other party. Each party shall promptly notify the other party of any actual or suspected misuse
                  or unauthorized disclosure of the other party’s Confidential Information. Neither party shall reverse
                  engineer, disassemble or decompile any prototypes, software or other tangible objects which embody the
                  other party's Confidential Information and which are provided to the party hereunder. Each party may
                  disclose Confidential Information of the other party on a need-to-know basis to its contractors who
                  are subject to confidentiality agreements requiring them to maintain such information in confidence
                  and use it only to facilitate the performance of their services on behalf of the receiving party. 11.3
                  Exceptions. Confidential Information excludes information that: (a) is known publicly at the time of
                  the disclosure or becomes known publicly after disclosure through no fault of the receiving party, (b)
                  is known to the receiving party, without restriction, at the time of disclosure or becomes known to
                  the receiving party, without restriction, from a source other than the disclosing party not bound by
                  confidentiality obligations to the disclosing party, or (c) is independently developed by the
                  receiving party without use of the Confidential Information as demonstrated by the written records of
                  the receiving party. The receiving party may disclose Confidential Information of the other party to
                  the extent such disclosure is required by law or order of a court or other governmental authority,
                  provided that the receiving party shall use reasonable efforts to promptly notify the other party
                  prior to such disclosure to enable the disclosing party to seek a protective order or otherwise
                  prevent or restrict such disclosure. Each party may disclose the existence of this SaaS Agreement and
                  the relationship of the parties, but agrees that the specific terms of this SaaS Agreement will be
                  treated as Confidential Information; provided, however, that each party may disclose the terms of this
                  SaaS Agreement to those with a need to know and under a duty of confidentiality such as accountants,
                  lawyers, bankers and investors. </p>


                <h3>12. GENERAL PROVISIONS </h3>
                <p><span class="fw-600">12.1 Non-Exclusive Service.</span> Customer acknowledges that SaaS Services is
                  provided on a non-exclusive basis. Nothing shall be deemed to prevent or restrict EdgeCX’s ability to
                  provide the SaaS Services or other technology, including any features or functionality first developed
                  for Customer, to other parties. </p>
                <p><span class="fw-600">12.2 Personal Data.</span> Customer hereby acknowledges and agrees that EdgeCX’s
                  performance of this SaaS Agreement may require EdgeCX to process, transmit and/or store Customer
                  personal data or the personal data of Customer employees and Affiliates. By submitting personal data
                  to EdgeCX, Customer agrees that EdgeCX and its Affiliates may process, transmit and/or store personal
                  data only to the extent necessary for, and for the sole purpose of, enabling EdgeCX to perform its
                  obligations to under this SaaS Agreement. In relation to all Personal Data provided by or through
                  Customer to EdgeCX, Customer will be responsible as sole Data Controller for complying with all
                  applicable data protection or similar laws implementing that Directive that regulate the processing of
                  Personal Data and special categories of data as such terms are defined in that Directive. Customer
                  agrees to obtain all necessary consents and make all necessary disclosures before including Personal
                  Data in Content and using the Enabling Software and EdgeCX SaaS. Customer confirms that Customer is
                  solely responsible for any Personal Data that may be contained in Content, including any information
                  which any EdgeCX SaaS User shares with third parties on Customer’s behalf. Customer is solely
                  responsible for determining the purposes and means of processing Customer Personal Data by EdgeCX
                  under this Agreement, including that such processing according to Customer’s instructions will not
                  place EdgeCX in breach of applicable data protection laws. Prior to processing, Customer will inform
                  EdgeCX about any special categories of data contained within Customer Personal Data and any
                  restrictions or special requirements in the processing of such special categories of data, including
                  any cross border transfer restrictions. Customer is responsible for ensuring that the EdgeCX SaaS
                  meets such restrictions or special requirements. EdgeCX to process any Personal Data that meets the
                  requirements set forth in this Section according to these Terms of Use. </p>
                <p><span class="fw-600">12.3 EdgeCX Personal Data Obligations.</span> In performing the SaaS Services,
                  EdgeCX will comply with the EdgeCX Services Privacy Policy, which is available at
                  http://www.turbocx.com/privacy-policy and incorporated herein by reference. The EdgeCX Services Privacy
                  Policy is subject to change at EdgeCX’s discretion; however, EdgeCX policy changes will not result in
                  a material reduction in the level of protection provided for Customer data during the period for which
                  fees for the services have been paid. The services policies referenced in this SaaS Agreement specify
                  our respective responsibilities for maintaining the security of Customer data in connection with the
                  SaaS Services. EdgeCX reserves the right to provide the SaaS Services from Host locations, and/or
                  through use of subcontractors, worldwide. </p>
                <p>EdgeCX will only process Customer Personal Data in a manner that is reasonably necessary to provide
                  SaaS Services and only for that purpose. EdgeCX will only process Customer Personal Data in delivering
                  EdgeCX SaaS. Customer agrees to provide any notices and obtain any consent related to EdgeCX’s use of
                  the data for provisioning the SaaS Services, including those related to the collection, use,
                  processing, transfer and disclosure of personal information. Customer shall have sole responsibility
                  for the accuracy, quality, integrity, legality, reliability, appropriateness and retains ownership of
                  all of Customer data. </p>
                <p><span class="fw-600">12.4 Assignment.</span> Neither party may assign this SaaS Agreement or any
                  right under this SaaS Agreement, without the consent of the other party, which consent shall not be
                  unreasonably withheld or delayed; provided however, that either party may assign this SaaS Agreement
                  to an acquirer of all or substantially all of the business of such party to which this SaaS Agreement
                  relates, whether by merger, asset sale or otherwise. This SaaS Agreement shall be binding upon and
                  inure to the benefit of the parties’ successors and permitted assigns. Either party may employ
                  subcontractors in performing its duties under this SaaS Agreement, provided, however, that such party
                  shall not be relieved of any obligation under this SaaS Agreement. </p>
                <p><Span class="fw-600">12.5 Notices.</Span> Except as otherwise permitted in this SaaS Agreement,
                  notices under this SaaS Agreement shall be in writing and shall be deemed to have been given (a) five
                  (5) business days after mailing if sent by registered mail, (b) when transmitted if sent by email,
                  provided that a copy of the notice is promptly sent by another means specified in this section, or (c)
                  when delivered if delivered personally or sent by express courier service. All notices shall be sent
                  to the other party at the address set forth on the cover page of this SaaS Agreement. </p>
                <p><span class="fw-600">12.6 Force Majeure.</span> Each party will be excused from performance for any
                  period during which, and to the extent that, such party or any subcontractor is prevented from
                  performing any obligation or Service, in whole or in part, as a result of causes beyond its reasonable
                  control, and without its fault or negligence, including without limitation, acts of God, strikes,
                  lockdowns, riots, acts of terrorism or war, epidemics, communication line failures, and power
                  failures. </p>
                <p><span class="fw-600">12.7 Waiver.</span> No waiver shall be effective unless it is in writing and
                  signed by the waiving party. The waiver by either party of any breach of this SaaS Agreement shall not
                  constitute a waiver of any other or subsequent breach. </p>
                <p><span class="fw-600">12.8 Severability.</span> If any term of this SaaS Agreement is held to be
                  invalid or unenforceable, that term shall be reformed to achieve as nearly as possible the same effect
                  as the original term, and the remainder of this SaaS Agreement shall remain in full force. </p>
                <p><span class="fw-600">12.9 Entire SaaS Agreement.</span> This SaaS Agreement (including all Schedules
                  and exhibits) contains the entire agreement of the parties and supersedes all previous oral and
                  written communications by the parties, concerning the subject matter of this SaaS Agreement. This SaaS
                  Agreement may be amended solely in a writing signed by both parties. Standard or printed terms
                  contained in any purchase order or sales confirmation are deemed rejected and shall be void unless
                  specifically accepted in writing by the party against whom their enforcement is sought; mere
                  commencement of work or payment against such forms shall not be deemed acceptance of the terms. </p>
                <p><span class="fw-600">12.10 Survival.</span> Sections 3, 6, and 8 through 12 of this SaaS Agreement
                  shall survive the expiration or termination of this SaaS Agreement for any reason. </p>
                <p><span class="fw-600">12.11 Publicity.</span> EdgeCX may include Customer’s name and logo in its
                  customer lists and on its website. Upon signing, EdgeCX may issue a high-level press release
                  announcing the relationship and the manner in which Customer will use the EdgeCX solution. EdgeCX
                  shall coordinate its efforts with appropriate communications personnel in Customer’s organization to
                  secure approval of the press release if necessary. </p>
                <p><span class="fw-600">12.13 No Third-Party Beneficiaries.</span> This SaaS Agreement is an agreement
                  between the parties, and confers no rights upon either party’s employees, agents, contractors,
                  partners of customers or upon any other person or entity. </p>
                <p><span class="fw-600">12.14 Independent Contractor.</span> The parties have the status of independent
                  contractors, and nothing in this SaaS Agreement nor the conduct of the parties will be deemed to place
                  the parties in any other relationship. Except as provided in this SaaS Agreement, neither party shall
                  be responsible for the acts or omissions of the other party or the other party’s personnel. </p>
                <p><span class="fw-600">12.15 Statistical Information.</span> EdgeCX may anonymously compile statistical
                  information related to the performance of the Services for purposes of improving the SaaS service,
                  provided that such information does not identify Customer’s data or include Customer’s name. </p>
                <p><span class="fw-600">12.16 Governing Law.</span> This SaaS Agreement shall be governed by the laws of
                  India. </p>
                <p><span class="fw-600">12.17 Compliance with Laws.</span> EdgeCX shall comply with all applicable
                  local, state, and national laws in connection with its delivery of the SaaS Services, including those
                  laws related to data privacy, international communications, and the transmission of technical or
                  personal data </p>
                <p><span class="fw-600">12.18 Dispute Resolution.</span> Customer’s satisfaction is an important
                  objective to EdgeCX in performing its obligations under this SaaS Agreement. Except with respect to
                  intellectual property rights, if a dispute arises between the parties relating to the interpretation
                  or performance of this SaaS Agreement or the grounds for the termination hereof, the parties agree to
                  hold a meeting within fifteen (15) days of written request by either party, attended by individuals
                  with decision-making authority, regarding the dispute, to attempt in good faith to negotiate a
                  resolution of the dispute. </p>
                <p><span class="fw-600">12.19 Signatures.</span> This SaaS Agreement may be executed in multiple
                  counterparts, each of which when executed will be an original, and all of which, when taken together,
                  will constitute one agreement. Delivery of an executed counterpart of a signature page of this SaaS
                  Agreement by email will be effective as delivery of a manually executed counterpart. </p>

                <div class="sign-sec">
                  <p class="txt-g pb-10">Please sign & stamp with company seal </p>
                  <p>Signed by: </p>
                  <p>Customer Name: </p>
                  <p>Customer Organisation: </p>
                  <p>Effective Date:</p>
                  <p class="txt-g pb-10">Please sign & stamp with company seal </p>
                  <p>For: EdgeCX Pvt. Ltd. </p>
                  <p>Officer Name: </p>
                  <p>Date:</p>
                </div>

                <h3>Proposal for TurboCX – The CRM on WhatsApp Cloud API </h3>
                <p>At TurboCX, we offer a bouquet of WhatsApp Cloud API solutions to fit your business. The cost of
                  implementing TurboCX can be divided under 4 heads: Platform, API, Conversations & Onboarding. </p>
                <p><span class="fw-600">Platform Costs:</span><br> Monthly subscription charges for the TurboCX software.
                  All our plans include access to the web & mobile applications and a host of features that vary by
                  plan. To understand the differences between the plan, please refer to the table below and the section
                  “Modules & Features offered by Plan”. </p>
                <p><span class="fw-600">API Costs:</span><br> WA Cloud API is the technology product from WhatsApp
                  required for TurboCX to work. There are 2 ways to get the API. One is from an API Reseller like
                  360Dialog that provides premium cloud infrastructure for faster communication. Second, we can obtain
                  the API from Meta directly. Charges for each are listed in the section. </p>
                <p><span class="fw-600">Conversation Costs:</span><br> WhatsApp charges based on a 24-hour window and
                  the pricing differs by destination country of the contact. At TurboCX, we match the same
                  high-availability technology by WhatsApp and combine it with our own technology to ensure seamless
                  chat between your business users and contacts.</p>
                <p><span class="fw-600">Onboarding Costs:</span><br> Businesses need solutions. Solutions that work.
                  Instantly. That’s why we’ve tailored our onboarding services to match the requirements of different
                  teams. Whether it is one person onboarding or training a team of 50 on TurboCX, we have you covered.
                </p>
                <p><Span class="fw-600">Curate your own solution:</Span> </p>
                <ul>
                  <li>Step 1: Choose the right plan for your needs. </li>
                  <li>Step 2: Choose an API provider that works best for you. </li>
                  <li>Step 3: Choose the service packages you need to get the solution up and running. </li>
                  <li>Step 4: Choose any addons you require to power up your plan. </li>
                </ul>
                <p><span class="fw-600">1. Standard Plans:</span> Choose the right TurboCX plan for your business. </p>
                <table class="table-A">
                  <tr>
                    <th class="w-35">Product / Service </th>
                    <th class="w-40">Details</th>
                    <th>Cost</th>
                  </tr>
                  <tr>
                    <td>
                      <h3>Essential Plan</h3>
                      <p>Best suited for startups and SME businesses who want to start exploring the possibilities on
                        chat & TurboCX. </p>
                    </td>
                    <td>
                      <h3>Essential</h3>
                      <ul>
                        <li>1 WhatsApp API Account mapped to 1 number </li>
                        <li>5 Users, 1 Team </li>
                        <li>6,000 Contacts Management </li>
                        <li>Schedule Broadcast Messages </li>
                        <li>Live Chat & Conversation Manager </li>
                        <li>TurboCX 247 Mobile App for Agent Chat </li>
                        <li>Essential Features: Chat, Broadcast, Contacts </li>
                        <li>Email & Chat Support</li>
                      </ul>
                    </td>
                    <td>
                      <p>Rs.2,199 per month </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Standard Plan</h3>
                      <p>Most commonly purchased plan by tech-enabled SMEs and Startups alike to expand their reach on
                        WhatsApp and nurture valuable leads for their business. </p>
                    </td>
                    <td>
                      <h3>Standard </h3>
                      <ul>
                        <li>Everything in Essential Plan + </li>
                        <li>15 Users, 3 Teams </li>
                        <li>50,000 Contact Management </li>
                        <li>Access to ALL Current Features </li>
                        <li>Access to TurboCX API </li>
                        <li>On Call Support</li>
                      </ul>
                    </td>
                    <td>
                      <p>Rs.5,500 per month </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Growth Plan </h3>
                      <p>Suitable for larger marketing and sales teams looking for a centralized solution for all their
                        WhatsApp communication. This plan is loved by marketing, sales, product and operations teams
                        alike as they all participate collaboratively to grow the chat channel in the business. </p>
                    </td>
                    <td>
                      <h3>Growth </h3>
                      <ul>
                        <li>Everything in Standard Plan + </li>
                        <li>50 Users, 5 Teams </li>
                        <li>300,000 Contacts Management </li>
                        <li>Access to ALL Current & Planned Features as outlined </li>
                        <li>Integrations with Leadsquared, HubSpot, Pipedrive </li>
                        <li>On Call Priority Support </li>
                        <li>Named relationship manager</li>
                      </ul>
                    </td>
                    <td>
                      <p>Rs.25,000 per month </p>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="3" class="text-center">Customized Plans for over 50 users and above 300,000 contacts
                      are available upon request. </td>
                  </tr>
                </table>
                <p><span class="fw-600">2. API Provider:</span> Choose an API Provider of your choice. </p>
                <table class="table-A">
                  <tr>
                    <th class="w-35">Product / Service </th>
                    <th class="w-40">Details</th>
                    <th>Cost</th>
                  </tr>
                  <tr>
                    <td>
                      <h3>WhatsApp Cloud API by Meta </h3>
                      <p class="txt-green">Preferred</p>
                    </td>
                    <td>
                      <ul>
                        <li>API Infrastructure provided by Meta (slower by 15-20%) </li>
                        <li>Billing for Conversations happens directly with Meta through Credit Card </li>
                        <li>Latest features are available immediately on launch from Meta </li>
                      </ul>
                    </td>
                    <td>
                      <p>FREE</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Premium WA Cloud API from 360Dialog, Germany </h3>
                    </td>
                    <td>
                      <ul>
                        <li>Premium API Infrastructure provided by 360Dialog, Germany </li>
                        <li>Billing for Conversations happens through the TurboCX Wallet System </li>
                        <li>Latest features are available within 2 months of launch </li>
                      </ul>
                    </td>
                    <td>
                      <p>$50 per month per number </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Gupshup Business API – Enterprise </h3>
                    </td>
                    <td>
                      <ul>
                        <li>We can integrate with the Gupshup Business API if required </li>
                        <li>This API is generally 4-6 months behind the WA Cloud API in terms of features and not well
                          supported </li>
                      </ul>
                    </td>
                    <td>
                      <p>Rs. 0.10 / conversation</p>
                      <p>Suitable for over 10L conversations / month</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Airtel IQ </h3>
                    </td>
                    <td>
                      <ul>
                        <li>Support for WA Cloud API from Airtel IQ will be launched in February 2023. </li>
                      </ul>
                    </td>
                    <td>
                      <p>Coming soon. </p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">3: Service Packages:</span> Choose the services you will need to get your
                  solution up and running.</p>
                <table class="table-A">
                  <tr>
                    <th class="w-35">Product / Service </th>
                    <th class="w-40">Details</th>
                    <th>Cost</th>
                  </tr>
                  <tr>
                    <td>
                      <h3>Free Onboarding</h3>
                      <p>DIY style self-onboarding</p>
                    </td>
                    <td>
                      <ul>
                        <li>We’ll share a detailed guide on how to activate the WhatsApp Cloud API </li>
                        <li>We’ll require the Cloud API Keys & Token to activate the TurboCX Account. </li>
                        <li>Requires high technical knowledge and comfort with command line terminal to be able to
                          integrate. </li>
                        <li>Best suited for technical professionals who know what they are doing. </li>
                        <li>One training session of the TurboCX platform will be provided for customers opting for the
                          Free Onboarding. </li>
                      </ul>
                    </td>
                    <td>
                      <p>Free </p>
                      <p>Only applicable for WA Cloud API by Meta </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Basic Onboarding</h3>
                      <p>Takes about 1-7 days </p>
                    </td>
                    <td>
                      <ul>
                        <li>Assistance in gathering Onboarding requirements for WA Cloud API </li>
                        <li>Assessment of all assets & requirements </li>
                        <li>Submission of required documentation to Meta </li>
                        <li>Virtual call to complete WA Cloud API setup </li>
                        <li>Setup of payment method for WA Cloud API </li>
                        <li>Name approval submission (not OBA) </li>
                        <li>Handing over of the API keys to customer for safekeeping </li>
                        <li>Training session on how to use WhatsApp Manager and associated tools by Meta </li>
                        <li>Recording of onboarding sessions to be made available for 120 days. </li>
                        <li>Integrate TurboCX and the Cloud API </li>
                        <li>One training session of the TurboCX platform </li>
                      </ul>
                    </td>
                    <td>
                      <p>Rs.30,000 </p>
                      <p>One-time payment. <br>Reduce your time to go live. <br>Let us handle the complexity. <br>Setup
                        a future-ready system. <br>Plug API to any compatible solution like TurboCX. <br>Manage your WA
                        Cloud API efficiently. </p>
                      <br>
                      <p class="txt-green">Supporting Indian Startups & SME Businesses </p>
                      <p class="fw-600">We are a made in India, made for India product.</p>
                      <p>You can share your DPIIT Certificate or MSME Udyog Aadhar Certificate to receive Rs.20,000
                        credits in conversational charges. Connect with us to know more about this offer. </p>
                      <p><i>Applicable only for Basic Onboarding. </i></p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Standard Onboarding</h3>
                      <p>Takes about 10 days 1-month Active Support on WhatsApp group</p>
                    </td>
                    <td>
                      <p>Pre-onboarding: </p>
                      <ol>
                        <li>Assessment of all assets & requirements for WABA. </li>
                        <li>Discussion & finalisation of Use Cases. </li>
                        <li>Rollout & training plan. </li>
                        <li>Content development consultation.</li>
                      </ol>
                      <p>Onboarding: </p>
                      <ol>
                        <li>Procurement of API from vendor of choice. </li>
                        <li>Virtual Calls to complete the onboarding process. </li>
                        <li>Training Sessions, upto 50 Participants. </li>
                        <li>Setup of finalized use cases on the platform. </li>
                        <li>Green Tick Evaluation & Submission. </li>
                      </ol>
                      <p>Post Onboarding</p>
                      <ol>
                        <li>Weekly calls to identify Customer adoption opportunities. </li>
                        <li>Quarterly review call. </li>
                        <li>Setup of WhatsApp Group for 1-month Active Support </li>
                        <li>On call priority support for 6 months. </li>
                        <li>Optional quarterly retraining session. </li>
                      </ol>
                    </td>
                    <td>
                      <p>Rs. 1,75,000</p>
                      <p>One-time payment. <br>
                        Ensure success of the platform within the team. <br>
                        Increase adoption of the new communication channel. <br>
                        Handhold the team to success with our experts.</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Conversation Charges </h3>
                    </td>
                    <td>
                      <p>First 1000 conversations per month are free.</p>
                      <p>Conversation charges are paid through the Wallet within the TurboCX system or can be loaded in
                        your account in bulk after a bank transfer. Please connect with us at accounts@whatcx.com if you
                        need to bulk load your TurboCX Wallet. </p>
                    </td>
                    <td>
                      <p>Rs.0.20 + WhatsApp Charges / conversation </p>
                      <p class="fw-600">>100K conversations per month </p>
                      <p>Rs.0.10 + WhatsApp Charges / conversation </p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">4. Optional Services</span> </p>
                <table class="table-A">
                  <tr>
                    <th class="w-35">Product / Service </th>
                    <th class="w-40">Details</th>
                    <th>Cost</th>
                  </tr>
                  <tr>
                    <td>
                      <h3>Additional Users </h3>
                    </td>
                    <td>
                      <p>Per additional user per month </p>
                    </td>
                    <td>
                      <p>Rs.1500 per additional user </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Dedicated account manager </h3>
                    </td>
                    <td>
                      <p>Available on email & phone during working hours to execute & support. </p>
                    </td>
                    <td>Rs.25,000 per month </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Additional Storage Space </h3>
                    </td>
                    <td>
                      <p>Only applicable if required. Per 50GB of storage. </p>
                    </td>
                    <td>
                      <p>Rs.750 per month </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Green Tick Submission </h3>
                    </td>
                    <td>
                      <p>One time compilation and submissions charges </p>
                    </td>
                    <td>
                      <p>Rs.5000 </p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Unlimited Contacts Addon </h3>
                    </td>
                    <td>
                      <p>Add unlimited contacts to your TurboCX account. </p>
                    </td>
                    <td>
                      <p><span class="fw-600">Rs.5000 / month</span></p>
                      <p>Can be added to Standard & Growth Plans</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Conversational CRM Shift</h3>
                      <h3>A special customer success service delivered by the Leadership Team at TurboCX.</h3>
                      <h3>Optional. </h3>
                    </td>
                    <td>
                      <p>Deliverables:</p>
                      <ol>
                        <li>WhatsApp Use Case Discovery Session for WhatsApp across one business unit. </li>
                        <li>Use Case Setup Consultation, Plan. </li>
                        <li>Training on conversational CRM Paradigm thinking. </li>
                        <li>Training the team on application and use case implementation. </li>
                        <li>Weekly review calls with team to compile efficacy and report conversational CRM
                          implementation impact. </li>
                        <li>Presentation of 6-month growth report with conversational CRM implementation for the
                          business unit. </li>
                      </ol>
                    </td>
                    <td>
                      <p>Rs.75,000 / month </p>
                      <p>Six-month engagement 2 hours per week </p>
                    </td>
                  </tr>
                </table>
                <ul>
                  <li>GST to be charged as applicable at the time of invoicing. </li>
                  <li>Conversation charges to be paid through Wallet System in TurboCX or through bank transfer. </li>
                  <li>To make payment via bank transfer, please connect at accounts@whatcx.com </li>
                  <li>To upgrade your plan, please reach out to us at sales@whatcx.com </li>
                  <li>WhatsApp charges are paid directly via credit card on file on Facebook Business Manager Account. </li>
                  <li>WABA pricing details are available here: https://developers.facebook.com/docs/whatsapp/pricing </li>
                  <li>To cancel your plan, please reach out to us at sales@whatcx.com </li>
                </ul>
                <h3>Modules & Features offered by Plan </h3>
                <p><span class="fw-600">Platform Management</span> </p>
                <table class="table-A">
                  <tr>
                    <th class="w-40">Module </th>
                    <th class="w-20">Essential 2,199 p.m. </th>
                    <th class="w-20">Standard 5,500 p.m. </th>
                    <th class="w-20">Growth 25,000 p.m. </th>
                  </tr>
                  <tr>
                    <td>
                      <h3>TurboCX Web App Access </h3>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>TurboCX Mobile Apps for iOS & Android </h3>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>User Management, Working Hours, Holidays </h3>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Team Management </h3>
                    </td>
                    <td>
                      <p>1 team</p>
                    </td>
                    <td>
                      <p>3 team</p>
                    </td>
                    <td>
                      <p>5 team</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Custom Roles & Permissions </h3>
                    </td>
                    <td>
                      <p>No</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Wallet Management & Payments </h3>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <h3>Office Hours Management & Notice </h3>
                    </td>
                    <td>
                      <p>No</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                    <td>
                      <p>Yes</p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">Contact Management</span></p>
                <table class="table-A">
                  <tr>
                    <td class="w-40">
                      <h3>Contact Management </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Contact Limit </h3>
                    </td>
                    <td class="w-20">
                      <p>6000</p>
                    </td>
                    <td class="w-20">
                      <p>50000</p>
                    </td>
                    <td class="w-20">
                      <p>300000</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Contact Management </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Contact Notes & History </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Preferred Agent Relationship Management </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Contact Custom Fields </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Import / Export Contacts </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Manage Contacts via API </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Dynamic Filters & Lists </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">Conversation Management </span></p>
                <table class="table-A">
                  <tr>
                    <td class="w-40">
                      <h3>Conversations </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Multi Agent Collaborative Chat </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Pinned Chats, Chat Label Management </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Whatsy Auto-responder and Routing </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Agent Feedback Collection </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Team Chat View </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                </table>

                <p><span class="fw-600">Broadcast Management </span></p>
                <table class="table-A">
                  <tr>
                    <td class="w-40">
                      <h3>Instant Broadcasts </h3>
                    </td>
                    <td class="w-20">
                      <p>Basic</p>
                    </td>
                    <td class="w-20">
                      <p>Full</p>
                    </td>
                    <td class="w-20">
                      <p>Full</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Scheduled Broadcasts </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Template Management </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Template Integration Code </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Enquiry Capture for Broadcast </h3>
                    </td>
                    <td class="w-20">
                      <p>Basic</p>
                    </td>
                    <td class="w-20">
                      <p>Basic</p>
                    </td>
                    <td class="w-20">
                      <p>Basic</p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">QR Code & Widgets Management </span></p>
                <table class="table-A">
                  <tr>
                    <td class="w-40">
                      <h3>Module Access </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>QR Code & Widget Limit </h3>
                    </td>
                    <td class="w-20">
                      <p>n/a</p>
                    </td>
                    <td class="w-20">
                      <p>10</p>
                    </td>
                    <td class="w-20">
                      <p>Unlimited</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>QR Code Generation </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">Misc. Features </span></p>
                <table class="table-A">
                  <tr>
                    <td class="w-40">
                      <h3>Link Shortening & Click Tracking </h3>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Automatic QR Code </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Last agent assignment</h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Integration Key Management </h3>
                    </td>
                    <td class="w-20">
                      <p>5 keys</p>
                    </td>
                    <td class="w-20">
                      <p>5 keys</p>
                    </td>
                    <td class="w-20">
                      <p>25 keys</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Wordpress Integration Plugin </h3>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                    </td>
                  </tr>
                </table>
                <p><span class="fw-600">Future Release Features – Planned</span></p>
                <table class="table-A">
                  <small>Please note, these modules are under active development. We cannot commit a fixed timeline for these features. However, it is our endeavor to bring these features to market as soon as possible.</small>
                  <tr>
                    <td class="w-40">
                      <h3>Forms</h3>
                      <p>Create lead qualification forms within TurboCX and let your contacts answer a few queries before registering an enquiry in the system.</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Flows</h3>
                      <p>Flows are a series of messages that can be attached to any broadcast message reply to capture buyer intent automatically.</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Drip Campaigns</h3>
                      <p>Allow contacts to be sorted in bucket and trigger automatic messages with time delay. </p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Magic Rewrite</h3>
                      <p>Auto-magically correct grammar during live chat for agents. </p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Audio & Video Calling</h3>
                      <p>Setup custom rooms for a conversation and activate instant calls from within WhatsApp with recording.</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Broadcast A/B Testing</h3>
                      <p>Setup A/B Tests for campaigns. Optimize the campaign for readability & engagement automatically. </p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Actions</h3>
                      <p>Define multiple actions as response to broadcast response and automate manual tasks.</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Todos, Snooze & Task Management</h3>
                      <p>Inbuilt task management and snoozing capability for chats & agents</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Team Chat</h3>
                      <p>Inbuilt team chat for better collaboration on contact conversations.</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>Retargeting Broadcast Campaigns</h3>
                      <p>Generate dynamic lists from broadcasts & schedule broadcasts to subset of campaigns </p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Included</p>
                    </td>
                  </tr>
                  <tr>
                    <td class="w-40">
                      <h3>AI Conversation Summarization</h3>
                      <p>Automatically summarize chat conversations in notes.</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>No</p>
                    </td>
                    <td class="w-20">
                      <p>Yes</p>
                      <p>Additional</p>
                    </td>
                  </tr>
                </table>
              </div>
              <?php
              if (isset($_POST['cname']) && !empty($_POST['gstno']) && !empty($_POST['raddress']) && !empty($_POST['caddress']) && !empty($_POST['caddress']) && !empty($_POST['name']) && !empty($_POST['mobile']) && !empty($_POST['email']) && !empty($_POST['femail']) && !empty($_POST['agreement1']) && !empty($_POST['agreement2'])) {
              ?>
                <button type="submit" class="onboard-btn">I Agree</button>
              <?php
              }
              ?>

            </form>
          </div>
          <div class="onboard-foot">
            <p>It is our policy to protect your privacy and to keep all the information shared by you confidential. To visit our website, please click here. In case of any support, feel free to reach out to us at <a target="_blank" href="https://turbocx.com/">TurboCX</a>.</p>
          </div>
          <div class="foot-url">
            <a target="_blank" href="https://app.turbocx.com/login/">Login</a>
            <a target="_blank" href="https://www.turbocx.com/contact.php">Support</a>
          </div>
        </div>
      </div>
    </section>
  </main>
</body>