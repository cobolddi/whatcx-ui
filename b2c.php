<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX</title>
    
<?php @include('template-parts/header.php') ?>

<main>


  <!-- breadcrumbs -->
  <section class="breadcrumbs">
    <div class="container">
      <ul>
        <li><a href="/">Home</a></li>
        <li><a href="solutions.php"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Solution</a></li>
        <li><a href="b2c"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> For B2B Businesses</a></li>
      </ul>
    </div>
  </section>  


  <!--  -->
  <section class="Section b2cBanner">
    <div class="container">
      <div class="b2cBanner-section leftRightGrid" style="background-image: url(./assets/images/b2c/banner-bg.svg);">
        <div class="b2cBanner-content leftRightGrid-content">
          <h1>Digitize Your Business Conversations, Easily!</h1>

          <p>
            Leverage the power of WhatsApp to manage customer relationships at
            scale Ramp up sales, automate workflows, facilitate in-store
            assistance, broadcast messages, and resolve queries instantly with
            TurboCX.
          </p>

          <div class="ctaWrap">
            <a href="sign-up.php" class="secondaryBtn">Sign up for free Demo</a>
          </div>
        </div>

        <div class="b2cBanner-graphics leftRightGrid-image">
          <img  src="assets/images/b2c/mobile.svg" alt="" />
        </div>
      </div>
    </div>
  </section>
  <!--  -->

  <!--  -->
  <section class="Section b2cClient-section hpClients-Section">
    <div class="container">
      <div class="hpClients">
        <h5>Join hundreds of businesses who use TurboCX for customer success.</h5>
        <div class="hpClients--logos">
          <div class="logoWrap">
            <img loading="lazy" src="assets/images/logo3.png" alt="" />
          </div>
          <div class="logoWrap">
            <img loading="lazy" src="assets/images/logo2.png" alt="" />
          </div>
          <div class="logoWrap">
            <img loading="lazy" src="assets/images/logo1.png" alt="" />
          </div>
          <div class="logoWrap">
            <img loading="lazy" src="assets/images/logo4.png" alt="" />
          </div>
          <div class="logoWrap">
            <img loading="lazy" src="assets/images/logo5.png" alt="" />
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--  -->

  <!--  -->
  <section class="Section b2cBusiness-section">
    <div class="container">
      <div class="centerSectionHeading commonHeading">
        <h2>Why do customers prefer digital businesses?</h2>

        <p>
          The traditional ways of serving customers are being challenged.
        </p>

        <p>
          Digitize your business with a single solution to connect with your
          customers.
        </p>
      </div>

      <div class="b2cBusiness fourColWithCenteredOrphans">
        <div class="b2cBusiness__item colItems">
          <img loading="lazy" src="assets/images/b2c/icon-1.svg" alt="" />
          <p>
            Having multiple personal chats with different members of your team is
            a bad customer experience.
          </p>
        </div>

        <div class="b2cBusiness__item colItems">
          <img loading="lazy" src="assets/images/b2c/icon-2.svg" alt="" />
          <p>
            Long wait times and repeated follow up to get issues resolved
            negatively affects customer retention.
          </p>
        </div>

        <div class="b2cBusiness__item colItems">
          <img loading="lazy" src="assets/images/b2c/icon-3.svg" alt="" />
          <p>
            Lack of privacy keeps data at risk and compromises the credibility of
            your business.
          </p>
        </div>

        <div class="b2cBusiness__item colItems">
          <img loading="lazy" src="assets/images/b2c/icon-4.svg" alt="" />
          <p>Manually sending out messages to customers is slow and costly.</p>
        </div>
      </div>
    </div>
  </section>
  <!--  -->

  <!--  -->
  <section class="Section b2cBenefit hpHelp-Section">
    <div class="container-medium">
      
      <div class="centerSectionHeading commonHeading">
        <h2>Features & Benefits</h2>
      </div>

      <div class="hpHelp-slideWrap">
        <div class="b2cBenefit--features hpHelp-slide--features">

            <div class="commonLayout leftRightGrid">
              <div class="leftRightGrid-image fullWidth">
                <img loading="lazy" src="assets/images/home-banner-2.webp" alt="">
              </div>
              <div class="leftRightGrid-content">
                <div>
                  <!-- <span>Lorem ipsum dolor sit amet,</span> -->
                  <h3>Begin Conversations & 
                    Broadcast Campaigns</h3>
                  
                  <ul>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>

                      Define simple and elaborate workflows.
                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>

                      With 2 billion active users and 85% of consumer time, WhatsApp broadcast is the most effective way to reach your customers.

                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>
                      TurboCX brings together powerful marketing automation to help you successfully run your WhatsApp broadcast campaigns.
                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>
                      Detailed reports per message, per campaign &amp; per contact.
                    </li>
                  </ul>

                  <a class="btn--text" href="#" tabindex="0">
                    Lorem ipsum dolor sit amet <img loading="lazy" src="assets/images/icons/arrow-orange.svg">
                  </a>

                </div>

              </div>
            </div>

            <div class="commonLayout leftRightGrid leftRightGrid-reversed">
              <div class="leftRightGrid-image">
                <img loading="lazy" src="assets/images/features-2.svg" alt="">
              </div>
              
              <div class="leftRightGrid-content">
                <div>
                  <span>Lorem ipsum dolor sit amet,</span>
                  <h3>Engage with your stakeholders on one number
                  </h3>
                  
                  <ul>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>
                      Offer better customer support with multiple agents using a shared inbox and centralized dashboard.
                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>
                      TurboCX is a one-stop solution for contact import, omnichannel chat history, contact management, and real-time analytics dashboard.
                    </li>
                   </ul>
                  <a class="btn--text" href="#" tabindex="0">
                    Lorem ipsum dolor sit amet <img loading="lazy" src="assets/images/icons/arrow-orange.svg">
                  </a>

                </div>
              </div>
            </div>
            <div class="commonLayout leftRightGrid">
              <div class="leftRightGrid-image fullWidth">
                <img loading="lazy" src="assets/images/b2c/p-3.svg" alt="">
              </div>
              <div class="leftRightGrid-content">
                <div>
                  <span>Lorem ipsum dolor sit amet,</span>
                  <h3>Manage queries and make support quicker and easier
                  </h3>
                  <ul>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>

                      Make your brand available 24/7 and speed up the communication process.
                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>

                      Facilitate safe passage of communication for your customers with immediate feedback and closure.
                    </li>
                    <li>
                      <svg width="30" height="17" viewBox="0 0 30 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path
                          d="M14.2122 10.5353L16.0948 12.418L27.3828 1.12998L29.2682 3.01531L16.0948 16.1886L7.6095 7.70331L9.49484 5.81798L12.3282 8.65131L14.2122 10.534V10.5353ZM14.2148 6.76464L20.8175 0.160645L22.6975 2.04064L16.0948 8.64464L14.2148 6.76464ZM10.4455 14.3046L8.5615 16.1886L0.0761719 7.70331L1.96151 5.81798L3.8455 7.70198L3.84417 7.70331L10.4455 14.3046Z"
                          fill="#009155"></path>
                      </svg>

                      With TurboCX, always keep private notes ready to take notes for your customers during conversations.
                    </li>
                    
                  </ul>

                  <a class="btn--text" href="#" tabindex="0">
                    Lorem ipsum dolor sit amet <img loading="lazy" src="assets/images/icons/arrow-orange.svg">
                  </a>
                </div>
              </div>
            </div>
          </div>


      </div>
    </div>
  </section>
  <!--  -->

  <!--  -->
  <section class="Section b2cwhatcx-section">
    <div class="container">

      <div class="b2cwhatcx__box">

        <div class="centerSectionHeading commonHeading">
          <h2>Why TurboCX ?</h2>

          <p>
            We work on the principles of lead conversion & customer satisfaction.
            TurboCX is the perfect solution for businesses to handle customers in one place without any hassles.
          </p>
        </div>

        <div class="b2cwhatcx fourColWithCenteredOrphans">
          <div class="b2cwhatcx__item colItems">
            <img loading="lazy" src="assets/images/b2c/what-1.svg" alt="" />
            <h5>Vernacular support</h5>
            <p>
              TurboCX is enhanced with support for nine Indian languages.
            </p>
          </div>

          <div class="b2cwhatcx__item colItems">
            <img loading="lazy" src="assets/images/b2c/what-4.svg" alt="" />
            <h5>Human touch model</h5>
            <p>
              Our conversations are based on natural human-based conversations.
            </p>
          </div>

          <div class="b2cwhatcx__item colItems">
            <img loading="lazy" src="assets/images/b2c/what-3.svg" alt="" />
            <h5>Collaborative tool</h5>
            <p>
              Bring your team members together on one platform, easily.
            </p>
          </div>

          <div class="b2cwhatcx__item colItems">
            <img loading="lazy" src="assets/images/b2c/what-2.svg" alt="" />
            <h5>White glove</h5>
            <p>TurboCX traditionally associates with your high-value customers</p>
          </div>
        </div>

        <div class="b2cwhatcx--btn">
          <a href="#" class="secondaryBtn secondaryBtn--white">
            Learn more
          </a>
        </div>

    </div>
  </div>

</section>
  <!--  -->

  <!--  -->
  <section class="Section b2cJourney-section">
    <div class="container-small">
      <div class="centerSectionHeading commonHeading">
        <h2>Buyer’s Journey </h2>

        <p>
          TurboCX seamlessly integrates your customer conversations into your buyer’s journey. It helps brands reach
          interested prospects, ease them through the buyer’s journey with a highly engaging experience, and increase
          sales significantly.
        </p>
      </div>

      <div class="b2cJourney--process">

          <div class="commonLayout leftRightGrid">
            <div class="leftRightGrid-image">
              <img loading="lazy" src="assets/images/b2c/p-1.svg" alt="">
            </div>

            <div class="leftRightGrid-content" data-step="1">
              <div>
                <h3>Start conversations with the customers</h3>
                <p>
                  The user reaches out to your business with their requirements/needs or you ask them to share their
                  requirements/needs.
                </p>
              </div>
            </div>
          </div>

          <div class="b2cJourney--progress">
            <img loading="lazy" src="assets/images/b2c/process-1.svg" alt="progress">
          </div>

          <div class="commonLayout leftRightGrid leftRightGrid-reversed">
            <div class="leftRightGrid-image">
              <img loading="lazy" src="assets/images/b2c/p-2.svg" alt="">
            </div>

            <div class="leftRightGrid-content" data-step="2">
              <div>
                <h3>Considering requests of the customers </h3>
                <p>
                  TurboCX identifies the perfect team to support them. They are then connected to the right team
                  according to the requirement be it marketing, sales, or designing.
                </p>
              </div>
            </div>
          </div>

          <div class="b2cJourney--progress">
            <img loading="lazy" src="assets/images/b2c/process-2.svg" alt="progress">
          </div>

          <div class="commonLayout leftRightGrid">
            <div class="leftRightGrid-image">
              <img loading="lazy" src="assets/images/b2c/p-3.svg" alt="">
            </div>

            <div class="leftRightGrid-content" data-step="3">
              <div>
                <h3>Connecting the customers with the
                  right team
                </h3>
                <p>
                  The eligible person from the right team connects to the customers. They gather complete information on
                  the customer requirements and deliver solutions according to their needs.
                </p>
              </div>
            </div>
          </div>

          <div class="b2cJourney--progress">
            <img loading="lazy" src="assets/images/b2c/process-1.svg" alt="progress">
          </div>

          <div class="commonLayout leftRightGrid leftRightGrid-reversed">
            <div class="leftRightGrid-image">
              <img loading="lazy" src="assets/images/b2c/p-4-1.svg" alt="">
            </div>

            <div class="leftRightGrid-content" data-step="4">
              <div>
                <h3>Resolve queries and deliver utmost satisfaction </h3>
                <p>
                  TurboCX helps your business deliver maximum customer satisfaction and customers end up with the utmost
                  satisfaction by getting their queries resolved completely.
                </p>
              </div>
            </div>
          </div>

        </div>
      </div>
      
  </section>
  <!--  -->

  <!--  -->
  <section class="Section ppTestSection b2cTestimonial">
    <div class="container-small">
      <div class="ppTest">
        <div class="ppTestSlide">
          <button class="ppTestSlide-btnPrev">
            <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M6.93825 12.2001L16.6605 2.54513L14.0976 0L0 14L14.0976 28L16.6605 25.4549L6.93825 15.7999H29V12.2001H6.93825Z"
                fill="white" />
            </svg>

          </button>
          <div class="ppTestSlide-slider">
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
            <div class="ppTestSlide-slider--item">
              <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we
                  couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of
                  mind.”</em></p>
              <div>
                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                <h5>Jaden Arnold, <span>Smithsonian Museum.</span></h5>
              </div>

            </div>
          </div>
          <button class="ppTestSlide-btnNext">
            <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path
                d="M22.0618 12.2001L12.3395 2.54513L14.9024 0L29 14L14.9024 28L12.3395 25.4549L22.0618 15.7999H0V12.2001H22.0618Z"
                fill="white" />
            </svg>
          </button>

        </div>
      </div>
    </div>
  </section>
  <!--  -->

  <!--  -->
  <section class="Section ppBrochureSection b2cBrochure">
    <div class="container">
      <div class="ppBrochure">
        <h5>Download Brochure</h5>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore
          magna aliqua.</p>
          <div>
            <form class="brochure-form" action="email/thanks.php" method="post">
              <input type="email" placeholder="Enter your email ID." required>
              <input type="hidden" value="TurboCX Download Brochure">
              <button type="submit">Download</button>
              <!-- <a download class="filePdf" href="assets/images/pdf/dummy.pdf" hidden></a> -->
            </form>
          <div class="info"></div>
          <div class="success">Thanks for sharing the email address. We will mail the brochure to you shortly.</div>
        </div>
      </div>
    </div>
  </section>
  <!--  -->

</main>

<?php @include('template-parts/footer.php') ?>