<!DOCTYPE html>
<html lang="en">

<head>
    <title>Official WhatsApp Cloud API for SMEs: WhatCX</title>
    <meta name="description" content="Start communicating officially on WhatsApp and improve customer satisfaction. Get a multi-agent mobile app, broadcast on WhatsApp, official WhatsApp number, unlimited broadcasts on WhatsApp.">
    <meta property="og:title" content="Official WhatsApp Cloud API for SMEs: WhatCX" />
    <meta property="og:description" content="Start communicating officially on WhatsApp and improve customer satisfaction. Get a multi-agent mobile app, broadcast on WhatsApp, official WhatsApp number, unlimited broadcasts on WhatsApp." />
    <meta property="og:url" content="https://whatcx.com/" />
    <link rel="canonical" href="https://whatcx.com/" />

    <?php @include('template-parts/header.php') ?>

    <main>
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#"><img src="assets/images/icons/arrow-right.svg" alt="WhatCX - breadcrumbs icon">Refund Cancellation Policy</a></li>
                </ul>
            </div>
        </section>


        <section class="Section refund-policy">
            <div class="container">
                <h1>Refund & Cancellation Policy for WhatCX</h1>
            </div>
            <div class="container-medium">
                <h3>DEFINITIONS</h3>
                <p><span>"Administrator User"</span> means each Customer employee designated by Customer to serve as
                    technical administrator of the SaaS Services on Customer’s behalf. Each Administrator User must
                    complete training and qualification requirements reasonably required by EdgeCX.</p>
                <p><span>"Customer Content"</span> means all data and materials provided by Customer to EdgeCX for use
                    in connection with the SaaS Services, including, without limitation, customer applications, data
                    files, and graphics.</p>
                <p><span>"Documentation"</span> means the user guides, online help, release notes, training materials
                    and other documentation provided or made available by EdgeCX to Customer regarding the use or
                    operation of the SaaS Services.</p>
                <p><span>"Host"</span> means the computer equipment on which the Software is installed, which is owned
                    and operated by EdgeCX or its subcontractors.</p>
                <p><span>"Maintenance Services"</span> means the support and maintenance services provided by EdgeCX to
                    Customer pursuant to this SaaS Agreement and Exhibit B.</p>
                <p><span>"Other Services"</span> means all technical and non-technical services performed or delivered
                    by EdgeCX under this SaaS Agreement, including, without limitation, implementation services and
                    other professional services, training and education services but excluding the SaaS Services and the
                    Maintenance Services. Other Services will be provided on a time and material basis at such times or
                    during such periods, as may be specified in a Schedule and mutually agreed to by the parties. All
                    Other Services will be provided on a non-work for hire basis.</p>
                <p><span>"Schedule"</span> is a written document attached to this SaaS Agreement under Exhibit A or
                    executed separately by EdgeCX and Customer for the purpose of purchasing SaaS Services under the
                    terms and conditions of this SaaS Agreement.</p>
                <p><span>"Software"</span> means the object code version of any software to which Customer is provided
                    access as part of the Service, including any updates or new versions.</p>
                <p><span>"SaaS Services"</span> refer to the specific EdgeCX’s internet-accessible service identified in
                    a Schedule that provides use of EdgeCX’s business communication Software that is hosted by EdgeCX or
                    its services provider and made available to Customer over a network on a term-use basis.</p>
                <p><span>"Subscription Term"</span> shall mean that period specified in a Schedule during which Customer
                    will have on-line access and use of the Software through EdgeCX’s SaaS Services. The Subscription
                    Term shall renew for successive 12-month periods unless either party delivers written notice of
                    non-renewal to the other party at least 30 days prior to the expiration of the then-current
                    Subscription Term.</p>
                <p><span>"User"</span> means all persons identified by Customer as users of EdgeCX’s SaaS Services and
                    configured on the software.</p>

                <h3>SAAS SERVICES</h3>
                <p>During the Subscription Term, Customer will receive a nonexclusive, non-assignable, royalty free,
                    worldwide right to access and use the SaaS Services solely for business operations subject to the
                    terms of this Agreement and up to the number of Users and other limitations documented in the
                    Schedule.</p>
                <p>Customer acknowledges that this Agreement is a services agreement and EdgeCX will not be delivering
                    copies of the Software to Customer as part of the SaaS Services.</p>

                <h3>RESTRICTIONS</h3>
                <p>Customer shall not, and shall not permit anyone to: </p>
                <ul>
                    <li>copy or republish the SaaS Services or Software, </li>
                    <li>make the SaaS Services available to any person other than authorized Users,</li>
                    <li>use or access the SaaS Services to provide service bureau, time-sharing or other computer
                        hosting services to third parties,</li>
                    <li>modify or create derivative works based upon the SaaS Services or Documentation,</li>
                    <li>remove, modify or obscure any copyright, trademark or other proprietary notices contained in the
                        software used to provide the SaaS Services or in the Documentation,</li>
                    <li>reverse engineer, decompile, disassemble, or otherwise attempt to derive the source code of the
                        Software used to provide the SaaS Services, except and only to the extent such activity is
                        expressly permitted by applicable law, or access the SaaS Services or use the Documentation in
                        order to build a similar product or competitive product. Subject to the limited licenses granted
                        herein, EdgeCX shall own all right, title and interest in and to the Software, services,
                        Documentation, and other deliverables provided under this SaaS Agreement, including all
                        modifications, improvements, upgrades, derivative works and feedback related thereto and
                        intellectual property rights therein. Customer agrees to assign all right, title and interest it
                        may have in the foregoing to EdgeCX.</li>
                </ul>

                <h3>ORDERS & PAYMENT</h3>
                <p><span>Orders</span>. Customer shall order SaaS Services pursuant to a Schedule. All services acquired
                    by Customer shall be governed exclusively by this SaaS Agreement and the applicable Schedule. In the
                    event of a conflict between the terms of a Schedule and this SaaS Agreement, the terms of the
                    Schedule shall take precedence.</p>
                <p><span>Invoicing and Payment</span>. Unless otherwise provided in the Schedule, EdgeCX shall invoice
                    Customer for all fees on the Schedule effective date. Customer shall pay all undisputed invoices
                    within 15 days after Customer receives the invoice. Except as expressly provided otherwise, fees are
                    non-refundable. All fees are stated in Indian Rupee and must be paid in Indian Rupee.</p>
                <p><span>Taxes</span>. EdgeCX shall bill Customer for applicable taxes as a separate line item on each
                    invoice. Customer shall be responsible for payment of all taxes as per governing law at the time of
                    invoicing or similar charges relating to Customer’s purchase and use of the services.</p>

                <h3>TERM AND TERMINATION </h3>
                <p>Term of SaaS Agreement. The term of this SaaS Agreement shall begin on the Effective Date and shall
                    continue until terminated by either party as outlined in this Section. </p>
                <p>Termination. Either party may terminate this Agreement with 30 days’ notice. In case of material
                    breach by the other Party, either party may terminate this SaaS Agreement immediately in case the
                    material breach by the other party has not been cured within thirty (30) days after receipt of
                    notice of such breach.</p>
                <p>Suspension for Non-Payment. EdgeCX reserves the right to suspend delivery of the SaaS Services if
                    Customer fails to timely pay any undisputed amounts due to EdgeCX under this SaaS Agreement, but
                    only after EdgeCX notifies Customer of such failure and such failure continues for fifteen (15)
                    days. Suspension of the SaaS Services shall not release Customer of its payment obligations under
                    this SaaS Agreement. Customer agrees that EdgeCX shall not be liable to Customer or to any third
                    party for any liabilities, claims or expenses arising from or relating to suspension of the SaaS
                    Services resulting from Customer’s nonpayment. </p>
                <p>Suspension for Ongoing Harm. EdgeCX reserves the right to suspend delivery of the SaaS Services if
                    EdgeCX reasonably concludes that Customer or a Customer’s user’s use of the SaaS Services is causing
                    immediate and ongoing harm to EdgeCX or others. In the extraordinary case that EdgeCX must suspend
                    delivery of the SaaS Services, EdgeCX shall immediately notify Customer of the suspension and the
                    parties shall diligently attempt to resolve the issue. EdgeCX shall not be liable to Customer or to
                    any third party for any liabilities, claims or expenses arising from or relating to any suspension
                    of the SaaS Services in accordance with this Section 6.4. Nothing in this Section 6.4 will limit
                    EdgeCX’s rights under Section 6.5 below. </p>

                <h3>Effect of Termination & Cancellation</h3>
                <ol>
                    <li> Upon termination of this SaaS Agreement or expiration of the Subscription Term, EdgeCX shall
                        immediately cease providing the SaaS Services and all usage rights granted under this SaaS
                        Agreement shall terminate. </li>
                    <li> If EdgeCX terminates this SaaS Agreement due to a breach by Customer, then Customer shall
                        immediately pay to EdgeCX all amounts then due under this SaaS Agreement. If Customer terminates
                        this SaaS Agreement due to a breach by EdgeCX, then EdgeCX shall repay within 45 days to
                        Customer all pre-paid amounts for any unperformed SaaS Services scheduled to be delivered after
                        the termination date.</li>
                    <li> Upon termination of this SaaS Agreement and upon subsequent written request by the disclosing
                        party, the receiving party of tangible Confidential Information shall immediately return such
                        information or destroy such information and provide written certification of such destruction,
                        provided that the receiving party may permit its legal counsel to retain one archival copy of
                        such information in the event of a subsequent dispute between the parties.</li>
                </ol>

            </div>
        </section>

    </main>

    <?php @include('template-parts/footer.php') ?>