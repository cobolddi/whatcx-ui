<!DOCTYPE html>
<html lang="en">

<head>
    <title>TurboCX: Best practices in configuring</title>
    <meta name="description" content="Whatsy is a unique feature of TurboCX and provides one of the best ways to instantly expand your reach with customers. 
">
    <meta property="og:title" content="Best practices in configuring" />
    <meta property="og:description" content="Whatsy is a unique feature of TurboCX and provides one of the best ways to instantly expand your reach with customers.  
" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://www.turbocx.com/best-practices-in-configuring-up-whatsy.php" />   
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://www.turbocx.com/assets/images/blog/single/best-practices-in-configuring-Blog-7-Single.webp">
    <meta property="og:image:secure_url" content="https://www.turbocx.com/assets/images/blog/single/best-practices-in-configuring-Blog-7-Single.webp" />
    <meta property="og:image:alt" content="Best practices in configuring" />
  <link rel="canonical" href="https://www.turbocx.com/best-practices-in-configuring-up-whatsy.php" />
<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="https://turbocx.com/blogs.php"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog</a></li>
                <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Blog Detail</a></li>
            </ul>
        </div>
    </section>


    <!-- blog detail section -->
    <section class="Section blogSingle">
        <div class="container-small">

            <h1 class="blogSingle__heading">Best practices in configuring/setting up Whatsy</h1>

            <div class="blogSingle__area">
                <div class="blogSingle__date">
                    <span>16/03/2022</span>
                </div>

                <?php @include('template-parts/share-icons.php'); ?>

            </div>

            <div class="blogSingle-image">
                <img  src="assets/images/blog/single/best-practices-in-configuring-Blog-7-Single.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
            </div>

            <article class="blogSingle__content">

                <p>Conversational experiences require a set of best practices that go beyond natural language understanding and personality. In real life, you will encounter unexpected scenarios, obstacles, uncertainties, and other situations that are common.
                </p>

                <p>
                    When we started creating Whatsy, a conversational chatbot for businesses, we decided to follow an informative approach that leads chatbot developers through a set of best practices. Let’s take a look at practices you need to follow while configuring or setting up Whatsy for your business.
                </p>

                <h3>Setting up Whatsy</h3>

                <ol>
                    <li><a href="https://app.turbocx.com/login/" target="_blank">Log into</a> your TurboCX account with your username and password.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/1.signup.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>Click on Add New Whatsy to create your chatbot. You will get a wide range of options to configure Whatsy. You can give a name to the chatbot and add a welcome message for your customers.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/2.add new wahtsy.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>Labels help you to auto-tag assignments for your team. Choose actions and enter labels for your team members.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/3.options.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>You have to add “Couldn’t understand and Invalid option” message templates for your customers that will assign the conversation to the default team member.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/4.could not understand message.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>You can select the team for default team interaction and choose invalid count tolerance from the options. Whenever a customer will type invalid messages, the tolerance level will decrease and will automatically assign the chat to the team. A default message template before assigning chat to the team will be sent to the customers when you will set a template message for them.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/5.default team.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                </ol>

                <h3>See how Whatsy works</h3>
                <ol>
                    <li>Once you are done with setting up Whatsy, the next thing is to check the statistics or edit the details, if required. The action buttons are provided in the front of Whatsy you have configured for the chat.
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/6.whatsy.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>You can edit message templates, names, options according to your business needs. 
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/7.edit whatsy.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>If at any point you might discover the need to change the auto-assignments you can simply edit the auto tag assignment and assign chats to another person from the team. 
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/8.auto assign.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>Invalid count tolerances, default teams can be changed as per your requirements. 
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/9.tolorence.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>While transferring the chat to the default team, add a message for the team member so that they know they have been assigned the chat.  
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/10.default team.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>You can test Whatsy with the statistics. You can select the range or the day you want to view. A clear timeline of activation of Whatsy will display in front of you.  
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/11.stats.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                    <li>You can also see how many times a customer has connected with different teams, downloaded pdf, or selected invalid response counts.  
                    </li>
                    <img loading="lazy" src="assets/images/blog/blog-7/12.stats.webp" alt="TurboCX - Best practices in configuring/setting up Whatsy">
                </ol>

                <h3>Here is a list of what Whatsy can do</h3>
                <ol>
                    <li>
                    Welcome message- This is the place to list all the options your bot can perform so that users do not waste time trying to engage in a conversation your bot cannot handle.
                    </li>
                    <li>
                    You should provide users with the option to restart the conversation in your welcome message so that they do not feel trapped. Ensure that you make the conversation versatile by including multiple messages that the bot can choose from.
                    </li>
                    <li>
                    There is often a need to pass the conversation to the person. You should ensure that your users have access to this option and that the chatbot remains silent while the operator and the user continue a conversation.
                    </li>
                    <li>
                    Your chatbot will be able to identify the correct conversation to have with the user with the assistance of conversation training. 
                    </li>
                </ol>

                <h3>Automate your conversations wisely with TurboCX</h3>
                <p>Get TurboCX’s automated messaging platform (Whatsy), the perfect chatbot for your marketing, customer support, and sales team. Look out for new possibilities to communicate with your customers effectively. </p>
                <p>
                Make your customer service the best it can be with the best chatbot practices. Tap into the best capabilities of <a href="https://turbocx.com/">TurboCX</a> and continue to iterate your conversations.
                </p>
            </article>
        </div>
    </section>
    <!-- blog detail section -->


    <?php @include('template-parts/blog-cards.php') ?>


</main>

<?php @include('template-parts/footer.php') ?>