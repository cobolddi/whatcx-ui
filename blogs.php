<!DOCTYPE html>
<html lang="en">

<head>
    <title>Informational content from TurboCX </title>
    <meta name="description" content="Become familiar with WhatsApp, one of the most popular communication channels, and learn to use it for your business.">

    <meta property="og:title" content="Informational content from TurboCx" />
    <meta property="og:description" content="Become familiar with WhatsApp, one of the most popular communication channels, and learn to use it for your business." />
    <meta property="og:site_name" content="TurboCx">
    <meta property="og:url" content="https://turbocx.com/blogs.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/og/blogs-og.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/blogs-og.png" />
    <meta property="og:image:alt" content="Informational content from TurboCx" />
    <link rel="canonical" href="https://turbocx.com/blogs.php/" />
    <?php @include('template-parts/header.php') ?>

    <main>

        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>

                    <li><a href="#"> <img  src= "assets/images/icons/arrow-right.svg" alt="TurboCx - Contact Banner"> Resources</a></li>

                    <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCx - Contact Banner"> Resources</a></li>

                </ul>
            </div>
        </section>


        <section class="Section blogWorks">
            <div class="container">
                <div class="hpWorks-Section">
                    <div class="hpWorks leftRightGrid">
                        <div class="hpWorks-content leftRightGrid-content">
                            <!-- <div class="hpWorks-content--small">Featured</div> -->
                            <h1 class="hpWorks-content--heading">Informational content, insights, and compelling stories
                                from TurboCx.</h1>
                            <p>A plethora of useful content to value your business. Check out the topics of your interest
                                and build a potentially valuable readership.</p>

                            <!-- <a class="btn btn--text btn--green" href="">Read more <img loading="lazy" src= "assets/images/blog/arrow-green.svg" alt=""></a> -->
                        </div>

                        <div class="hpWorks-graphics leftRightGrid-image">
                            <img  src= "assets/images/blog/whatsapp.png" alt="whtsapp">

                            <!-- <a class="btn btn--text btn--green" href="">Read more <img loading="lazy" src="assets/images/blog/arrow-green.svg" alt=""></a> -->
                        </div>

                      
                    </div>
                </div>
            </div>
        </section>


        <section class="Section blogCard-section hpCrm-Section">

            <div class="container-small">

                <!-- <div class="tab__filterBtnWrap">
                <a href="#" class="active">All Blogs</a>
                <a href="#">Conversational Al</a>
                <a href="#">Leadership</a>
                <a href="#">#FeatureFriday</a>
                <a href="#">SaaS IT WITH PARTNERS</a>
            </div> -->


                <div class="threeColWithCenteredOrphans">
                    
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/whatsapp-unveils-cloud-api-how-whatcx-is-a-crm-built-on-top-of-the-cloud-api.php">

                            <img loading="lazy" src="assets/images/blog/thumb/WhatsApp-unveils-Cloud-API-2.webp" alt="TurboCx - WhatsApp unveils Cloud API: How TurboCx is a CRM built on top of the cloud API">

                        </a>
                        <div class="blogCard__content">

                            <div class="blogCard__meta">
                                <span>20/05/2022</span>

                                <!-- <a href="#"><img loading="lazy" src= "assets/images/blog/share.svg" alt=""></a> -->

                                <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->

                            </div>

                            <h3 class="blogCard__heading">WhatsApp unveils Cloud API: How TurboCx is a CRM built on top of the cloud API</h3>

                            <a href="https://turbocx.com/whatsapp-unveils-cloud-api-how-whatcx-is-a-crm-built-on-top-of-the-cloud-api.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>

                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/best-practices-in-configuring-up-whatsy.php">

                            <img loading="lazy" src="assets/images/blog/thumb/best-practices-in-configuring-Blog-7-thumb.webp" alt="TurboCx - Best practices in configuring/setting up Whatsy">

                        </a>
                        <div class="blogCard__content">
                            <div class="blogCard__meta">
                                <span>16/03/2022</span>

                                <!-- <a href="#"><img loading="lazy" src= "assets/images/blog/share.svg" alt=""></a> -->

                                <!-- <a href="#"><img loading="lazy" src="assets/images/blog/share.svg" alt=""></a> -->

                            </div>
                            <h3 class="blogCard__heading">Best practices in configuring/setting up Whatsy</h3>
                            <a href="https://turbocx.com/best-practices-in-configuring-up-whatsy.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/tips-to-keep-your-account-secure.php">

                            <img loading="lazy" src="assets/images/blog/thumb/tips-to-keep-your-account-secure-Blog-8-thumb.webp" alt="TurboCx - Tips to keep your account secure">
                        </a>
                        <div class="blogCard__content">
                            <div class="blogCard__meta">
                                <span>16/03/2022</span>

                            </div>
                            <h3 class="blogCard__heading">Tips to keep your account secure</h3>
                            <a href="https://turbocx.com/tips-to-keep-your-account-secure.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/onboarding-your-team-on-whatcx.php">

                            <img loading="lazy" src="assets/images/blog/thumb/onboarding-your-team-on-whatcx-blog-6.webp" alt="TurboCx - Onboarding your team on TurboCx">
                        </a>
                        <div class="blogCard__content">
                            <div class="blogCard__meta">
                                <span>14/03/2022</span>

                            </div>
                            <h3 class="blogCard__heading">Onboarding your team on TurboCx</h3>
                            <a href="https://turbocx.com/onboarding-your-team-on-whatcx.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>         -->
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/contact-management-and-views-in-whatcx.php">

                            <img loading="lazy" src="assets/images/blog/thumb/contact-management-blog-5.webp" alt="TurboCx - Contact management and views in TurboCx">
                        </a>
                        <div class="blogCard__content">
                            <div class="blogCard__meta">
                                <span>04/03/2022</span>

                            </div>
                            <h3 class="blogCard__heading">Contact management and views in TurboCx</h3>
                            <a href="https://turbocx.com/contact-management-and-views-in-whatcx.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div> -->
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/do-s-and-dont-s-while-communicating-with-clients-to-avoid-being-reported-as-spam-being-blocked.php">

                            <img loading="lazy" src="assets/images/blog/thumb/teamwork-communication.webp" alt="TurboCx - Do's and Dont's - while communicating with clients (to avoid being reported as spam/being blocked)">

                        </a>

                        <div class="blogCard__content">

                            <div class="blogCard__meta">
                                <span>10/01/2022</span>

                            </div>

                            <h3 class="blogCard__heading">Do's and Dont's - while communicating with clients (to avoid being
                                reported as spam/being blocked)</h3>

                            <a href="https://turbocx.com/do-s-and-dont-s-while-communicating-with-clients-to-avoid-being-reported-as-spam-being-blocked.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>

                    </div> -->
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/canned-responses-how-to-set-up-quick-responses.php">

                            <img loading="lazy" src="assets/images/blog/thumb/communication.webp" alt="TurboCx - Canned responses- how to set up quick responses">

                        </a>

                        <div class="blogCard__content">

                            <div class="blogCard__meta">
                                <span>10/01/2022</span>

                            </div>

                            <h3 class="blogCard__heading">Canned responses- how to set up quick responses</h3>

                            <a href="https://turbocx.com/canned-responses-how-to-set-up-quick-responses.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>

                    </div> -->
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/whatsapp-message-templates-best-practices-to-follow.php">

                            <img loading="lazy" src="assets/images/blog/thumb/whatsapp-msg-template-blog.webp" alt="TurboCx - Whatsapp message templates: Best practices to follow">

                        </a>

                        <div class="blogCard__content">

                            <div class="blogCard__meta">
                                <span>10/01/2022</span>

                            </div>

                            <h3 class="blogCard__heading">Whatsapp message templates: Best practices to follow</h3>

                            <a href="whatsapp-message-templates-best-practices-to-follow.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>

                    </div> -->
                    <!-- <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/understanding-the-whatsapp-communication-channel-making-the-most-of-it.php">

                            <img loading="lazy" src="assets/images/blog/thumb/whatsapp-connection.webp" alt="TurboCx - Understanding the Whatsapp communication channel - making the most of it.">

                        </a>
                        <div class="blogCard__content">

                            <div class="blogCard__meta">
                                <span>05/01/2022</span>

                                

                            </div>

                            <h3 class="blogCard__heading">Understanding the Whatsapp communication channel - making the most
                                of it.</h3>

                            <a href="https://turbocx.com/understanding-the-whatsapp-communication-channel-making-the-most-of-it.php" class="btn btn--text btn--orange">
                                Read more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>

                    </div> -->


                    
                </div>

                <!-- <nav class="pagination">
                <a class="prev page-numbers" href="">Prev</a>
                <span class="page-numbers current">1</span>
                <a class="page-numbers" href="#">2</a>
                <a class="page-numbers" href="#">3</a>
                <a class="page-numbers" href="#">4</a>
                <a class="page-numbers" href="#">5</a>
                <a class="page-numbers" href="#">6</a>
                <a class="page-numbers" href="#">...</a>
                <a class="page-numbers" href="#">11</a>
                <a class="next page-numbers" href="#">Next</a>
            </nav> -->

            </div>
        </section>

    </main>

    <?php @include('template-parts/footer.php') ?>