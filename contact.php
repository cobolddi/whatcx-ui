<!DOCTYPE html>
<html lang="en">

<head>
    <title>Contact Us- TurboCX</title>
    <meta name="description" content="Get started with WhatsApp by getting in touch with us. Call at: +91-9871069987 Write at: sales@whatcx.com">
    <meta property="og:title" content="Contact Us- TurboCX" />
    <meta property="og:description" content="Get started with WhatsApp by getting in touch with us. Call at: +91-9871069987 Write at: sales@whatcx.com" />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/contact.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/og/contact-og.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/contact-og.png" />
    <meta property="og:image:alt" content="Contact Us- TurboCX" />
    <link rel="canonical" href="https://turbocx.com/contact.php/" />

    <?php @include('template-parts/header.php') ?>

    <main>
        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#"> <img src="assets/images/icons/arrow-right.svg" alt="WhatCX - breadcrumbs icon"> Contact</a></li>
                </ul>
            </div>
        </section>


        <!-- Contact Connect with us -->
        <section class="Section contactSection">
            <div class="container-small">
                <div class="contactSection--inner"> 
                    <div class="contactSection--heading centerSectionHeading">
                        <h1>Connect with us on whatsapp <br /> <span>+919315936656</span></h1>
                        <p>Scan to get started</p>
                    </div>
                    <div class="contactQR">
                        <div class="contactQR--level1">
                            <div class="contactQR--level2">
                                <img src="assets/images/qr-code-new.png" alt="WhatCX - Contact QC Code">
                            </div>
                        </div>
                    </div>
                    <div class="centerSectionHeading">
                        <a href="tel:+919871069987" class="secondaryBtn">Contact sales team</a>
                    </div>
                </div>
            </div>
        </section>


        <section class="Section contactAddSection">
            <div class="container">
                <div class="contactAddress">
                    <div class="contactAddressG leftRightGrid">
                        <div class="contactAddressG-content leftRightGrid-content">
                            <div class="contactAddressG-content--Details">
                                <div>
                                    <h2>Contact</h2>
                                    <a href="tel:+919315936656">+91-9315936656</a>
                                    <a href="mailto:sales@whatcx.com">sales@whatcx.com</a>
                                </div>
                                <div>
                                    <h2>Address</h2>
                                    <p>
                                        EdgeCX Private Limited<br>
                                        55, 2nd Floor, Lane-2 <br>
                                        Westend Marg, Saidullajab <br>
                                        Near Saket Metro Station <br>
                                        New Delhi-110030
                                    </p>
                                </div>
                            </div>
                            <div class="ctaWrap">
                                <a href="https://whatcx.com/sign-up.php" class="primaryBtn">Sign up for free Demo</a>
                            </div>
                        </div>
                        <div class="contactAddressG-graphics leftRightGrid-image">
                            <img loading="lazy" src="assets/images/contactAddress-banner.webp" alt="WhatCX - Contact Banner">
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </main>

    <?php @include('template-parts/footer.php') ?>