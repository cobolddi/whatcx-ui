<!DOCTYPE html>
<html lang="en">

<head>
    <title>Disclamier- TurboCX</title>
    <meta name="description" content="The information contained on this website is for general information purposes only. The information is provided by https://turbocx.com/.">

    <meta property="og:title" content="Disclamier- TurboCX" />
    <meta property="og:description" content="The information contained on this website is for general information purposes only. The information is provided by https://turbocx.com/." />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/disclaimer.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/og/logo-og.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/logo-og.png" />
    <meta property="og:image:alt" content="Disclamier- TurboCX" />
    <link rel="canonical" href="https://turbocx.com/disclaimer.php/" />

    <?php @include('template-parts/header.php') ?>

    <main>

        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Disclaimer</a></li>
                </ul>
            </div>
        </section>

        <section class="Section spBanner-Section">
            <div class="container">
                <div class="spBanner">
                    <div class="spBanner--heading centerSectionHeading">
                        <h1>Disclaimer</h1>
                    </div>
                </div>
            </div>
        </section>


        <section class="Section TCP-Section">
            <div class="container-small">
                <div class="TCP-Section-content">
                    <div class="TCP-Section-content_item">
                        <h4>Disclaimer</h3>
                            <p>The information contained on this website is for general information purposes only. The information is provided by <a href="https://turbocx.com/">https://turbocx.com/</a>. While we strive to keep the information up to date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, availability, or suitability concerning the website or the information, products, services, videos or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
                            <p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of, or in connection with, the use of this website. Through this website, you can link to other websites that are not under the control of TurboCX. We have no control over the nature, content, and availability of those sites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them. Every effort is made to keep the website working and running smoothly. However, TurboCX takes no responsibility for, and will not be liable for, the website being temporarily unavailable due to technical issues beyond our control.</p>
                            <p>
                                This site is not a part of WhatsApp© or Facebook©. Mentioned names or logos are properties of their respective companies. The information on this website is for educational purposes only; we neither support nor be held responsible for any misuse of this info. This tool is not affiliated with any brand or website. Buyer must use the software responsibly and adhere to the website terms or usage policy (or whatever applicable). It is just a standalone tool that can facilitate and extend some options in WhatsApp. It is not a spam tool and is not allowed to use for spamming or violating WhatsApp policies. This tool automates a natural human's behavior to save their time in daily use, and we do not take responsibility for how the buyer uses this software. The product is an automation tool intended to save time doing repetitive actions. All disputes are subject to Israeli jurisdiction.

                            </p>
                    </div>

                </div>
            </div>
        </section>




    </main>

    <?php @include('template-parts/footer.php') ?>