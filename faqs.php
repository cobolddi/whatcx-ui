<!DOCTYPE html>
<html lang="en">

<head>
  <title>Get answers to your common queries- FAQs- TurboCX</title>
  <meta name="description"
    content="Get answers to all your queries. Contact us on +91-9871069987 or discover extensive list of frequently asked questions.">
  <meta property="og:title" content="Get answers to your common queries- FAQs- TurboCX" />
  <meta property="og:description"
    content="Get answers to all your queries. Contact us on +91-9871069987 or discover extensive list of frequently asked questions." />
  <meta property="og:site_name" content="TurboCX">
  <meta property="og:url" content="https://turbocx.com/faqs.php/" />
  <meta property="og:type" content="website">
  <meta property="og:image" content="https://turbocx.com/assets/images/og/faqs-og.png">
  <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/og/faqs-og.png" />
  <meta property="og:image:alt" content="Get answers to your common queries- FAQs- TurboCX" />
  <link rel="canonical" href="https://turbocx.com/faqs.php/" />
  <script type="application/ld+json">
    {
      "@context": "https://schema.org",
      "@type": "FAQPage",
      "mainEntity": [{
        "@type": "Question",
        "name": "What is TurboCX?",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "TurboCX is an all-in-one business WhatsApp tool. It helps you manage your customers on WhatsApp by bringing your whole team together, on one WhatsApp number for your customer’s ease."
        }
      }, {
        "@type": "Question",
        "name": "How can TurboCX help my business grow?",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "TurboCX uses one of the most popular personal communication apps, WhatsApp. Most if not all your customers are probably on WhatsApp already. So of course it’s the natural channel businesses want to use to engage customers. But managing your conversations on WhatsApp is not easy. That’s where TurboCX comes in. TurboCX packs in features like rich text support, live chat, unified dashboard, customer profile, private notes, collaboration on chat etc."
        }
      }, {
        "@type": "Question",
        "name": "Is TurboCX part of WhatsApp?",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "TurboCX is an application that uses WhatsApp. We use the official Whatsapp Cloud API, but WhatsApp does not certify our services. We are an independent providers of this service for a fee."
        }
      }]
    }
  </script>

  <?php @include('template-parts/header.php') ?>

  <main>


    <!-- breadcrumbs -->
    <section class="breadcrumbs">
      <div class="container">
        <ul>
          <li><a href="/">Home</a></li>
          <li><a href="#"> <img src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Faqs</a></li>
        </ul>
      </div>
    </section>


    <!-- faq's search section starts -->
    <section class="Section faqs-search">
      <div class="container">

        <div class="faqs-search__head centerSectionHeading">
          <h1>Frequently Asked Questions</h1>
        </div>

        <div class="faqs-search__box">
          <input class="faqs-search__input" type="text" name="search" placeholder="Describe your issue" id="">
          <button type="submit" class="faqs-search__submit"><img src="assets/images/icons/search.svg"
              alt="TurboCX - Search"></button>
        </div>

      </div>
    </section>
    <!-- faq's search section ends -->


    <!-- Faq's section starts -->
    <section class="Section faqs-section">
      <div class="container">

        <div class="faqs__head centerSectionHeading">
          <h3>If you have any questions, please read through these common queries first or
            <span class="text--orange"><a href="https://turbocx.com/contact.php">contact us.</a></span>
          </h3>
        </div>

        <div class="faqs__wrapper faqs__set faqs_single">

        </div>

      </div>
    </section>
    <!-- Faq's section ends -->

  </main>

  <?php @include('template-parts/footer.php') ?>