<?php @include('template-parts/header.php') ?>

    <section class="Section hpBanner-Section ppBanner-Section">
        <div class="hpBanner">
            <div class="centerSectionHeading">
                <h1>Connecting businesses with customers on Whatsapp. Officially.</h1>
                <p class="bodyCopyLarge">
                    TurboCX software and solutions help businesses manage their Sales,
                    Marketing & Customer Service effectively through Whatsapp Business
                    API.
                </p>
            </div>

        </div>
    </section>

    <section class="Section ppPlans-Section" id="plans">
        <div class="ppPlans">
            <div class="ppPlans-btnWrap">
                <h3>Plans for</h3>
                <div>
                    <a href="#plans">Basic</a>
                    <a href="#plans">Business</a>
                    <a href="#plans">Enterprise</a>
                    <a href="#ppEnterprise-large">Large Enterprise</a>
                    <a href="#ppEnterprise-govt">Goverment</a>
                </div>
            </div>


            <div class="ppPlans-plansWrap">
                <div class="ppPlans-outerGrid">

                
                    <div class="ppPlans-rows plansHeading">
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />
                            <h4>BASIC</h4>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis libero illum dicta debitis excepturi aspernatur?</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />
                            <h4>BASIC</h4>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis libero illum dicta debitis excepturi aspernatur?</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />
                            <h4>BASIC</h4>
                            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Perspiciatis libero illum dicta debitis excepturi aspernatur?</p>
                        </div>
                    </div>
                    <div class="ppPlans-rows plansPricing">
                        <div class="ppPlans-rows--colms">
                            <h5>Price</h5>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <p>Starts from just</p>
                            <h3>2999/month</h3>
                            <span>For small teams just starting out</span>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <p>Starts from just</p>
                            <h3>2999/month</h3>
                            <span>For small teams just starting out</span>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <p>Starts from just</p>
                            <h3>2999/month</h3>
                            <span>For small teams just starting out</span>
                        </div>
                    </div>
                    <div class="ppPlans-rows plansFeatures">
                        <div class="ppPlans-rows--colms">

                            <h5> <?php @include('template-parts/svg/icons/user.php') ?> user</h5>

                            <h5> <?php @include('template-parts/svg/icons/storage.php') ?> storage</h5>

                            <h5> <?php @include('template-parts/svg/icons/contact.php') ?> contacts</h5>

                        </div>
                        <div class="ppPlans-rows--colms">
                            <h5>5</h5>
                            <h5>25GB</h5>
                            <h5>Unlimited Contacts</h5>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <h5>15</h5>
                            <h5>50GB</h5>
                            <h5>Unlimited Contacts</h5>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <h5>25</h5>
                            <h5>75GB</h5>
                            <h5>Unlimited Contacts</h5>
                        </div>
                    </div>
                    <div class="ppPlans-rows plansFeaturesHeading">
                        <div class="ppPlans-rows--colms">
                            <h5>Features</h5>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>
                    <div class="ppPlans-rows featureRow">
                        <div class="ppPlans-rows--colms">
                            <p>Team Inbox</p>
                        </div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms"></div>
                        <div class="ppPlans-rows--colms">
                            <?php @include('template-parts/svg/icons/tick.php') ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </section>

    <section class="Section ppPlansMobileSection">
        <h3>Plans</h3>
        <div class="ppPlansMobile">
            <div class="ppPlansMobile-basic ppPlansMobile-plan">
                <button>
                    <h3><img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />BASIC</h3>
                    <p>2999/month  <?php @include('template-parts/svg/icons/arrow-down.php') ?></p>
                </button>
                <div class="ppPlansMobile-wrap">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h4>
                    <h5><em>For small teams just starting out</em></h5>
                    <p>User <span>5</span></p>
                    <p>Storage <span>25</span></p>
                    <p>Contact <span>Unlimited Contacts</span></p>

                    <ul>
                        <span>Features</span>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                    </ul>
                </div>

            </div>
            <div class="ppPlansMobile-business ppPlansMobile-plan">
                <button>
                
                    <h3> <img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />BUSINESS</h3>
                    <p>2999/month <?php @include('template-parts/svg/icons/arrow-down.php') ?></p>
                </button>
                <div class="ppPlansMobile-wrap">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h4>
                    <h5><em>For small teams just starting out</em></h5>
                    <p>User <span>5</span></p>
                    <p>Storage <span>25</span></p>
                    <p>Contact <span>Unlimited Contacts</span></p>
                    <ul>
                        <span>Features</span>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                    </ul>
                </div>
            </div>
            <div class="ppPlansMobile-enterprise ppPlansMobile-plan">
                <button>
                    <h3> <img loading="lazy" src="assets/images/icons/helpIcon-3.svg" alt="" />Enterprise</h3>
                    <p>2999/month <?php @include('template-parts/svg/icons/arrow-down.php') ?></p>
                </button>
                <div class="ppPlansMobile-wrap">
                    <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h4>
                    <h5><em>For small teams just starting out</em></h5>
                    <p>User <span>5</span></p>
                    <p>Storage <span>25</span></p>
                    <p>Contact <span>Unlimited Contacts</span></p>

                    <ul>
                        <span>Features</span>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                        <li>Team Inbox</li>
                    </ul>
                </div>
            </div>
        
        </div>
    </section>

    <section class="Section ppAvailPaySection">
        <div class="container">
            <div class="ppAvailPay">
                <h3>Available payment
                    methods</h3>
                <div class="ppAvailPay-logos">
                    <img loading="lazy" src="assets/images/logos/Frame506.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame507.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame508.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame509.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame510.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame511.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame512.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame513.webp" alt="">
                    <img loading="lazy" src="assets/images/logos/Frame514.webp" alt="">
                </div>
            </div>
        </div>

    </section>

    <section class="Section ppEnterpriseSection">
        <div class="container">
            <div class="ppEnterprise">
                <div class="ppEnterprise-large" id="ppEnterprise-large">
                    <img loading="lazy" src="assets/images/icons/govt.svg" alt="">
                    <h3>Large Enterprise</h3>
                    <p class="">
                        For businesses with advanced collaboration, access and privacy needs
                    </p>
                    <p>Please contact our sales
                        team for price estimates</p>
                    <a href="" class="secondaryBtn">Contact Us</a>
                </div>
                <div class="ppEnterprise-govt" id="ppEnterprise-govt">
                    <img loading="lazy" src="assets/images/icons/large-ent.svg" alt="">
                    <h3>Large Enterprise</h3>
                    <p class="">
                        For businesses with advanced collaboration, access and privacy needs
                    </p>
                    <p>Please contact our sales
                        team for price estimates</p>
                    <a href="" class="secondaryBtn">Contact Us</a>
                </div>
            </div>
        </div>
    </section>

    <section class="Section faqs-section">

        <div class="faqs__head centerSectionHeading">
            <h1>Frequently Asked Questions

            </h1>
        </div>

        <div class="faqs__wrapper">

            <div class="accordion">
                <div class="accordion-heading">
                    <h4>For what kind of teams is it meant for?</h4>
                </div>

                <div class="accordion-body">

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="accordion">
                <div class="accordion-heading">
                    <h4>For what kind of teams is it meant for?</h4>
                </div>

                <div class="accordion-body">

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="accordion">
                <div class="accordion-heading">
                    <h4>For what kind of teams is it meant for?</h4>
                </div>

                <div class="accordion-body">

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="accordion">
                <div class="accordion-heading">
                    <h4>For what kind of teams is it meant for?</h4>
                </div>

                <div class="accordion-body">

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                </div>
            </div>

            <div class="accordion">
                <div class="accordion-heading">
                    <h4>For what kind of teams is it meant for?</h4>
                </div>

                <div class="accordion-body">

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                    <div class="accordion accordion-sub">
                        <div class="accordion-heading">
                            <h4>For what kind of teams is it meant for?</h4>
                        </div>

                        <div class="accordion-body">
                            <p>At work, you will be exposed to a unique combination of policy, practice and platforms and will have abundant opportunities to interact with the sector’s best as well as top bureaucratic leadership and apply professional skills to complex social challenges.
                            </p>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </section>
    <!-- Faq's section ends -->

    <section class="Section ppTestSection">
        <div class="container">
            <div class="ppTest">
                <div class="ppTestSlide">
                    <button class="ppTestSlide-btnPrev">
                        <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M6.93825 12.2001L16.6605 2.54513L14.0976 0L0 14L14.0976 28L16.6605 25.4549L6.93825 15.7999H29V12.2001H6.93825Z" fill="white" />
                        </svg>

                    </button>
                    <div class="ppTestSlide-slider">
                        <div class="ppTestSlide-slider--item">
                            <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of mind.”</em></p>
                            <div>
                                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                                <h5>Jaden Arnold, Smithsonian Museum.</h5>
                            </div>

                        </div>
                        <div class="ppTestSlide-slider--item">
                            <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of mind.”</em></p>
                            <div>
                                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                                <h5>Jaden Arnold, Smithsonian Museum.</h5>
                            </div>

                        </div>
                        <div class="ppTestSlide-slider--item">
                            <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of mind.”</em></p>
                            <div>
                                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                                <h5>Jaden Arnold, Smithsonian Museum.</h5>
                            </div>

                        </div>
                        <div class="ppTestSlide-slider--item">
                            <p> <em> “We felt the difference. Customers are very different on TurboCX. The way their app is setup, we couldn’t make a mistake. Without a doubt, TurboCX is a tool for every business owner’s peace of mind.”</em></p>
                            <div>
                                <img loading="lazy" src="assets/images/icons/testplaceholder.svg" alt="" />
                                <h5>Jaden Arnold, Smithsonian Museum.</h5>
                            </div>

                        </div>
                    </div>
                    <button class="ppTestSlide-btnNext">
                        <svg width="29" height="28" viewBox="0 0 29 28" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M22.0618 12.2001L12.3395 2.54513L14.9024 0L29 14L14.9024 28L12.3395 25.4549L22.0618 15.7999H0V12.2001H22.0618Z" fill="white" />
                        </svg>
                    </button>

                </div>
            </div>
        </div>
    </section>

    <section class="Section ppBrochureSection">
        <div class="ppBrochure">
            <h5>Download Brochure</h5>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
            <div>
                <form class="brochure-form" action="email/thanks.php" method="post">
                    <input type="email" placeholder="Enter your email ID." required>
                    <input type="hidden" value="TurboCX Download Brochure">
                    <button type="submit">Download</button>
                    <!-- <a download class="filePdf" href="assets/images/pdf/dummy.pdf" hidden></a> -->
                </form>
                <div class="info"></div>
                <div class="success">Thanks for sharing the email address. We will mail the brochure to you shortly.</div>
            </div>
        </div>
    </section>

<?php @include('template-parts/footer.php') ?>