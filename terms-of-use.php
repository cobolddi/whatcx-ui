<!DOCTYPE html>
<html lang="en">

<head>
    <title>Terms of Use- TurboCX</title>
<meta name="description" content="These Terms of Use (“Terms of Use”) govern the access or usage of https://turbocx.com/. By continuing to use the website, you accept our terms of use.  ">

<meta property="og:title" content="Terms of Use- TurboCX" />
<meta property="og:description" content="These Terms of Use (“Terms of Use”) govern the access or usage of https://turbocx.com/. By continuing to use the website, you accept our terms of use. " />
<meta property="og:site_name" content="TurboCX">
<meta property="og:url" content="https://turbocx.com/terms-of-use.php/" />
<meta property="og:type" content="website">
<meta property="og:image" content="https://turbocx.com/assets/images/guide.webp">
<meta property="og:image:secure_url" content="https://turbocx.com/assets/images/guide.webp" />
<meta property="og:image:alt" content="Terms of Use- TurboCX" />

<link rel="canonical" href="https://turbocx.com/terms-of-use.php/" />

<?php @include('template-parts/header.php') ?>

<main>

    <!-- breadcrumbs -->
    <section class="breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="/">Home</a></li>
                <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Terms Of Use</a></li>
            </ul>
        </div>
    </section>

    <section class="Section spBanner-Section">
        <div class="container">
            <div class="spBanner">
                <div class="spBanner--heading centerSectionHeading">
                    <h1>Terms Of Use</h1>
                </div>
            </div>
        </div>
    </section>


    <section class="Section TCP-Section">
        <div class="container-small">
            <div class="TCP-Section-content">
                <div class="TCP-Section-content_item">
                    <h4>Terms Of Use</h3>
                    <p>These Terms of Use (“Terms of Use”) govern the access or usage of <a href="https://turbocx.com/">https://turbocx.com/</a>. By continuing to use the website, you accept our terms of use. Please read these carefully. We reserve the right to change, remove or replace any part of these Terms of Use by posting updates and/or changes to our website. If new features or tools are added to the website these shall also be subject to the Terms of Use.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Eligibility for use</h3>
                    <p>If you use the website, then you automatically attest that you are above the age of 18 years and will not use the website for any illegal or prohibited acts, and will use the website in accordance with the prevailing laws and regulations of the land.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Privacy and your information</h3>
                    <p>Our Privacy Policy governs the use and protection of the information and data you share in the course of using <a href="https://turbocx.com">https://turbocx.com</a> or our products and services.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Charges for use</h3>
                    <p>There is no charge for browsing the website. We reserve the right to change this policy, however, and posting the notice for such a change on the website will automatically enforce it. </p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Your rights</h3>
                    <p>You have limited rights to access and browse the website. You do not have any rights to any commercial use of the data/information on the website by any means whatsoever or to the collection and modification of the information on the pages of the website for commercial applications. </p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Your conduct</h3>
                    <p>Your use of the website must not be unlawful or cause or intend to cause interruption, damage or harm to the website or our services.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Disclaimer</h3>
                    <p>We have done our best to ensure the accuracy of information and content on the website but do not provide any warranty of the same. For your breach of these Terms of Use or your violation of any laws, the sole responsibility will be yours. You agree to not hold the website or GLB International Services responsible for claims or demands on your behalf under any statute, contract or otherwise. By visiting <a href="https://turbocx.com/">https://turbocx.com/</a> or sending us emails, you are electronically communicating with us. Your consent to receive communications from us is implied including via SMS, email or phone. We will not be held responsible for any delays or non-compliance for reasons beyond our control.</p>
                </div>
                <div class="TCP-Section-content_item">
                    <h4>Queries and complaints</h3>
                    <p>This website is owned and operated by EdgeCX Pvt Ltd. For any questions or complaints, please write to <a href="mainto:sales@whatcx.com">sales@whatcx.com</a>.</p>
                </div>
            </div>
        </div>
    </section>

    

   
</main>

<?php @include('template-parts/footer.php') ?>