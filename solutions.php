<!DOCTYPE html>
<html lang="en">

<head>
    <title>Automate customer support for your business with WhatsApp Cloud API</title>
    <meta name="description" content="Get to know how our customized WhatsApp Cloud API solutions can meet specific business needs.">
    <meta property="og:title" content="Automate customer support for your business with WhatsApp Cloud API" />
    <meta property="og:description" content="Get to know how our customized WhatsApp Cloud API solutions can meet specific business needs." />
    <meta property="og:site_name" content="TurboCX">
    <meta property="og:url" content="https://turbocx.com/solutions.php/" />
    <meta property="og:type" content="website">
    <meta property="og:image" content="https://turbocx.com/assets/images/solutions/solution-apparel1.png">
    <meta property="og:image:secure_url" content="https://turbocx.com/assets/images/solutions/solution-apparel1.png" />
    <meta property="og:image:alt" content="Automate customer support for your business with WhatsApp Cloud API" />

    <link rel="canonical" href="https://turbocx.com/solutions.php/" />
    <?php @include('template-parts/header.php') ?>

    <main>

        <!-- breadcrumbs -->
        <section class="breadcrumbs">
            <div class="container">
                <ul>
                    <li><a href="/">Home</a></li>
                    <li><a href="#"> <img  src="assets/images/icons/arrow-right.svg" alt="TurboCX - breadcrumbs icon"> Explore all industries </a></li>
                </ul>
            </div>
        </section>

        <section class="Section spBanner-Section">
            <div class="container">
                <div class="spBanner">
                    <div class="spBanner--heading centerSectionHeading">
                        <h1>Delivering maximum customer satisfaction across every industry.</h1>
                        <p>See what our industry-specific solutions can do to your business.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="Section blogCard-section hpCrm-Section spSolutionTypes--Section">

            <div class="container-small">

                <div class="threeColWithCenteredOrphans">

                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/solution-jewellery.php">
                            <img  src="assets/images/solutions/solution-jewellery1.png" alt="TurboCX - Jewellery industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Jewellery industry</h3>
                            <p>Leverage your jewellery business with TurboCX automation and give customers a superb experience.</p>
                            <a href="https://turbocx.com/solution-jewellery.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/solution-real-estate.php">
                            <img  src="assets/images/solutions/solution-realestate1.png" alt="TurboCX - Real Estate industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Real Estate industry</h3>
                            <p>Perfect solutions to transform the real estate industry to streamline your business and communicate more effectively.</p>
                            <a href="https://turbocx.com/solution-real-estate.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/solution-apparel.php">
                            <img src="assets/images/solutions/solution-apparel1.png" alt="TurboCX - Apparel industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Apparel industry</h3>
                            <p>Incorporate the power of conversational commerce into your fashion brands and retails to effectively leverage messaging.</p>
                            <a href="https://turbocx.com/solution-apparel.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/solution-educational.php">
                            <img loading="lazy" src="assets/images/solutions/solution-education1.png" alt="TurboCX - Education industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Education industry</h3>
                            <p>Elevate the e-Learning platforms into a more engaging, user-focused, and concise solution for your customers.</p>
                            <a href="https://turbocx.com/solution-educational.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/mba-schools.php">
                            <img loading="lazy" src="assets/images/solutions/solution-mba1.png" alt="TurboCX - MBA schools solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">MBA schools</h3>
                            <p>Elevate the e-Learning platforms into a more engaging, user-focused, and concise solution for your customers.</p>
                            <a href="https://turbocx.com/mba-schools.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>

                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/salon-industry.php">
                            <img loading="lazy" src="assets/images/solutions/solution-salon1.png" alt="TurboCX - Salon industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Salon industry</h3>
                            <p>Automate and enhance marketing management for your beauty salon or spa to drive customer engagement & in store visits.</p>
                            <a href="https://turbocx.com/salon-industry.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="threeColWithCenteredOrphans loadMoreOptions d-n">
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/retail-industry.php">
                            <img loading="lazy" src="assets/images/solutions/solution-retail1.png" alt="TurboCX - Retail industry solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Retail industry</h3>
                            <p>Power customer experience, growth, conversion, and higher ROI for retail businesses across different verticals.</p>
                            <a href="https://turbocx.com/retail-industry.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/manufacturing-unit.php">
                            <img loading="lazy" src="assets/images/solutions/solution-manufacturing1.png" alt="TurboCX - Manufacturing unit solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Manufacturing unit</h3>
                            <p>Tap into one of the most popular channels in the world and digitize your conversations to boost engagement & efficiency.</p>
                            <a href="https://turbocx.com/manufacturing-unit.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>

                    <div class="blogCard blogCard--shadow colItems">
                        <a href="https://turbocx.com/solution-coaching-institutes.php">
                            <img loading="lazy" src="assets/images/solutions/coaching.png" alt="TurboCX - Coaching institutes solution">
                        </a>
                        <div class="blogCard__content">
                            <h3 class="blogCard__heading">Coaching institutes</h3>
                            <p>Give your institute a leg up and stand out from your competition with direct communications & instant connections.</p>
                            <a href="https://turbocx.com/solution-coaching-institutes.php" class="btn btn--text btn--orange">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>

                </div>

                <div class="b2cwhatcx--btn">
                    <a href="javascript:;" class="secondaryBtn secondaryBtn--white loadMoreBtn">
                        Load More
                    </a>
                </div>

            </div>
        </section>

        <!-- <section class="Section spSolutionTypes--Section">
        <div class="container-small">
            <div class="spSolutionTypes">
                <div class="spSolutionTypesG leftRightGrid">
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/solution-jewellery.png" alt="">
                    </div>
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Jewellery industry</h3>
                            <p>Leverage your jewellery business with TurboCX automation and give customers a superb
                                experience.</p>
                            <a class="btn--text" href="solution-jewellery" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid">
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Real Estate industry</h3>
                            <p>Perfect solutions to transform the real estate industry to streamline your business and
                                communicate more effectively.</p>
                            <a class="btn--text" href="solution-real-estate" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/solution-realEstate.png" alt="">
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid">
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/solution-apparel.png" alt="">
                    </div>
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Apparel industry</h3>
                            <p>Incorporate the power of conversational commerce into your fashion brands and retails to
                                effectively leverage messaging.</p>
                            <a class="btn--text" href="solution-apparel" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid ">
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Education industry</h3>
                            <p>Elevate the e-Learning platforms into a more engaging, user-focused, and concise solution
                                for your customers.</p>
                            <a class="btn--text" href="solution-educational" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/solution-education.png" alt="">
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid">
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/mba-schools.png" alt="">
                    </div>
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>MBA schools</h3>
                            <p>Elevate the e-Learning platforms into a more engaging, user-focused, and concise solution for your customers.</p>
                            <a class="btn--text" href="mba-schools" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid ">
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Salon industry</h3>
                            <p>Automate and enhance marketing management for your beauty salon or spa to drive customer engagement & in store visits.</p>
                            <a class="btn--text" href="salon-industry" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/solon-industry.png" alt="">
                    </div>
                </div>
                <div class="spSolutionTypesG leftRightGrid">
                    <div class="spSolutionTypesG-graphics leftRightGrid-image">
                        <img loading="lazy" src="assets/images/solutions/Retail-Industry.png" alt="">
                    </div>
                    <div class="spSolutionTypesG-content leftRightGrid-content">
                        <div class="spSolutionTypesG-content--details">
                            <h3>Retail industry</h3>
                            <p>Power customer experience, growth, conversion, and higher ROI for retail businesses across different verticals.</p>
                            <a class="btn--text" href="retail-industry" tabindex="0">
                                Explore more <?php @include('assets/images/icons/arrow-orange.svg'); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->




        <!-- <?php //@include('template-parts/testimonialSection.php') 
                ?>

    <?php //@include('template-parts/brochureSection.php') 
    ?> -->
    </main>

    <?php @include('template-parts/footer.php') ?>