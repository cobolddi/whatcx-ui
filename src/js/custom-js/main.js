
jQuery(document).ready(function ($) {
  // Feature Grid Filter
  $(".grid").isotope({
    itemSelector: ".hpCrm-featureGrid--item",
    layoutMode: "fitRows",
  });

  // for country code in onboarding form 
  $('.js-example-basic-single').select2();

  
  // filter items on button click
  $(".hpCrm-filterBtnWrap").on("click", "button", function () {
    var filterValue = $(this).attr("data-filter");
    $(".grid").isotope({ filter: filterValue });
    $(".hpCrm-filterBtnWrap button").removeClass("active");
    $(this).addClass("active");
  });

  // Tabs
  $(".fpFeature-nav ol li").on("click", function () {
    let id = $(this).attr("tabID");
    if ($("#" + id).hasClass("d-n")) {
      $("#" + $(".fpFeature-nav ol li.active").attr("tabID")).addClass("d-n");
      $(".fpFeature-nav ol li.active").removeClass("active");
      $(this).addClass("active");
      $("#" + id).removeClass("d-n");
    }
  });

  // Features and help Slider on homepage
  $(".hpHelp-slideWrap").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    accesibility: false,
    draggable: false,
    swipe: false,
    touchMove: false,
  });

  $("button[data-slide]").click(function (e) {
    e.preventDefault();
    $("button[data-slide]").each(function () {
      $(this).removeClass("active");
    });
    let slideno = $(this).data("slide");
    $(this).addClass("active");
    $(".hpHelp-slideWrap").slick("slickGoTo", slideno - 1);
  });

  //ppTestSlide-slider
  $(".ppTestSlide-slider").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: ".ppTestSlide-btnNext",
    prevArrow: ".ppTestSlide-btnPrev",
    fade: false,
  });

  // notification slider
  $(".notificationSlider").slick({
    vertical: true,
    verticalSwiping: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 2000,
    arrows: false,
    dots: false,
  });

  // recent slider
  $(".recent-slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    nav:true,
    autoplaySpeed: 2000,
    nextArrow: ".blogCard-btnNext",
    prevArrow: ".blogCard-btnPrev",
    fade: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
  $(".grid-three-ab").slick({
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: false,
    nav:true,
    // autoplaySpeed: 4000,
    nextArrow: ".next",
    prevArrow: ".prev",
    fade: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        },
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        },
      },
    ],
  });
  $(".scroll-logos").slick({
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 1000,
    nextArrow: ".next-1",
    prevArrow: ".prev-1",
    fade: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 601,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
    ],
  });

  // notification slider
  const notificationClose = document.querySelector(
    ".notification__wrapper > img"
  );
  const notification = document.querySelector(".notification");

  notificationClose.addEventListener("click", function () {
    notification.classList.add("closed");
  });

  // Dark Switch
  const darkButton = document.querySelector(".nav-actions--darkSwitch input");
  let themeMode = window.matchMedia("(prefers-color-scheme: dark)");
  let light = "light";
  let dark = "dark";

  const darkSwitch = () => {
    const lightTheme = () => {
      darkButton.checked = true;
      localStorage.setItem("theme", light);
      document.body.classList.remove(dark);
      document.body.classList.add(light);
      return;
    };
    const darkTheme = () => {
      darkButton.checked = false;
      document.body.classList.remove(light);
      document.body.classList.add(dark);
      localStorage.setItem("theme", dark);
      return;
    };

    const onInit = () => {
      if (localStorage.getItem("theme") === null) {
        if (!themeMode.matches) lightTheme();
        else if (themeMode.matches) darkTheme();
      } else {
        if (localStorage.getItem("theme") === dark) darkTheme();
        else if (localStorage.getItem("theme") === light) lightTheme();
      }
    };
    onInit();

    const bgSwitch = () => {
      if (localStorage.getItem("theme") === dark) lightTheme();
      else darkTheme();
    };

    darkButton.addEventListener("change", () => {
      bgSwitch();
    });
  };
  darkSwitch();

  //Sub Menu Expand
  const menu = document.querySelector(".nav-js");
  const menuItems = Array.from(menu.querySelectorAll(".nav-links > ul > li"));
  const hamBtn = menu.querySelector(".navHamBtn");
  const hadSubMenu = menuItems.filter((item) => {
    return item.querySelector("ul");
  });

  hadSubMenu.forEach((item) => {
    item.addEventListener("click", () => {
      item.classList.toggle("expanded");
    });
  });

  hamBtn.addEventListener("click", () => {
    document.querySelector("nav").classList.toggle("closed");
    hamBtn.classList.toggle("on");
    document.body.classList.toggle("no-scroll");
  });

  const plansBtns = Array.from(
    document.querySelectorAll(".ppPlansMobile-plan button")
  );

  if (plansBtns) {
    plansBtns.forEach((btn) => {
      btn.addEventListener("click", () => {
        btn.classList.toggle("active");
      });
    });
  }

  // faqs search

  // faq's accordion functionality starts
  const faqWrapper = document.querySelector(".faqs__wrapper");

  if (faqWrapper) {
    faqWrapper.addEventListener("click", function (e) {
      // faqs elements
      const faqs = document.querySelectorAll(".faqs__wrapper > .accordion");
      const subFaqs = document.querySelectorAll(
        ".faqs__wrapper .accordion-sub"
      );
      const parent = e.target.parentElement;

      if (e.target.classList.contains("accordion-heading")) {
        if (parent.classList.contains("accordion-sub")) {
          if (!parent.classList.contains("active")) {
            subFaqs.forEach((faq) => faq.classList.remove("active"));
            parent.classList.add("active");
            return;
          }
          parent.classList.remove("active");
          return;
        } else if (parent.classList.contains("accordion")) {
          if (!parent.classList.contains("active")) {
            faqs.forEach((faq) => faq.classList.remove("active"));
            parent.classList.add("active");
            return;
          }
          parent.classList.remove("active");
          return;
        }
      }
    });
  }
  // faq's accordion functionality ends

  // Search functionality starts
  const searchEle = document.querySelector(".faqs-search__input");
  const setSearchList = document.querySelector(".faqs__set");

  var faqsList = [
    {
      name: "What is TurboCX?",
      des: "<p>TurboCX is an all-in-one business WhatsApp tool. It helps you manage your customers on WhatsApp by bringing your whole team together, on one WhatsApp number for your customer’s ease.</p>",
    },
    {
      name: "How can TurboCX help my business grow?",
      des: "<p>TurboCX uses one of the most popular personal communication apps, WhatsApp. Most if not all your customers are probably on WhatsApp already. So of course it’s the natural channel businesses want to use to engage customers. But managing your conversations on WhatsApp is not easy. That’s where TurboCX comes in. TurboCX packs in features like rich text support, live chat, unified dashboard, customer profile, private notes, collaboration on chat etc.</p>",
    },
    {
      name: "Is TurboCX part of WhatsApp?",
      des: "<p>TurboCX is an application that uses WhatsApp. We use the official Whatsapp Cloud API, but WhatsApp does not certify our services. We are an independent providers of this service for a fee.</p> ",
    },
    {
      name: "Can more than one agent respond to WhatsApp messages using TurboCX?",
      des: "<p>Yes! You can bring your whole team together on TurboCX, sales, marketing and customer service - on one WhatsApp number! Multiple agents can easily collaborate on a single WhatsApp conversation as well using TurboCX.</p>",
    },
    {
      name: "What is WhatsApp Conversation Based pricing??",
      des: "<p>WhatsApp Conversations are an important term because your business is charged based on conversations, not on the number of messages sent. Any message sent in a 24 hour time-window is regarded as a conversation. When the first message is delivered - whether sent by you or your customer - that is when WhatsApp considers a conversation (or session) to have started.</p>",
    },
    {
      name: "What is WhatsApp Conversation Based pricing?",
      des: `<p>Once the conversation is initiated the session is considered to be active. You are charged upon message delivery. The pricing is split into two heads, depending on who has initiated the conversation.
    <ol>
      <li> Business initiated conversation</li>
      <li>Customer initiated conversation</li>
    </ol>
    <a href="contact">Connect with us</a> to know more about Conversation Based Pricing.
    </p>`,
    },
    {
      name: "When do session windows start in a conversation? What does it mean for my business?",
      des: `<p>Conversations or sessions start when the message is delivered. When your business responds to a message sent by your customer, that 24 hour conversation window or session goes live. This is important because your business can only communicate in free form conversation with customers during such conversations or sessions.</p>`,
    },
    {
      name: "When does my business get charged?",
      des: `<p>Your business will be charged only once the message is delivered. During this active session you can send as many messages as you want - you will only be charged once during this conversation window.</p>`,
    },
    {
      name: "How are businesses charged for messages? Is it charged for each message?",
      des: `<p>Businesses are charged for a conversation or session, instead of individual messages. When you initiate a conversation or reply to a customer, a 24 hour conversation window begins. Unlimited messages can be exchanged during this 24 hour time period. You are only charged once for the initial message depending on whether it was your business or the customer who initiated the conversation.</p>`,
    },
    {
      name: "What is WhatsApp Template Message?",
      des: `<p>Outside of the 24 hour conversation window, businesses can only send messages to customers in a pre-approved template. These templates must be submitted from the TurboCX dashboard and approved by WhatsApp. Template are made up of set text with placeholders variables.</p>`,
    },
    {
      name: "What is the pricing you follow?",
      des: `<p>TurboCX has plans for all business sizes. For more information, see our <a href="pricing.php">pricing page</a>.</p>`,
    },
    {
      name: "Can I change from one plan to the other?",
      des: `<p>Yes. You can always migrate between plans. <a href="contact">Contact us</a> for more information.</p>`,
    },
    {
      name: "I want to downgrade plans. Will I get a refund?",
      des: `<p>Yes, you will receive a refund on a pro-rata basis according to the usage months and the cost incurred for the initial setup. For more details kindly reach out to us.</p>`,
    },
    {
      name: "Can I cancel my subscription?",
      des: `<p>Yes. That’s right. However, the number registered with WhatsApp Cloud API is non-transferable. You won’t be able to downgrade back to a standard WhatsApp Business Account.</p>`,
    },
    {
      name: "How does TurboCX’s free trial work?",
      des: `<p>Our free trial brings you all of the powerful features of TurboCX for you to test and explore. However, you are limited to sending messages to 10 contacts. This list of contacts is refreshed every hour.</p>
    <p>You’ll be able to use our WhatsApp number to receive messages from your customers, to interact with them and set up auto-responders to see TurboCX in action.</p>`,
    },
    {
      name: "What methods of payment are accepted?",
      des: `<p>You can recharge your wallet on TurboCX by using credit cards, debit cards, net banking, and check.</p>`,
    },
    {
      name: "I have paid my annual subscription, why do I need a monthly invoice? Why do I need balance in my wallet?",
      des: `<p>Your subscription to TurboCX is a lumpsum payment. But to send messages and communicate with your customers you need to maintain a wallet balance because WhatsApp messages are billed on actuals.</p>`,
    },
    {
      name: "I want to increase the number of agents that can login to TurboCX? What are the charges?",
      des: `<p>You can upgrade your plan, or contact us for a customized solution.</p>`,
    },
    {
      name: "Does TurboCX offer training in using and help for setting up the WhatsApp channel for business?",
      des: `<p>Yes. With the TurboCX managed service you get all the help you need. With this optional service, we can help you setup your Whatsapp Cloud API, content and template strategy, and even help with the Green Tick Verification.</p>`,
    },
    {
      name: "How can I get the WhatsApp Business Verified Green Tick?",
      des: `<p>The green tick means that you have an Official Business Account on WhatsApp. This green tick gives your business credibility and lets customers see your business profile, even if they have not saved your phone number as a contact.
    </p>
    <p>Applying for the green tick is free, however, approval is solely at WhatsApp’s discretion. We can help you with the process and our expertise can come in handy. But we can’t influence WhatsApp’s decision. Green tick verification is based on many factors, so to know more, please get in touch with us.</p>`,
    },
    {
      name: "Is TurboCX available on mobile?",
      des: `<p>Yes. We have a companion mobile app where you can see & assign chats, check active chats, start free form conversations, view contacts, edit customer profiles, and do much more.
    </p>`,
    },
    {
      name: "If I have an existing WhatsApp Business Account, can I use it with TurboCX?",
      des: `<p>Yes. But you’ll have to delete the old Business Account. This is an irreversible step, so please backup your chats and contacts. Kindly contact us to learn more.
    </p>`,
    },
  ];

  function allFaqsList() {
    setSearchList.innerHTML = faqsList
      ?.map((element) => {
        return `<div class="accordion">
              <div role="button" class="accordion-heading">
                <h4>${element?.name}</h4>
              </div>
              <div class="accordion-body">
                ${element?.des}
              </div>
            </div>
          `;
      })
      .join("");
  }

  setSearchList && allFaqsList();

  const options = {
    shouldSort: true,
    matchAllTokens: true,
    findAllMatches: true,
    threshold: 0.4,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: ["name", "des"],
  };

  const fuse = new Fuse(faqsList, options);

  if (searchEle && setSearchList) {
    searchEle.addEventListener("input", function () {
      const resultJSON = fuse.search(searchEle.value);
      // result.value = JSON.stringify(resultJSON, null, 3);

      if (searchEle.value !== "") {
        setSearchList.innerHTML = resultJSON
          ?.map((element) => {
            return `<div class="accordion">
                  <div role="button" class="accordion-heading">
                    <h4>${element?.item?.name}</h4>
                  </div>
                  <div class="accordion-body">
                    ${element?.item?.des}
                  </div>
              </div>
              `;
          })
          .join("");
      } else {
        allFaqsList();
      }
    });
    // search ends
  }

  // share
  const socialShare = document.querySelectorAll(".blogSingle__social > li");
  if (socialShare) {
    socialShare.forEach((item) => {
      let socialLink = item.querySelector("a").getAttribute("href");
      socialLink += window.location.href;
      item.querySelector("a").setAttribute("href", socialLink);
    });
  }
  const shareCopy = document.querySelector("#shareCopy");
  if (shareCopy) {
    shareCopy.addEventListener("click", function (e) {
      e.preventDefault();
      shareCopy.querySelector(".copyText").classList.add("show");
      const copyLink = shareCopy.getAttribute("href");
      navigator.clipboard.writeText(copyLink);
      setTimeout(function () {
        shareCopy.querySelector(".copyText").classList.remove("show");
      }, 2000);
    });
  }

  // form
  const reportForm = document.querySelector(".report-form");
  const newsletterForm = document.querySelector(".newsletter-form");
  const brochureForm = document.querySelector(".brochure-form");

  reportForm && formHandler(reportForm);
  newsletterForm && formHandler(newsletterForm);
  brochureForm && formHandler(brochureForm);

  function formHandler(form) {
    form.addEventListener("submit", function (e) {
      form.nextElementSibling.innerHTML = "Sending...";
      form.nextElementSibling.style.display = "block";
      e.preventDefault();
      const download = form?.querySelector(".filePdf");
      var formData = {
        email: e.target[0].value,
        subject: e.target[1].value,
      };
      $.ajax({
        type: "POST",
        url: "email/thanks.php",
        data: formData,
        dataType: "json",
      }).done(function (data) {
        if (data.success) {
          form.nextElementSibling.style.display = "none";
          form.nextElementSibling.nextElementSibling.style.display = "block";
          e.target[0].value = "";
          setTimeout(function () {
            form.nextElementSibling.nextElementSibling.style.display = "none";
          }, 5000);
          form !== newsletterForm && download?.click();
        } else {
          form.nextElementSibling.innerHTML =
            "Something wrong, Could not send mail!";
          e.target[0].value = "";
        }
      });
    });
  }

  // sign up form
  const countries = [
  {
    code: "AF",
    phone: 93,
  },
  {
    code: "AX",
    phone: 358,
  },
  {
    code: "AL",
    phone: 355,
  },
  {
    code: "DZ",
    phone: 213,
  },
  {
    code: "AS",
    phone: 1684,
  },
  {
    code: "AD",
    phone: 376,
  },
  {
    code: "AO",
    phone: 244,
  },
  {
    code: "AI",
    phone: 1264,
  },
  {
    code: "AQ",
    phone: 672,
  },
  {
    code: "AG",
    phone: 1268,
  },
  {
    code: "AR",
    phone: 54,
  },
  {
    code: "AM",
    phone: 374,
  },
  {
    code: "AW",
    phone: 297,
  },
  {
    code: "AU",
    phone: 61,
  },
  {
    code: "AT",
    phone: 43,
  },
  {
    code: "AZ",
    phone: 994,
  },
  {
    code: "BS",
    phone: 1242,
  },
  {
    code: "BH",
    phone: 973,
  },
  {
    code: "BD",
    phone: 880,
  },
  {
    code: "BB",
    phone: 1246,
  },
  {
    code: "BY",
    phone: 375,
  },
  {
    code: "BE",
    phone: 32,
  },
  {
    code: "BZ",
    phone: 501,
  },
  {
    code: "BJ",
    phone: 229,
  },
  {
    code: "BM",
    phone: 1441,
  },
  {
    code: "BT",
    phone: 975,
  },
  {
    code: "BO",
    phone: 591,
  },
  {
    code: "BQ",
    phone: 599,
  },
  {
    code: "BA",
    phone: 387,
  },
  {
    code: "BW",
    phone: 267,
  },
  {
    code: "BV",
    phone: 55,
  },
  {
    code: "BR",
    phone: 55,
  },
  {
    code: "IO",
    phone: 246,
  },
  {
    code: "BN",
    phone: 673,
  },
  {
    code: "BG",
    phone: 359,
  },
  {
    code: "BF",
    phone: 226,
  },
  {
    code: "BI",
    phone: 257,
  },
  {
    code: "KH",
    phone: 855,
  },
  {
    code: "CM",
    phone: 237,
  },
  {
    code: "CA",
    phone: 1,
  },
  {
    code: "CV",
    phone: 238,
  },
  {
    code: "KY",
    phone: 1345,
  },
  {
    code: "CF",
    phone: 236,
  },
  {
    code: "TD",
    phone: 235,
  },
  {
    code: "CL",
    phone: 56,
  },
  {
    code: "CN",
    phone: 86,
  },
  {
    code: "CX",
    phone: 61,
  },
  {
    code: "CC",
    phone: 672,
  },
  {
    code: "CO",
    phone: 57,
  },
  {
    code: "KM",
    phone: 269,
  },
  {
    code: "CG",
    phone: 242,
  },
  {
    code: "CD",
    phone: 242,
  },
  {
    code: "CK",
    phone: 682,
  },
  {
    code: "CR",
    phone: 506,
  },
  {
    code: "CI",
    phone: 225,
  },
  {
    code: "HR",
    phone: 385,
  },
  {
    code: "CU",
    phone: 53,
  },
  {
    code: "CW",
    phone: 599,
  },
  {
    code: "CY",
    phone: 357,
  },
  {
    code: "CZ",
    phone: 420,
  },
  {
    code: "DK",
    phone: 45,
  },
  {
    code: "DJ",
    phone: 253,
  },
  {
    code: "DM",
    phone: 1767,
  },
  {
    code: "DO",
    phone: 1809,
  },
  {
    code: "EC",
    phone: 593,
  },
  {
    code: "EG",
    phone: 20,
  },
  {
    code: "SV",
    phone: 503,
  },
  {
    code: "GQ",
    phone: 240,
  },
  {
    code: "ER",
    phone: 291,
  },
  {
    code: "EE",
    phone: 372,
  },
  {
    code: "ET",
    phone: 251,
  },
  {
    code: "FK",
    phone: 500,
  },
  {
    code: "FO",
    phone: 298,
  },
  {
    code: "FJ",
    phone: 679,
  },
  {
    code: "FI",
    phone: 358,
  },
  {
    code: "FR",
    phone: 33,
  },
  {
    code: "GF",
    phone: 594,
  },
  {
    code: "PF",
    phone: 689,
  },
  {
    code: "TF",
    phone: 262,
  },
  {
    code: "GA",
    phone: 241,
  },
  {
    code: "GM",
    phone: 220,
  },
  {
    code: "GE",
    phone: 995,
  },
  {
    code: "DE",
    phone: 49,
  },
  {
    code: "GH",
    phone: 233,
  },
  {
    code: "GI",
    phone: 350,
  },
  {
    code: "GR",
    phone: 30,
  },
  {
    code: "GL",
    phone: 299,
  },
  {
    code: "GD",
    phone: 1473,
  },
  {
    code: "GP",
    phone: 590,
  },
  {
    code: "GU",
    phone: 1671,
  },
  {
    code: "GT",
    phone: 502,
  },
  {
    code: "GG",
    phone: 44,
  },
  {
    code: "GN",
    phone: 224,
  },
  {
    code: "GW",
    phone: 245,
  },
  {
    code: "GY",
    phone: 592,
  },
  {
    code: "HT",
    phone: 509,
  },
  {
    code: "HM",
    phone: 0,
  },
  {
    code: "VA",
    phone: 39,
  },
  {
    code: "HN",
    phone: 504,
  },
  {
    code: "HK",
    phone: 852,
  },
  {
    code: "HU",
    phone: 36,
  },
  {
    code: "IS",
    phone: 354,
  },
  {
    code: "IN",
    phone: 91,
  },
  {
    code: "ID",
    phone: 62,
  },
  {
    code: "IR",
    phone: 98,
  },
  {
    code: "IQ",
    phone: 964,
  },
  {
    code: "IE",
    phone: 353,
  },
  {
    code: "IM",
    phone: 44,
  },
  {
    code: "IL",
    phone: 972,
  },
  {
    code: "IT",
    phone: 39,
  },
  {
    code: "JM",
    phone: 1876,
  },
  {
    code: "JP",
    phone: 81,
  },
  {
    code: "JE",
    phone: 44,
  },
  {
    code: "JO",
    phone: 962,
  },
  {
    code: "KZ",
    phone: 7,
  },
  {
    code: "KE",
    phone: 254,
  },
  {
    code: "KI",
    phone: 686,
  },
  {
    code: "KP",
    phone: 850,
  },
  {
    code: "KR",
    phone: 82,
  },
  {
    code: "XK",
    phone: 381,
  },
  {
    code: "KW",
    phone: 965,
  },
  {
    code: "KG",
    phone: 996,
  },
  {
    code: "LA",
    phone: 856,
  },
  {
    code: "LV",
    phone: 371,
  },
  {
    code: "LB",
    phone: 961,
  },
  {
    code: "LS",
    phone: 266,
  },
  {
    code: "LR",
    phone: 231,
  },
  {
    code: "LY",
    phone: 218,
  },
  {
    code: "LI",
    phone: 423,
  },
  {
    code: "LT",
    phone: 370,
  },
  {
    code: "LU",
    phone: 352,
  },
  {
    code: "MO",
    phone: 853,
  },
  {
    code: "MK",
    phone: 389,
  },
  {
    code: "MG",
    phone: 261,
  },
  {
    code: "MW",
    phone: 265,
  },
  {
    code: "MY",
    phone: 60,
  },
  {
    code: "MV",
    phone: 960,
  },
  {
    code: "ML",
    phone: 223,
  },
  {
    code: "MT",
    phone: 356,
  },
  {
    code: "MH",
    phone: 692,
  },
  {
    code: "MQ",
    phone: 596,
  },
  {
    code: "MR",
    phone: 222,
  },
  {
    code: "MU",
    phone: 230,
  },
  {
    code: "YT",
    phone: 269,
  },
  {
    code: "MX",
    phone: 52,
  },
  {
    code: "FM",
    phone: 691,
  },
  {
    code: "MD",
    phone: 373,
  },
  {
    code: "MC",
    phone: 377,
  },
  {
    code: "MN",
    phone: 976,
  },
  {
    code: "ME",
    phone: 382,
  },
  {
    code: "MS",
    phone: 1664,
  },
  {
    code: "MA",
    phone: 212,
  },
  {
    code: "MZ",
    phone: 258,
  },
  {
    code: "MM",
    phone: 95,
  },
  {
    code: "NA",
    phone: 264,
  },
  {
    code: "NR",
    phone: 674,
  },
  {
    code: "NP",
    phone: 977,
  },
  {
    code: "NL",
    phone: 31,
  },
  {
    code: "AN",
    phone: 599,
  },
  {
    code: "NC",
    phone: 687,
  },
  {
    code: "NZ",
    phone: 64,
  },
  {
    code: "NI",
    phone: 505,
  },
  {
    code: "NE",
    phone: 227,
  },
  {
    code: "NG",
    phone: 234,
  },
  {
    code: "NU",
    phone: 683,
  },
  {
    code: "NF",
    phone: 672,
  },
  {
    code: "MP",
    phone: 1670,
  },
  {
    code: "NO",
    phone: 47,
  },
  {
    code: "OM",
    phone: 968,
  },
  {
    code: "PK",
    phone: 92,
  },
  {
    code: "PW",
    phone: 680,
  },
  {
    code: "PS",
    phone: 970,
  },
  {
    code: "PA",
    phone: 507,
  },
  {
    code: "PG",
    phone: 675,
  },
  {
    code: "PY",
    phone: 595,
  },
  {
    code: "PE",
    phone: 51,
  },
  {
    code: "PH",
    phone: 63,
  },
  {
    code: "PN",
    phone: 64,
  },
  {
    code: "PL",
    phone: 48,
  },
  {
    code: "PT",
    phone: 351,
  },
  {
    code: "PR",
    phone: 1787,
  },
  {
    code: "QA",
    phone: 974,
  },
  {
    code: "RE",
    phone: 262,
  },
  {
    code: "RO",
    phone: 40,
  },
  {
    code: "RU",
    phone: 7,
  },
  {
    code: "RW",
    phone: 250,
  },
  {
    code: "BL",
    phone: 590,
  },
  {
    code: "SH",
    phone: 290,
  },
  {
    code: "KN",
    phone: 1869,
  },
  {
    code: "LC",
    phone: 1758,
  },
  {
    code: "MF",
    phone: 590,
  },
  {
    code: "PM",
    phone: 508,
  },
  {
    code: "VC",
    phone: 1784,
  },
  {
    code: "WS",
    phone: 684,
  },
  {
    code: "SM",
    phone: 378,
  },
  {
    code: "ST",
    phone: 239,
  },
  {
    code: "SA",
    phone: 966,
  },
  {
    code: "SN",
    phone: 221,
  },
  {
    code: "RS",
    phone: 381,
  },
  {
    code: "CS",
    phone: 381,
  },
  {
    code: "SC",
    phone: 248,
  },
  {
    code: "SL",
    phone: 232,
  },
  {
    code: "SG",
    phone: 65,
  },
  {
    code: "SX",
    phone: 1,
  },
  {
    code: "SK",
    phone: 421,
  },
  {
    code: "SI",
    phone: 386,
  },
  {
    code: "SB",
    phone: 677,
  },
  {
    code: "SO",
    phone: 252,
  },
  {
    code: "ZA",
    phone: 27,
  },
  {
    code: "GS",
    phone: 500,
  },
  {
    code: "SS",
    phone: 211,
  },
  {
    code: "ES",
    phone: 34,
  },
  {
    code: "LK",
    phone: 94,
  },
  {
    code: "SD",
    phone: 249,
  },
  {
    code: "SR",
    phone: 597,
  },
  {
    code: "SJ",
    phone: 47,
  },
  {
    code: "SZ",
    phone: 268,
  },
  {
    code: "SE",
    phone: 46,
  },
  {
    code: "CH",
    phone: 41,
  },
  {
    code: "SY",
    phone: 963,
  },
  {
    code: "TW",
    phone: 886,
  },
  {
    code: "TJ",
    phone: 992,
  },
  {
    code: "TZ",
    phone: 255,
  },
  {
    code: "TH",
    phone: 66,
  },
  {
    code: "TL",
    phone: 670,
  },
  {
    code: "TG",
    phone: 228,
  },
  {
    code: "TK",
    phone: 690,
  },
  {
    code: "TO",
    phone: 676,
  },
  {
    code: "TT",
    phone: 1868,
  },
  {
    code: "TN",
    phone: 216,
  },
  {
    code: "TR",
    phone: 90,
  },
  {
    code: "TM",
    phone: 7370,
  },
  {
    code: "TC",
    phone: 1649,
  },
  {
    code: "TV",
    phone: 688,
  },
  {
    code: "UG",
    phone: 256,
  },
  {
    code: "UA",
    phone: 380,
  },
  {
    code: "AE",
    phone: 971,
  },
  {
    code: "GB",
    phone: 44,
  },
  {
    code: "US",
    phone: 1,
  },
  {
    code: "UM",
    phone: 1,
  },
  {
    code: "UY",
    phone: 598,
  },
  {
    code: "UZ",
    phone: 998,
  },
  {
    code: "VU",
    phone: 678,
  },
  {
    code: "VE",
    phone: 58,
  },
  {
    code: "VN",
    phone: 84,
  },
  {
    code: "VG",
    phone: 1284,
  },
  {
    code: "VI",
    phone: 1340,
  },
  {
    code: "WF",
    phone: 681,
  },
  {
    code: "EH",
    phone: 212,
  },
  {
    code: "YE",
    phone: 967,
  },
  {
    code: "ZM",
    phone: 260,
  },
  {
    code: "ZW",
    phone: 263,
  },
];
  // const signUpForm = document.querySelector("#lead_form");
  // const apiUrl = "https://demoapi.whatcx.com/demo-account/signup";
  // const clientKey = "6294834c9efc006adf691b12";
  // const role = "62949747af303af1e71bad80";
  
  // if (signUpForm) {
  //   signUpForm.addEventListener("submit", function (e) {
  //     e.preventDefault();
  //     let country_initial = countries.filter(x=>{
  //       if(x.phone==`${e.target[3].value}`)
  //       return x;
  //     });
      
  //     country_initial=country_initial[0].code;//getting Country initails e.g "IN"
      
  //     const data = {
  //       firstName: e.target[0].value,
  //       lastName: e.target[1].value,
  //       email: e.target[2].value,
  //       organizationName: e.target[5].value,
  //       website: e.target[6].value,
  //       clientKey: clientKey,
  //       role: role,
  //       mobileNumber: {
  //         country: country_initial,
  //         mobile: e.target[4].value,
  //       },
  //     };
  //     // console.log(data);
  //     // return;
  //     fetch(apiUrl, {
  //       method: "POST",
  //       headers: {
  //         "Content-Type": "application/json",
  //       },
  //       body: JSON.stringify(data),
  //       redirect: "follow",
  //     })
  //       .then((res) => {
  //         if (res.ok) {
  //           console.log("success");
  //           signUpForm.reset();
  //           window.location.href = "email/thankyou.php";
  //           return res;
  //         } else {
  //           console.log("not success");
  //           res.json().then((data) => {
  //             Toastify({
  //               text: data.message,
  //               duration: 4000,
  //               newWindow: true,
  //               close: true,
  //               gravity: "center", // `top` or `bottom`
  //               position: "center", // `left`, `center` or `right`
  //               stopOnFocus: true, // Prevents dismissing of toast on hover
  //               style: {
  //                 background:
  //                   "linear-gradient(76.07deg, #ee2a7b 9.33%, #9d1f5d 92.01%)",
  //               },
  //               onClick: function () {}, // Callback after click
  //             }).showToast();
  //           });
  //         }
  //       })
  //       .catch((error) => console.log("error", error));
  //   });
  // }

  //New Signup form
  const leadform = document.querySelector("#lead_form");
  if (leadform) {

    leadform.addEventListener("submit", function (e) {
      e.preventDefault();
      const data = new FormData(e.target);
      const fname = data.get("fname");
      const lname = data.get("lname");
      const email = data.get("email");
      const mobile = data.get("mobile");
      const country = data.get("country");
      const companyName = data.get("cname");
      const websiteURL = data.get("websiteURL");

      fetch("https://www.turbocx.com/webapi/sendMail", {
        method: "POST",
        body: JSON.stringify({
          name: fname + " " + lname,
          email: email,
          mobile: country+mobile,
          companyName: companyName,
          websiteURL: websiteURL
        }),
        headers: {
          "Content-Type": "application/json",
        },
      }).then(() => {
        leadform.reset();
	      window.location.href = "https://www.turbocx.com/sign-up?utm_source=TurboCX&utm_medium=Header&utm_campaign=Signup";
      });
      
    });
  }


  // footer menu
  const footerMenus = Array.from(
    document.querySelectorAll(".footer .colItems > h5")
  );
  if (footerMenus) {
    footerMenus.forEach((menu) => {
      menu.addEventListener("click", (e) => {
        if (!e.target.parentElement.classList.contains("open")) {
          for (let i in footerMenus) {
            footerMenus[i].parentElement.classList.remove("open");
          }
          e.target.parentElement.classList.add("open");
        } else {
          e.target.parentElement.classList.remove("open");
        }
      });
    });
  }

  // plans
  const plansHead = Array.from(document.querySelectorAll(".plansHeading"));
  let planHeadHeight = 0;
  plansHead.forEach((element) => {
    element.clientHeight > planHeadHeight
      ? (planHeadHeight = element.clientHeight)
      : planHeadHeight;
  });
  plansHead.forEach((element, index) => {
    plansHead[index].style.height = planHeadHeight + "px";
  });

  // Features Accordion
  $(".btn-link").on("click", function () {
    if (!$(this).parent().siblings(".fpaccordion-body").hasClass("current")) {
      $(".fpaccordion-arrow").removeClass("active");
      $(".btn-link")
        .parent()
        .siblings(".fpaccordion-body")
        .removeClass("current");
      $(this).parent().siblings(".fpaccordion-body").addClass("current");
      $(this).children(".fpaccordion-arrow").addClass("active");
      $(".fpaccordion").removeClass("active");
      $(this).parent().parent(".fpaccordion").addClass("active");
      $(".fpAccordionG .leftRightGrid-image img").attr(
        "src",
        $(this).attr("imageLink")
      );
    } else {
      $(this).children(".fpaccordion-arrow").removeClass("active");
      $(this).parent().siblings(".fpaccordion-body").removeClass("current");
      $(this).parent().parent(".fpaccordion").removeClass("active");
    }
  });

  // Model
  const modalWindow = document.querySelector(".modalWindow");
  if (modalWindow) {
    const close = modalWindow.querySelector(".modalWindow-close");
    const cards = Array.from(document.querySelectorAll(".modelBtn"));
    cards.forEach((card) => {
      card.addEventListener("click", () => {
        modalWindow.classList.remove("open");
        modalWindow.style.display = "flex";
        modalWindow.classList.add("open");
        document.body.classList.add("no-scroll");
      });
    });
    close.addEventListener("click", () => {
      modalWindow.classList.remove("open");
      modalWindow.classList.add("close");
      document.body.classList.remove("no-scroll");
      setTimeout(() => {
        modalWindow.style.display = "none";
        modalWindow.classList.remove("close");
      }, 300);
    });
  }

  // Benefit Page Load More Option
  const loadMoreBtn = $(".loadMoreBtn");
  const loadMoreOptions = $(".loadMoreOptions");
  if (loadMoreBtn && loadMoreOptions) {
    loadMoreBtn.click(() => {
      loadMoreOptions.toggleClass("d-n");
      if (loadMoreBtn.html() == "Load Less") {
        loadMoreBtn.html("Load More");
      } else {
        loadMoreBtn.html("Load Less");
      }
    });
  }

  // dom loaded ends
});
// window load
window.onload = () => {
  console.log("page is fully loaded");

  const header = document.querySelector("nav");
  let lastScrollTop = 0;

  if (header && document.documentElement.clientWidth > 1249) {
    document.addEventListener("scroll", function () {
      if (window.scrollY > 150 || document.documentElement.scrollTop > 150) {
        const currentScroll =
          window.scrollY || document.documentElement.scrollTop; // Credits: "https://github.com/qeremy/so/blob/master/so.dom.js#L426"

        if (currentScroll > lastScrollTop) {
          if (!header.classList.contains("unpinned")) {
            header.classList.add("unpinned");
            header.classList.remove("pinned");
            return;
          }
        } else {
          if (!header.classList.contains("pinned")) {
            header.classList.remove("unpinned");
            header.classList.add("pinned");
            return;
          }
        }
        lastScrollTop = currentScroll <= 0 ? 0 : currentScroll; // For Mobile or negative scrolling
      }

      if (window.scrollY < 150 || document.documentElement.scrollTop < 150) {
        if (!header.classList.contains("unpinned")) {
          header.classList.remove("unpinned");
          header.classList.add("pinned");
          return;
        }
      }
    });
  }

  $(function () {
    $(
      '.nav-links a[href="' +
        location.pathname.split("/")[location.pathname.split("/").length - 1] +
        '"]'
    )
      .parent()
      .addClass("active");
    if ($(".nav-links ul li ul li").hasClass("active")) {
      $(".nav-links ul li ul li.active").parent().parent().addClass("active");
    }
    // if (!$(".nav-links ul li").hasClass('active')) {
    //   $(".nav-links ul li").first().addClass("active");
    // }
  });
};
// window load end

// old code snippets

// header
// const scrollHandler = (func, wait) => {
//   let lastTime = 0;
//   return (...args) => {
//     const now = Date.now();
//     if (now - lastTime >= wait) {
//       func(...args);
//       lastTime = now;
//     }
//   }
// }

// const observer = new IntersectionObserver((entries) => {
//   entries.forEach((entry) => {
//     const id = entry?.target?.getAttribute("id");
//     if (entry.intersectionRatio > 0) {
//       document
//         .querySelector(`aside li a[href="#${id}"]`)
//         ?.parentElement.classList.add("active");
//     } else {
//       document
//         .querySelector(`aside li a[href="#${id}"]`)
//         ?.parentElement.classList.remove("active");
//     }
//   });
// });

// Track all sections that have an `id` applied
// const sectionWithID = document.querySelectorAll("section[id]");
// if (sectionWithID) {
//   sectionWithID.forEach((section) => {
//     observer.observe(section);
//   });
// }

// function faqHandler(faqList) {
//   faqList.forEach((faq) => {
//      faq.addEventListener("click", () => {
//        faqList.forEach((faq) => faq.classList.remove("active"));
//        faq.classList.add("active");
//     });
//   });
// }

// faqs && faqHandler(faqs);
// subFaqs && faqHandler(subFaqs);

// pricing plans
// const ppPlansPlansWrap = document.querySelector(".ppPlans-plansWrap");

// if (ppPlansPlansWrap) {
//   // get value

//   const plansPricing = Array.from(document.querySelectorAll(".plansPricing"));
//   const plansFeatures = Array.from(
//     document.querySelectorAll(".plansFeaturesHeading")
//   );

//   // default value
//   let planHeadHeight = 0;
//   let priceHeadHeight = 0;
//   let planfeaturesHeight = 0;

//   // get head height
//   plansHead.forEach((element) => {
//     element.clientHeight > planHeadHeight
//       ? (planHeadHeight = element.clientHeight)
//       : planHeadHeight;
//   });

//   // set head height
//   plansHead.forEach((element, index) => {
//     plansHead[index].style.height = planHeadHeight + "px";
//   });

//   // get price height
//   plansPricing.forEach((element) => {
//     element.clientHeight > priceHeadHeight
//       ? (priceHeadHeight = element.clientHeight)
//       : priceHeadHeight;
//   });

//   // set price height
//   plansPricing.forEach((element, index) => {
//     plansPricing[index].style.height = priceHeadHeight + "px";
//   });

//   // get price height
//   plansFeatures.forEach((element) => {
//     element.clientHeight > planfeaturesHeight
//       ? (planfeaturesHeight = element.clientHeight)
//       : planfeaturesHeight;
//   });

//   // set price height
//   plansFeatures.forEach((element, index) => {
//     plansFeatures[index].style.height = planfeaturesHeight + "px";
//   });
// }


// TIMELINE SECTION CURSOR DRAG
const slider = document.querySelector('.timeline-box');
let isDown = false;
let startX;
let scrollLeft;

if(slider){
  slider.addEventListener('mousedown', (e) => {
      isDown = true;
      slider.classList.add('active');
      startX = e.pageX - slider.offsetLeft;
      scrollLeft = slider.scrollLeft;
  });
  slider.addEventListener('mouseleave', () => {
      isDown = false;
      slider.classList.remove('active');
  });
  slider.addEventListener('mouseup', () => {
      isDown = false;
      slider.classList.remove('active');
  });
  slider.addEventListener('mousemove', (e) => {
      if (!isDown) return;
      e.preventDefault();
      const x = e.pageX - slider.offsetLeft;
      const walk = (x - startX) * 3; //scroll-fast
      slider.scrollLeft = scrollLeft - walk;
      console.log(walk);
  });
}

// let slideIndex = 1;
// showSlides(slideIndex);

// function plusSlides(n) {
//   showSlides(slideIndex += n);
// }

// function currentSlide(n) {
//   showSlides(slideIndex = n);
// }

// function showSlides(n) {
//   let i;
//   let slides = document.getElementsByClassName("ab-mySlides");
//   let dots = document.getElementsByClassName("dot");
//   if (n > slides.length) {slideIndex = 1}    
//   if (n < 1) {slideIndex = slides.length}
//   for (i = 0; i < slides.length; i++) {
//     slides[i].style.display = "none";  
//   }
//   for (i = 0; i < dots.length; i++) {
//     dots[i].className = dots[i].className.replace(" active", "");
//   }
//   slides[slideIndex-1].style.display = "block";  
//   dots[slideIndex-1].className += " active";
// }

$(window).scroll(function() {    
  var scroll = $(window).scrollTop();    
  if (scroll >= 50) {
      $(".circle-1").addClass("circle-1-ani");
      $(".circle-2").addClass("circle-2-ani");
      $(".btw-img").addClass("btw-img-ani");
      $(".circle-1 p").addClass("p-ani");
      $(".circle-2 p").addClass("p2-ani");
  }
});
