<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>Onboarding-whatcx</title>
  <meta name="description" content="">
  <meta name="keywords" content="">
  <meta property="og:title" content="Onboarding-whatcx" />
  <meta property="og:description" content="" />
  <link rel="stylesheet" href="assets/css/main.min.css" />
  <meta property="og:url" content="https://turbocx.com/onboarding/" />
  <link rel="canonical" href="https://turbocx.com/onboarding/" />
  <link rel="icon" href="assets/images/favicon.png" sizes="32x32" />
</head>


<body class="on-board-bg">
  <main>
    <section>
      <div class="container">
        <div class="onboard-box">
          <div class="logo-onboard">
            <img src="assets/images/onboard-logo.svg">
          </div>
          <p class="text-center fs-32 fw-600 txt-1">Onboarding</p>
          <p class="text-center txt-g fs-16 txt-75">We’re super excited to work with you! But before that, please fill out the form below to begin the onboarding process.</p>
          <div>
            <form action="onboarding-terms.php" class="onboard_form" method="post">
              <div class="grid-onboard">
                <div>
                  <label for="">Company Name<span>*</span></label>
                  <input type="text" placeholder="Enter company name" name="cname" required id="cname">
                </div>
                <div>
                  <label for="">GST/TGS ID no.<span>*</span></label>
                  <input type="text" placeholder="Enter GST/TGS ID number" name="gstno" id="gstno" required>
                </div>
              </div>
              <div class="onboard-FT">
                <label>Registerd Address<span>*</span></label>
                <input type="text" placeholder="Enter registered address" name="raddress" id="raddress" required>
              </div>
              <div class="onboard-FT">
                <label>Correspondence Address<span>*</span></label>
                <input type="text" placeholder="Enter correspondence address" name="caddress" id="caddress" required>
              </div>
              <div class="grid-onboard onboard-FT">
                <div>
                  <label for="">City<span>*</span></label>
                  <input type="text" placeholder="Enter your city " name="city" id="city" required>
                </div>
                <div>
                  <label for="">State<span>*</span></label>
                  <input type="text" placeholder="Enter state" name="state" id="state" required>
                </div>
                <div>
                  <label for="">Country<span>*</span></label>
                  <select name="country" class="js-example-basic-single" id="country">
                    <option value="0" label="Select a country ... " selected="selected">Select a country ... </option>
                    <optgroup id="country-optgroup-Africa" label="Africa">
                      <option value="Algeria">Algeria</option>
                      <option value="Angola">Angola</option>
                      <option value="Benin">Benin</option>
                      <option value="Botswana">Botswana</option>
                      <option value="Burkina Faso">Burkina Faso</option>
                      <option value="Burundi">Burundi</option>
                      <option value="Cameroon">Cameroon</option>
                      <option value="Cape Verde">Cape Verde</option>
                      <option value="Central African Republic">Central African Republic</option>
                      <option value="Chad">Chad</option>
                      <option value="Comoros">Comoros</option>
                      <option value="Congo - Brazzaville">Congo - Brazzaville</option>
                      <option value="Congo - Kinshasa">Congo - Kinshasa</option>
                      <option value="Côte d’Ivoire">Côte d’Ivoire</option>
                      <option value="Djibouti">Djibouti</option>
                      <option value="Egypt">Egypt</option>
                      <option value="Equatorial Guinea">Equatorial Guinea</option>
                      <option value="Eritrea">Eritrea</option>
                      <option value="Ethiopia">Ethiopia</option>
                      <option value="Gabon">Gabon</option>
                      <option value="Gambia">Gambia</option>
                      <option value="Ghana">Ghana</option>
                      <option value="Guinea">Guinea</option>
                      <option value="Guinea-Bissau">Guinea-Bissau</option>
                      <option value="Kenya">Kenya</option>
                      <option value="Lesotho">Lesotho</option>
                      <option value="Liberia">Liberia</option>
                      <option value="Libya">Libya</option>
                      <option value="Madagascar">Madagascar</option>
                      <option value="Malawi">Malawi</option>
                      <option value="Mali">Mali</option>
                      <option value="Mauritania">Mauritania</option>
                      <option value="Mauritius">Mauritius</option>
                      <option value="Mayotte">Mayotte</option>
                      <option value="Morocco">Morocco</option>
                      <option value="Mozambique">Mozambique</option>
                      <option value="Namibia">Namibia</option>
                      <option value="Niger">Niger</option>
                      <option value="Nigeria">Nigeria</option>
                      <option value="Rwanda">Rwanda</option>
                      <option value="Réunion">Réunion</option>
                      <option value="Saint Helena">Saint Helena</option>
                      <option value="Senegal">Senegal</option>
                      <option value="Seychelles">Seychelles</option>
                      <option value="Sierra Leone">Sierra Leone</option>
                      <option value="Somalia">Somalia</option>
                      <option value="South Africa">South Africa</option>
                      <option value="Sudan">Sudan</option>
                      <option value="Swaziland">Swaziland</option>
                      <option value="São Tomé and Príncipe">São Tomé and Príncipe</option>
                      <option value="Tanzania">Tanzania</option>
                      <option value="Togo">Togo</option>
                      <option value="Tunisia">Tunisia</option>
                      <option value="Uganda">Uganda</option>
                      <option value="Western Sahara">Western Sahara</option>
                      <option value="Zambia">Zambia</option>
                      <option value="Zimbabwe">Zimbabwe</option>
                    </optgroup>
                    <optgroup id="country-optgroup-Americas" value="Americas">
                      <option value="Anguilla">Anguilla</option>
                      <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                      <option value="Argentina">Argentina</option>
                      <option value="Aruba">Aruba</option>
                      <option value="Bahamas">Bahamas</option>
                      <option value="Barbados">Barbados</option>
                      <option value="Belize">Belize</option>
                      <option value="Bermuda">Bermuda</option>
                      <option value="Bolivia">Bolivia</option>
                      <option value="Brazil">Brazil</option>
                      <option value="British Virgin Islands">British Virgin Islands</option>
                      <option value="Canada">Canada</option>
                      <option value="Cayman Islands">Cayman Islands</option>
                      <option value="Chile">Chile</option>
                      <option value="Colombia">Colombia</option>
                      <option value="Costa Rica">Costa Rica</option>
                      <option value="Cuba">Cuba</option>
                      <option value="Dominica">Dominica</option>
                      <option value="Dominican Republic">Dominican Republic</option>
                      <option value="Ecuador">Ecuador</option>
                      <option value="El Salvador">El Salvador</option>
                      <option value="Falkland Islands">Falkland Islands</option>
                      <option value="French Guiana">French Guiana</option>
                      <option value="Greenland">Greenland</option>
                      <option value="Grenada">Grenada</option>
                      <option value="Guadeloupe">Guadeloupe</option>
                      <option value="Guatemala">Guatemala</option>
                      <option value="Guyana">Guyana</option>
                      <option value="Haiti">Haiti</option>
                      <option value="Honduras">Honduras</option>
                      <option value="Jamaica">Jamaica</option>
                      <option value="Martinique">Martinique</option>
                      <option value="Mexico">Mexico</option>
                      <option value="Montserrat">Montserrat</option>
                      <option value="Netherlands Antilles">Netherlands Antilles</option>
                      <option value="Nicaragua">Nicaragua</option>
                      <option value="Panama">Panama</option>
                      <option value="Paraguay">Paraguay</option>
                      <option value="Peru">Peru</option>
                      <option value="Puerto Rico">Puerto Rico</option>
                      <option value="Saint Barthélemy">Saint Barthélemy</option>
                      <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                      <option value="Saint Lucia">Saint Lucia</option>
                      <option value="Saint Martin">Saint Martin</option>
                      <option value="Saint Pierre and Miquelon">Saint Pierre and Miquelon</option>
                      <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                      <option value="Suriname">Suriname</option>
                      <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                      <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                      <option value="U.S. Virgin Islands">U.S. Virgin Islands</option>
                      <option value="United States">United States</option>
                      <option value="Uruguay">Uruguay</option>
                      <option value="Venezuela">Venezuela</option>
                    </optgroup>
                    <optgroup id="country-optgroup-Asia" value="Asia">
                      <option value="Afghanistan">Afghanistan</option>
                      <option value="Armenia">Armenia</option>
                      <option value="Azerbaijan">Azerbaijan</option>
                      <option value="Bahrain">Bahrain</option>
                      <option value="Bangladesh">Bangladesh</option>
                      <option value="Bhutan">Bhutan</option>
                      <option value="Brunei">Brunei</option>
                      <option value="Cambodia">Cambodia</option>
                      <option value="China">China</option>
                      <option value="Georgia">Georgia</option>
                      <option value="Hong Kong SAR China">Hong Kong SAR China</option>
                      <option value="India">India</option>
                      <option value="Indonesia">Indonesia</option>
                      <option value="Iran">Iran</option>
                      <option value="Iraq">Iraq</option>
                      <option value="Israel">Israel</option>
                      <option value="Japan">Japan</option>
                      <option value="Jordan">Jordan</option>
                      <option value="Kazakhstan">Kazakhstan</option>
                      <option value="Kuwait">Kuwait</option>
                      <option value="Kyrgyzstan">Kyrgyzstan</option>
                      <option value="Laos">Laos</option>
                      <option value="Lebanon">Lebanon</option>
                      <option value="Macau SAR China">Macau SAR China</option>
                      <option value="Malaysia">Malaysia</option>
                      <option value="Maldives">Maldives</option>
                      <option value="Mongolia">Mongolia</option>
                      <option value="Myanmar [Burma]">Myanmar [Burma]</option>
                      <option value="Nepal">Nepal</option>
                      <option value="Neutral Zone">Neutral Zone</option>
                      <option value="North Korea">North Korea</option>
                      <option value="Oman">Oman</option>
                      <option value="Pakistan">Pakistan</option>
                      <option value="Palestinian Territories">Palestinian Territories</option>
                      <option value="People's Democratic Republic of Yemen">People's Democratic Republic of Yemen</option>
                      <option value="Philippines">Philippines</option>
                      <option value="Qatar">Qatar</option>
                      <option value="Saudi Arabia">Saudi Arabia</option>
                      <option value="Singapore">Singapore</option>
                      <option value="South Korea">South Korea</option>
                      <option value="Sri Lanka">Sri Lanka</option>
                      <option value="Syria">Syria</option>
                      <option value="Taiwan">Taiwan</option>
                      <option value="Tajikistan">Tajikistan</option>
                      <option value="Thailand">Thailand</option>
                      <option value="Timor-Leste">Timor-Leste</option>
                      <option value="Turkey">Turkey</option>
                      <option value="Turkmenistan">Turkmenistan</option>
                      <option value="United Arab Emirates">United Arab Emirates</option>
                      <option value="Uzbekistan">Uzbekistan</option>
                      <option value="Vietnam">Vietnam</option>
                      <option value="Yemen">Yemen</option>
                    </optgroup>
                    <optgroup id="country-optgroup-Europe" value="Europe">
                      <option value="Albania">Albania</option>
                      <option value="Andorra">Andorra</option>
                      <option value="Austria">Austria</option>
                      <option value="Belarus">Belarus</option>
                      <option value="Belgium">Belgium</option>
                      <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                      <option value="Bulgaria">Bulgaria</option>
                      <option value="Croatia">Croatia</option>
                      <option value="Cyprus">Cyprus</option>
                      <option value="Czech Republic">Czech Republic</option>
                      <option value="Denmark">Denmark</option>
                      <option value="East Germany">East Germany</option>
                      <option value="Estonia">Estonia</option>
                      <option value="Faroe Islands">Faroe Islands</option>
                      <option value="Finland">Finland</option>
                      <option value="France">France</option>
                      <option value="Germany">Germany</option>
                      <option value="Gibraltar">Gibraltar</option>
                      <option value="Greece">Greece</option>
                      <option value="Guernsey">Guernsey</option>
                      <option value="Hungary">Hungary</option>
                      <option value="Iceland">Iceland</option>
                      <option value="Ireland">Ireland</option>
                      <option value="Isle of Man">Isle of Man</option>
                      <option value="Italy">Italy</option>
                      <option value="Jersey">Jersey</option>
                      <option value="Latvia">Latvia</option>
                      <option value="Liechtenstein">Liechtenstein</option>
                      <option value="Lithuania">Lithuania</option>
                      <option value="Luxembourg">Luxembourg</option>
                      <option value="Macedonia">Macedonia</option>
                      <option value="Malta">Malta</option>
                      <option value="Metropolitan France">Metropolitan France</option>
                      <option value="Moldova">Moldova</option>
                      <option value="Monaco">Monaco</option>
                      <option value="Montenegro">Montenegro</option>
                      <option value="Netherlands">Netherlands</option>
                      <option value="Norway">Norway</option>
                      <option value="Poland">Poland</option>
                      <option value="Portugal">Portugal</option>
                      <option value="Romania">Romania</option>
                      <option value="Russia">Russia</option>
                      <option value="San Marino">San Marino</option>
                      <option value="Serbia">Serbia</option>
                      <option value="Serbia and Montenegro">Serbia and Montenegro</option>
                      <option value="Slovakia">Slovakia</option>
                      <option value="Slovenia">Slovenia</option>
                      <option value="Spain">Spain</option>
                      <option value="Svalbard and Jan Mayen">Svalbard and Jan Mayen</option>
                      <option value="Sweden">Sweden</option>
                      <option value="Switzerland">Switzerland</option>
                      <option value="Ukraine">Ukraine</option>
                      <option value="Union of Soviet Socialist Republics">Union of Soviet Socialist Republics</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="Vatican City">Vatican City</option>
                      <option value="Åland Islands">Åland Islands</option>
                    </optgroup>
                    <optgroup id="country-optgroup-Oceania" value="Oceania">
                      <option value="American Samoa">American Samoa</option>
                      <option value="Antarctica">Antarctica</option>
                      <option value="Australia">Australia</option>
                      <option value="Bouvet Island">Bouvet Island</option>
                      <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                      <option value="Christmas Island">Christmas Island</option>
                      <option value="Cocos [Keeling] Islands">Cocos [Keeling] Islands</option>
                      <option value="Cook Islands">Cook Islands</option>
                      <option value="Fiji">Fiji</option>
                      <option value="French Polynesia">French Polynesia</option>
                      <option value="French Southern Territories">French Southern Territories</option>
                      <option value="Guam">Guam</option>
                      <option value="Heard Island and McDonald Islands">Heard Island and McDonald Islands</option>
                      <option value="Kiribati">Kiribati</option>
                      <option value="Marshall Islands">Marshall Islands</option>
                      <option value="Micronesia">Micronesia</option>
                      <option value="Nauru">Nauru</option>
                      <option value="New Caledonia">New Caledonia</option>
                      <option value="New Zealand">New Zealand</option>
                      <option value="Niue">Niue</option>
                      <option value="Norfolk Island">Norfolk Island</option>
                      <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                      <option value="Palau">Palau</option>
                      <option value="Papua New Guinea">Papua New Guinea</option>
                      <option value="Pitcairn Islands">Pitcairn Islands</option>
                      <option value="Samoa">Samoa</option>
                      <option value="Solomon Islands">Solomon Islands</option>
                      <option value="South Georgia and the South Sandwich Islands">South Georgia and the South Sandwich Islands</option>
                      <option value="Tokelau">Tokelau</option>
                      <option value="Tonga">Tonga</option>
                      <option value="Tuvalu">Tuvalu</option>
                      <option value="U.S. Minor Outlying Islands">U.S. Minor Outlying Islands</option>
                      <option value="Vanuatu">Vanuatu</option>
                      <option value="Wallis and Futuna">Wallis and Futuna</option>
                    </optgroup>
                  </select>
                  <!-- <input type="text" placeholder="Enter country" name="country" id="country" required> -->
                </div>

                <div>
                  <label for="">pincode<span>*</span></label>
                  <input type="number" placeholder="Enter your pincode " name="pincode" id="pincode" required>
                </div>
              </div>
              <div class="grid-onboard onboard-FT">
                <div>
                  <label for="">Name<span>*</span></label>
                  <input type="text" placeholder="Enter your name " name="name" id="name" required>
                </div>
                <div>
                  <label for="">Mobile no.<span>*</span></label>
                  <div class="formItem required ">

                    <div class="country-code onboarding-mobile">
                      <select name="countrycode" required>
                        <option value="+1">+1</option>,
                        <option value="+20">+20</option>,
                        <option value="+211">+211</option>,
                        <option value="+212">+212</option>,
                        <option value="+213">+213</option>,
                        <option value="+216">+216</option>,
                        <option value="+218">+218</option>,
                        <option value="+220">+220</option>,
                        <option value="+221">+221</option>,
                        <option value="+222">+222</option>,
                        <option value="+223">+223</option>,
                        <option value="+224">+224</option>,
                        <option value="+225">+225</option>,
                        <option value="+226">+226</option>,
                        <option value="+227">+227</option>,
                        <option value="+228">+228</option>,
                        <option value="+229">+229</option>,
                        <option value="+230">+230</option>,
                        <option value="+231">+231</option>,
                        <option value="+232">+232</option>,
                        <option value="+233">+233</option>,
                        <option value="+234">+234</option>,
                        <option value="+235">+235</option>,
                        <option value="+236">+236</option>,
                        <option value="+237">+237</option>,
                        <option value="+238">+238</option>,
                        <option value="+239">+239</option>,
                        <option value="+240">+240</option>,
                        <option value="+241">+241</option>,
                        <option value="+242">+242</option>,
                        <option value="+243">+243</option>,
                        <option value="+244">+244</option>,
                        <option value="+245">+245</option>,
                        <option value="+246">+246</option>,
                        <option value="+248">+248</option>,
                        <option value="+249">+249</option>,
                        <option value="+250">+250</option>,
                        <option value="+251">+251</option>,
                        <option value="+252">+252</option>,
                        <option value="+253">+253</option>,
                        <option value="+254">+254</option>,
                        <option value="+255">+255</option>,
                        <option value="+256">+256</option>,
                        <option value="+257">+257</option>,
                        <option value="+258">+258</option>,
                        <option value="+260">+260</option>,
                        <option value="+261">+261</option>,
                        <option value="+262">+262</option>,
                        <option value="+263">+263</option>,
                        <option value="+264">+264</option>,
                        <option value="+265">+265</option>,
                        <option value="+266">+266</option>,
                        <option value="+267">+267</option>,
                        <option value="+268">+268</option>,
                        <option value="+269">+269</option>,
                        <option value="+27">+27</option>,
                        <option value="+290">+290</option>,
                        <option value="+291">+291</option>,
                        <option value="+297">+297</option>,
                        <option value="+298">+298</option>,
                        <option value="+299">+299</option>,
                        <option value="+30">+30</option>,
                        <option value="+31">+31</option>,
                        <option value="+32">+32</option>,
                        <option value="+33">+33</option>,
                        <option value="+34">+34</option>,
                        <option value="+350">+350</option>,
                        <option value="+351">+351</option>,
                        <option value="+352">+352</option>,
                        <option value="+353">+353</option>,
                        <option value="+354">+354</option>,
                        <option value="+355">+355</option>,
                        <option value="+356">+356</option>,
                        <option value="+357">+357</option>,
                        <option value="+358">+358</option>,
                        <option value="+359">+359</option>,
                        <option value="+36">+36</option>,
                        <option value="+370">+370</option>,
                        <option value="+371">+371</option>,
                        <option value="+372">+372</option>,
                        <option value="+373">+373</option>,
                        <option value="+374">+374</option>,
                        <option value="+375">+375</option>,
                        <option value="+376">+376</option>,
                        <option value="+377">+377</option>,
                        <option value="+378">+378</option>,
                        <option value="+379">+379</option>,
                        <option value="+380">+380</option>,
                        <option value="+381">+381</option>,
                        <option value="+382">+382</option>,
                        <option value="+383">+383</option>,
                        <option value="+385">+385</option>,
                        <option value="+386">+386</option>,
                        <option value="+387">+387</option>,
                        <option value="+389">+389</option>,
                        <option value="+39">+39</option>,
                        <option value="+40">+40</option>,
                        <option value="+41">+41</option>,
                        <option value="+420">+420</option>,
                        <option value="+421">+421</option>,
                        <option value="+423">+423</option>,
                        <option value="+43">+43</option>,
                        <option value="+44">+44</option>,
                        <option value="+45">+45</option>,
                        <option value="+46">+46</option>,
                        <option value="+47">+47</option>,
                        <option value="+48">+48</option>,
                        <option value="+49">+49</option>,
                        <option value="+500">+500</option>,
                        <option value="+501">+501</option>,
                        <option value="+502">+502</option>,
                        <option value="+503">+503</option>,
                        <option value="+504">+504</option>,
                        <option value="+505">+505</option>,
                        <option value="+506">+506</option>,
                        <option value="+507">+507</option>,
                        <option value="+508">+508</option>,
                        <option value="+509">+509</option>,
                        <option value="+51">+51</option>,
                        <option value="+52">+52</option>,
                        <option value="+54">+54</option>,
                        <option value="+55">+55</option>,
                        <option value="+56">+56</option>,
                        <option value="+57">+57</option>,
                        <option value="+58">+58</option>,
                        <option value="+590">+590</option>,
                        <option value="+591">+591</option>,
                        <option value="+592">+592</option>,
                        <option value="+593">+593</option>,
                        <option value="+595">+595</option>,
                        <option value="+597">+597</option>,
                        <option value="+598">+598</option>,
                        <option value="+599">+599</option>,
                        <option value="+60">+60</option>,
                        <option value="+61">+61</option>,
                        <option value="+62">+62</option>,
                        <option value="+63">+63</option>,
                        <option value="+64">+64</option>,
                        <option value="+65">+65</option>,
                        <option value="+66">+66</option>,
                        <option value="+670">+670</option>,
                        <option value="+673">+673</option>,
                        <option value="+674">+674</option>,
                        <option value="+675">+675</option>,
                        <option value="+676">+676</option>,
                        <option value="+677">+677</option>,
                        <option value="+678">+678</option>,
                        <option value="+679">+679</option>,
                        <option value="+680">+680</option>,
                        <option value="+681">+681</option>,
                        <option value="+682">+682</option>,
                        <option value="+683">+683</option>,
                        <option value="+685">+685</option>,
                        <option value="+686">+686</option>,
                        <option value="+687">+687</option>,
                        <option value="+688">+688</option>,
                        <option value="+689">+689</option>,
                        <option value="+690">+690</option>,
                        <option value="+691">+691</option>,
                        <option value="+692">+692</option>,
                        <option value="+7">+7</option>,
                        <option value="+81">+81</option>,
                        <option value="+82">+82</option>,
                        <option value="+84">+84</option>,
                        <option value="+852">+852</option>,
                        <option value="+853">+853</option>,
                        <option value="+855">+855</option>,
                        <option value="+856">+856</option>,
                        <option value="+880">+880</option>,
                        <option value="+886">+886</option>,
                        <option value="+90">+90</option>,
                        <option selected="" value="+91">+91</option>,
                        <option value="+92">+92</option>,
                        <option value="+93">+93</option>,
                        <option value="+94">+94</option>,
                        <option value="+95">+95</option>,
                        <option value="+960">+960</option>,
                        <option value="+961">+961</option>,
                        <option value="+962">+962</option>,
                        <option value="+964">+964</option>,
                        <option value="+965">+965</option>,
                        <option value="+966">+966</option>,
                        <option value="+967">+967</option>,
                        <option value="+968">+968</option>,
                        <option value="+970">+970</option>,
                        <option value="+971">+971</option>,
                        <option value="+972">+972</option>,
                        <option value="+973">+973</option>,
                        <option value="+974">+974</option>,
                        <option value="+975">+975</option>,
                        <option value="+976">+976</option>,
                        <option value="+977">+977</option>,
                        <option value="+992">+992</option>,
                        <option value="+993">+993</option>,
                        <option value="+994">+994</option>,
                        <option value="+995">+995</option>,
                        <option value="+996">+996</option>,
                        <option value="+998">+998</option>
                      </select>
                      <input type="text" maxlength="10" placeholder="Enter your mobile number" name="mobile" id="mobile" required>
                      <span class="select2-selection__arrow" role="presentation"><b role="presentation"></b></span>
                    </div>
                  </div>
                  <!-- 
                  <input type="text" maxlength="10" placeholder="Enter your mobile number" name="mobile" id="mobile" required> -->
                </div>
                <div>
                  <label for="">Email<span>*</span></label>
                  <input type="email" placeholder="Enter your email" name="email" id="email" required>
                </div>
                <div>
                  <label for="">Founder Email<span>*</span></label>
                  <input type="email" placeholder="Enter founder's email" name="femail" id="femail" required>
                </div>
              </div>
              <div class="check-OB">
                <input type="checkbox" id="agreement1" name="agreement1" value="agreement1" required>
                <label for="agreement1">I am authorised to check and operate the TurboCX agent as admin for the
                  company.</label>
              </div>
              <div class="check-OB mb-3">
                <input type="checkbox" id="agreement2" name="agreement2" value="agreement2" required>
                <label for="agreement2"> I agree to the <span><a target="_blank" href="https://www.turbocx.com/privacy-policy.php">privacy and policy</a></span>.</label>
              </div>
              <button type="submit" class="onboard-btn">Submit</button>
            </form>
          </div>
          <div class="onboard-foot">
            <p>It is our policy to protect your privacy and to keep all the information shared by you confidential. To visit our website, please click here. In case of any support, feel free to reach out to us at <a target="_blank" href="https://turbocx.com/">TurboCX</a>.</p>
          </div>
          <div class="foot-url">
            <a target="_blank" href="https://app.turbocx.com/login/">Login</a>
            <a target="_blank" href="https://www.turbocx.com/contact.php">Support</a>
          </div>
        </div>
      </div>
    </section>
  </main>
</body>
<script src="assets/js/vendor.js<?php echo $version; ?>" defer></script>
<script src="assets/js/script.min.js<?php echo $version; ?>" defer></script>

</html>